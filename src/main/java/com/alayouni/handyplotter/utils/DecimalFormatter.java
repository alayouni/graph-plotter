package com.alayouni.handyplotter.utils;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/26/13
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class DecimalFormatter {
	private int precision = 3;
	private double zeroMargin = 1e-4;
	private DecimalFormat exponentFormatter;
	private DecimalFormat simpleFormatter;

	private double min;
	private double max;

	public DecimalFormatter(int precision) {
		this.precision = precision;
		initParams();
	}

	public String format(double d) {
		double abs = Math.abs(d);
		if(abs == 0. || (abs >= min && abs < max)) {
			String formatted = simpleFormatter.format(d);
			int unsignificantCount = countUnSignificantDigitsNumber(formatted, d);

			if(formatted.length() - unsignificantCount <= precision) {
				return formatted;
			} else {
				return formatted.substring(0, precision + unsignificantCount);
			}
		} else {
			return exponentFormatter.format(d);
		}
	}

	/**
	 * <p>To be used by SimpleExpressionField to avoid wonky decimal formatting.</p>
	 * <p>When d is very close to zero it should be formatted as 0, as being very close to zero is most probably due
	 * to doubles non precision.</p>
	 * <p>An algorithm based on step and precision will be used to decide whether values close to zero should be formatted
	 * as is or as 0.</p>
	 * @param d
	 * @param step
	 * @return
	 */
	public String format(double d, double step) {
		step = Math.abs(step);
		if(d != 0 && d - step < 0 && d + step > 0) {//good candidate for wonky decimal formatting
			if(Math.abs(d) < zeroMargin) {
				return "0";
			}
		}
		return this.format(d);
	}

	private int countUnSignificantDigitsNumber(String formatted, double d) {
		int count = 0;
		if(d < 0) {
			count++;
		}
		double abs = Math.abs(d);
		if(formatted.contains(".") && abs * 10 < max) {
			count++;
		}
		if(abs < 1) {
			count++;
		}
		return count;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
		this.zeroMargin = Math.pow(1, -precision - 1);
		initParams();
	}

	private void initParams() {
		min = Math.pow(10, -precision);
		max = Math.pow(10, precision);

		String pattern = StringUtils.repeat("#", precision - 1);
		exponentFormatter = new DecimalFormat("#." + pattern + "E0");
		simpleFormatter = new DecimalFormat(pattern + "#.#" + pattern);

	}


}
