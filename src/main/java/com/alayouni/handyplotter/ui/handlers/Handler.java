package com.alayouni.handyplotter.ui.handlers;

import com.alayouni.handyplotter.expressionutils.ExpressionBuilder;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.ui.components.containers.IndexedTogglesPanel;
import com.alayouni.handyplotter.ui.components.containers.IntersectionsColorPanel;
import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInput;
import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInputsHolder;
import com.alayouni.handyplotter.ui.components.other.UnsafeRunnable;
import com.alayouni.handyplotter.ui.components.splitpane.items.*;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisUnReferencableInputExpressions;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsChangeEvent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Handler {

	protected static IntersectionsColorPanel intersectionsColorPanel;
	protected static IndexedTogglesPanel<FunctionComponent> fIntersectionsPanel1;

	protected static FunctionsHandler functionsHandler;
	protected static VariablesHandler variablesHandler;
	protected static AxisesHandler axisesHandler;
	protected static ViewPortsHandler viewPortsHandler;

	/**
	 * for now will just be used the ErrorDialog to ignore FocusLostEvents when the error message pops up and
	 * takes the focus.
	 */
	private static boolean lockFocusLostEvents = false;

	/**
	 * when loading on startup all rebuilds triggered by handlers should be ignored until everything is loaded, where
	 * setRebuildLocked() should be called before triggering a full rebuild.
	 */
	private static boolean isRebuildLocked = true;


	protected static <T extends Component> ItemsListener<T> createSplitIndexesListener() {
		return new ItemsListener<T>() {
			public void itemsChanged(ItemsChangeEvent<T> e) {
				int startIndex = e.getItemIndex();
				if(e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
					startIndex ++;
				}
				List items = e.getContainer().getItems();
				IndexedComponent comp;
				for(int i = startIndex; i < items.size(); i++) {
					comp = (IndexedComponent)items.get(i);
					comp.setIndex(i);
				}
			}
		};
	}

	/**
	 * not static for easy mocking when testing
	 */
	protected void updateExpressionHolders(ExpressionProcessor expressionProcessor) {
		functionsHandler.updateExpressions(expressionProcessor);
		variablesHandler.updateExpressions(expressionProcessor);
		axisesHandler.updateExpressions(expressionProcessor);
	}


	private static boolean showErrorDialogOnBreak;
	/**
	 * not static for easy mocking when testing.
	 * All buttons that trigger rebuilds should be non focusable so no focus lost event send another rebuild
	 */
	public void rebuildAll(boolean showErrorDialogOnBreak,
	                       UnsafeRunnable beforeBuildRunnable,
	                       RebuildCause rebuildCause) {
		if(!isRebuildLocked) {
			Handler.showErrorDialogOnBreak = showErrorDialogOnBreak;
			GraphPanel.getInstance().setLockRepaint(true);
			try {
				if(beforeBuildRunnable != null) {
					beforeBuildRunnable.run(rebuildCause);
				}
				new Thread(rebuildAllRunnable).start();
			} catch (Exception e) {
				handleException(e);
			}
		}
	}

	private static final Runnable rebuildAllRunnable = new Runnable() {

		public void run() {
			try {
				List<String> variablesInput = variablesHandler.loadVariablesInput();
				List<String> functionsInput = functionsHandler.loadFunctionsInput();
				List<String> axisesMinInput = axisesHandler.loadAxisesMinInput();
				List<String> axisesMaxInput = axisesHandler.loadAxisesMaxInput();
				List<AxisUnReferencableInputExpressions> unReferencableInput = axisesHandler.loadAxisesUnReferencableInput();
				viewPortsHandler.loadViewPortsAxises();
				ExpressionBuilder.getInstance().buildExpressions(functionsInput,
						variablesInput,
						axisesMinInput,
						axisesMaxInput,
						unReferencableInput);
				SwingUtilities.invokeLater(applyRebuildToUIRunnable);
			} catch (Exception e) {
				handleException(e);
			}
		}
	};

	private static final Runnable applyRebuildToUIRunnable = new Runnable() {

		public void run() {
			try {
				variablesHandler.reloadVariables();
				axisesHandler.reloadAxis();

				functionsHandler.reInitiateFunctionComponents();
				viewPortsHandler.reinitiateViewPorts();
				variablesHandler.fireExpressionsRebuilt();
				axisesHandler.fireExpressionsRebuilt();

				GraphPanel.getInstance().setBroken(false);
				GraphPanel.getInstance().setLockRepaint(false);
				GraphPanel.getInstance().repaint();
			} catch (Exception e) {
				handleException(e);
			}
		}
	};

	private static void handleException(Exception e) {
		if(showErrorDialogOnBreak) {
			ErrorDialog.showErrorDialog(e);
		} else {
			GraphPanel.getInstance().setBroken(true);
		}
		GraphPanel.getInstance().setLockRepaint(false);
	}

	protected abstract void updateExpressions(ExpressionProcessor expressionProcessor);


	/**
	 * not static for easy mocking when testing
	 * @param fIntersectionsPanel1
	 */
	public void setfIntersectionsPanel1(IndexedTogglesPanel<FunctionComponent> fIntersectionsPanel1) {
		Handler.fIntersectionsPanel1 = fIntersectionsPanel1;
	}

	/**
	 * not static for easy mocking when testing
	 * @param intersectionsColorPanel
	 */
	public void setIntersectionsColorPanel(IntersectionsColorPanel intersectionsColorPanel) {
		Handler.intersectionsColorPanel = intersectionsColorPanel;
	}


	public String trimText(String text) {
		return text.trim().replaceAll("\\s+"," ");
	}

	private boolean codeChanged(RebuildExpressionInput rebuildExpressionInput) {
		String trimmedText = trimText(rebuildExpressionInput.getText()),
		lastTrimmedText = rebuildExpressionInput.getLastTrimmedText();
		if(!trimmedText.equals(lastTrimmedText)) {
			rebuildExpressionInput.setLastTrimmedText(trimmedText);
			return true;
		}
		return false;
	}


	protected void setupRebuildExpressionInputsHolder(RebuildExpressionInputsHolder rebuildExpressionInputsHolder) {
		for(final RebuildExpressionInput expressionInput : rebuildExpressionInputsHolder.getRebuildExpressionInputs()) {
			expressionInput.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					if(codeChanged(expressionInput) && !lockFocusLostEvents) {
						rebuildAll(false, expressionInput.getBeforeBuildRunnable(), RebuildCause.FOCUS_LOST);
					}
				}
			});

			expressionInput.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if(expressionInput.isValidateKey(e)) {
						codeChanged(expressionInput);
						rebuildAll(true, expressionInput.getBeforeBuildRunnable(), RebuildCause.VALIDATE_ACTION_TRIGGERED);
					}
				}
			});
		}
	}

	public void setRebuildLocked(boolean isRebuildLocked) {
		Handler.isRebuildLocked = isRebuildLocked;
	}

	public void stopSpinningFieldsIfAny() {
		variablesHandler.stopVariablesSpinningFieldsIfAny();
		axisesHandler.stopAxisesSpinningFieldsIfAny();
	}

	public void setLockFocusLostEvents(boolean lockFocusLostEvents) {
		Handler.lockFocusLostEvents = lockFocusLostEvents;
	}
}
