package com.alayouni.handyplotter.ui.handlers;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessorImpl;
import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.VariableComponent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsChangeEvent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsListener;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 10:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class VariablesHandler extends Handler {
	private final MultiSplitPane<VariableComponent> variablesSplit;
	protected final ExpressionProcessorImpl variablesExpressionProcessor = new ExpressionProcessorImpl(VariableComponent.VARIABLE_PREFIX);

	protected static final ItemsListener<VariableComponent> MULTI_SPLIT_INDEXES_LISTENER = createSplitIndexesListener();

	public VariablesHandler(MultiSplitPane<VariableComponent> variablesSplit) {
		this.variablesSplit = variablesSplit;
		variablesHandler = this;
		initVariablesSplitListeners();
	}

	private void initVariablesSplitListeners() {
		variablesSplit.addItemsListener(MULTI_SPLIT_INDEXES_LISTENER);
		variablesSplit.addItemsListener(new ItemsListener<VariableComponent>() {

			public void itemsChanged(ItemsChangeEvent<VariableComponent> e) {
				int changedVariableIndex = e.getItemIndex(),
						variablesCount = variablesSplit.getItems().size();
				if(e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
					VariableComponent variableComponent = e.getComponent();
					setupRebuildExpressionInputsHolder(variableComponent);

					variablesExpressionProcessor.buildItemAddedExpressionReplacements(changedVariableIndex, variablesCount);
				} else {
					variablesExpressionProcessor.buildItemRemovedExpressionReplacements(changedVariableIndex, variablesCount);
				}
				updateExpressionHolders(variablesExpressionProcessor);
				rebuildAll(false, null, RebuildCause.VARIABLES_CHANGE);
			}
		});
	}

	protected List<String> loadVariablesInput() throws Exception {
		List<String> variablesInput = new ArrayList<String>();
		for(VariableComponent variableComponent : variablesSplit.getItems()) {
			variablesInput.add(variableComponent.getExpression());
		}
		return variablesInput;
	}

	protected void reloadVariables() {
		for(VariableComponent variableComponent : variablesSplit.getItems()) {
			variableComponent.reloadVariable();
		}
	}

	protected void fireExpressionsRebuilt() throws Exception {
		for(VariableComponent variableComponent : variablesSplit.getItems()) {
			variableComponent.fireExpressionsRebuilt();
		}
	}

	@Override
	protected void updateExpressions(ExpressionProcessor expressionProcessor) {
		for(VariableComponent variableComponent : variablesSplit.getItems()) {
			variableComponent.updateExpressions(expressionProcessor);
		}
	}

	protected void stopVariablesSpinningFieldsIfAny() {
		for(VariableComponent variableComponent : variablesSplit.getItems()) {
			variableComponent.stopSpinningFieldIfRunning();
		}
	}
}
