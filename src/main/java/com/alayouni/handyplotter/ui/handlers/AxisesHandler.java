package com.alayouni.handyplotter.ui.handlers;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessorImpl;
import com.alayouni.handyplotter.ui.components.containers.IndexedTogglesPanel;
import com.alayouni.handyplotter.ui.components.other.ColorToggle;
import com.alayouni.handyplotter.ui.components.other.ColorableToggle;
import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.Colorable;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisUnReferencableInputExpressions;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsChangeEvent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsListener;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 11:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisesHandler extends Handler {
	private final MultiSplitPane<AxisComponent> axisesSplit;

	private final IndexedTogglesPanel<AxisComponent> axisesColorPanel;
	protected final ExpressionProcessorImpl axisesExpressionProcessor = new ExpressionProcessorImpl(AxisComponent.AXIS_PREFIX);

	protected static final ItemsListener<AxisComponent> MULTI_SPLIT_INDEXES_LISTENER = createSplitIndexesListener();

	public AxisesHandler(IndexedTogglesPanel<AxisComponent> axisesColorPanel,
	                        MultiSplitPane<AxisComponent> axisesSplit) {
		this.axisesColorPanel = axisesColorPanel;
		this.axisesSplit = axisesSplit;
		initAxisesSplitListeners();
		axisesHandler = this;
	}

	private void initAxisesSplitListeners() {
		axisesSplit.addItemsListener(MULTI_SPLIT_INDEXES_LISTENER);
		axisesSplit.addItemsListener(new ItemsListener<AxisComponent>() {

			public void itemsChanged(ItemsChangeEvent<AxisComponent> e) {
				try {
					if (e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
						itemAdded(e);
					} else {
						itemRemoved(e);
					}
					viewPortsHandler.updateExpressions(axisesExpressionProcessor);
					intersectionsColorPanel.repaint();
					rebuildAll(false, null, RebuildCause.AXIS_ITEMS_CHANGE);
				} catch(Exception e1) {
					GraphPanel.getInstance().setBroken(true);
				}

			}

			private void itemAdded(ItemsChangeEvent<AxisComponent> e) {
				AxisComponent axisComponent = e.getComponent();
				setupRebuildExpressionInputsHolder(axisComponent);

				ColorableToggle <AxisComponent> axisMappingToggle = new ColorableToggle<AxisComponent>(axisComponent);

				List<Colorable> colorables = new ArrayList<Colorable>();
				colorables.add(axisComponent);
				colorables.add(axisMappingToggle);
				ColorToggle<AxisComponent> axisColorToggle = new ColorToggle<AxisComponent>(colorables, axisComponent);
				axisesColorPanel.addToggle(axisColorToggle, e.getItemIndex());

				int addedAxisIndex = e.getItemIndex(),
						axisCount = axisesSplit.getItems().size();
				axisesExpressionProcessor.buildItemAddedExpressionReplacements(addedAxisIndex, axisCount);
			}

			private void itemRemoved(ItemsChangeEvent<AxisComponent> e) throws Exception {
				AxisComponent axisComponent = e.getComponent();
				axisesColorPanel.removeToggle(axisComponent);
				ViewPortComponent viewPortComponent = viewPortsHandler.findReferringViewPortIfAny(axisComponent);
				if(viewPortComponent != null) {
					throw new Exception("Removed axis " + axisComponent +
							" is being referred by view port " + viewPortComponent);
				} else {
					int removedAxisIndex = e.getItemIndex(),
							axisCount = axisesSplit.getItems().size();
					axisesExpressionProcessor.buildItemRemovedExpressionReplacements(removedAxisIndex, axisCount);
				}
			}
		});
	}

	protected void reloadAxis() {
		for(AxisComponent axisComponent : axisesSplit.getItems()) {
			axisComponent.reloadAxis();
		}
	}

	protected void fireExpressionsRebuilt() throws Exception {
		for(AxisComponent axisComponent : axisesSplit.getItems()) {
			axisComponent.fireExpressionsRebuilt();
		}
	}

	@Override
	protected void updateExpressions(ExpressionProcessor expressionProcessor) {
		for(AxisComponent axis : axisesSplit.getItems()) {
			axis.updateExpressions(expressionProcessor);
		}
	}

	protected List<String> loadAxisesMinInput() throws Exception {
		List<String> axisesMinInput = new ArrayList<String>();
		for(AxisComponent axisComponent : axisesSplit.getItems()) {
			axisesMinInput.add(axisComponent.getMinExpression());
		}
		return axisesMinInput;
	}

	protected List<String> loadAxisesMaxInput() throws Exception {
		List<String> axisesMaxInput = new ArrayList<String>();
		for(AxisComponent axisComponent : axisesSplit.getItems()) {
			axisesMaxInput.add(axisComponent.getMaxExpression());
		}
		return axisesMaxInput;
	}

	protected List<AxisUnReferencableInputExpressions> loadAxisesUnReferencableInput() throws Exception {
		List<AxisUnReferencableInputExpressions> unReferencableInputs = new ArrayList<AxisUnReferencableInputExpressions>();
		String expr;
		AxisUnReferencableInputExpressions unReferencableInput;
		for(AxisComponent axisComponent : axisesSplit.getItems()) {
			unReferencableInput = new AxisUnReferencableInputExpressions();

			expr = axisComponent.getLocationExpression();
			unReferencableInput.setLocationExpression(expr);

			expr = axisComponent.getOffsetExpression();
			unReferencableInput.setOffsetExpression(expr);

			expr = axisComponent.getUnitExpression();
			unReferencableInput.setUnitExpression(expr);

			unReferencableInputs.add(unReferencableInput);

		}
		return unReferencableInputs;
	}

	protected void stopAxisesSpinningFieldsIfAny() {
		for(AxisComponent axis : axisesSplit.getItems()) {
			axis.stopSpinningFieldsIfAny();
		}
	}
}
