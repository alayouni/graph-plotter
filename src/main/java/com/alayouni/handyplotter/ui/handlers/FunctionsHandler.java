package com.alayouni.handyplotter.ui.handlers;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessorImpl;
import com.alayouni.handyplotter.ui.components.containers.IndexedTogglesPanel;
import com.alayouni.handyplotter.ui.components.other.*;
import com.alayouni.handyplotter.ui.components.splitpane.items.Colorable;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsChangeEvent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsListener;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class FunctionsHandler extends Handler {
	private final IndexedTogglesPanel<FunctionComponent> fIntersectionsPanel2;
	private final MultiSplitPane<FunctionComponent> functionsSplit;
	private final IndexedTogglesPanel<FunctionComponent> fColorPanel;

	protected final ExpressionProcessorImpl functionsExpressionProcessor = new ExpressionProcessorImpl(FunctionComponent.FUNCTION_PREFIX);

	protected static final ItemsListener<FunctionComponent> MULTI_SPLIT_INDEXES_LISTENER = createSplitIndexesListener();

	public FunctionsHandler(IndexedTogglesPanel<FunctionComponent> fColorPanel,
	                        IndexedTogglesPanel<FunctionComponent> fIntersectionsPanel2,
	                        MultiSplitPane<FunctionComponent> functionsSplit) {
		this.fColorPanel = fColorPanel;
		this.fIntersectionsPanel2 = fIntersectionsPanel2;
		this.functionsSplit = functionsSplit;
		functionsHandler = this;
		initFunctionsSplitListeners();
	}

	private void initFunctionsSplitListeners() {
		functionsSplit.addItemsListener(MULTI_SPLIT_INDEXES_LISTENER);
		functionsSplit.addItemsListener(new ItemsListener<FunctionComponent>() {

			public void itemsChanged(ItemsChangeEvent<FunctionComponent> e) {
				if(e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
					itemAdded(e);
				} else {
					itemRemoved(e);
				}
				updateExpressionHolders(functionsExpressionProcessor);
				intersectionsColorPanel.repaint();
				rebuildAll(false, null, RebuildCause.FUNCTION_ITEMS_CHANGE);
			}

			private void itemAdded(ItemsChangeEvent<FunctionComponent> e) {
				int addedFunctionIndex = e.getItemIndex(),
						functionsCount = functionsSplit.getItems().size();
				FunctionComponent fComp = e.getComponent();
				setupRebuildExpressionInputsHolder(fComp);

				final ColorableToggle<FunctionComponent> fIntersection1 = new ColorableToggle<FunctionComponent>(fComp);
				fIntersectionsPanel1.addToggle(fIntersection1, addedFunctionIndex);

				final ColorableToggle<FunctionComponent> fIntersection2 = new ColorableToggle<FunctionComponent>(fComp);
				fIntersectionsPanel2.addToggle(fIntersection2, addedFunctionIndex);
				setupIntersection1Listener(fIntersection1, fIntersection2);
				setupFIntersection2Listener(fIntersection2);

				List<Colorable> colorables = viewPortsHandler.addAndMapAddedFunctionToAllViewPorts(fComp);
				colorables.add(fComp);
				colorables.add(fIntersection1);
				colorables.add(fIntersection2);
				ColorToggle<FunctionComponent> fColorToggle = new ColorToggle<FunctionComponent>(colorables, fComp);
				fColorPanel.addToggle(fColorToggle, addedFunctionIndex);

				functionsExpressionProcessor.buildItemRemovedExpressionReplacements(addedFunctionIndex, functionsCount);
			}

			private void itemRemoved(ItemsChangeEvent<FunctionComponent> e) {
				FunctionComponent removedFunctionComponent = e.getComponent();
				fIntersectionsPanel1.removeToggle(removedFunctionComponent);
				fIntersectionsPanel2.removeToggle(removedFunctionComponent);
				fColorPanel.removeToggle(removedFunctionComponent);
				intersectionsColorPanel.functionRemoved(removedFunctionComponent);
				viewPortsHandler.fireFunctionRemoved(removedFunctionComponent);

				int removedFunctionIndex = e.getItemIndex(),
						functionsCount = functionsSplit.getItems().size();
				functionsExpressionProcessor.buildItemRemovedExpressionReplacements(removedFunctionIndex, functionsCount);
			}
		});
	}


	private void setupIntersection1Listener(final ColorableToggle<FunctionComponent> fIntersection1,
	                                        final ColorableToggle<FunctionComponent> fIntersection2) {
		fIntersection1.addActionListener(new ActionListener() {//ActionListener to detect only events triggered by mouse

			public void actionPerformed(ActionEvent e) {
				if(fIntersection1.isSelected()) {
					fIntersectionsPanel2.resetAll();
					fIntersection2.setEnabled(false);
					FunctionComponent fComp = fIntersection1.getDependencyComponent();
					selectFIntersections(fComp);
				}
			}

			private void selectFIntersections(FunctionComponent fComp) {
				FunctionComponent intersectionFComp;
				IndexedToggle<FunctionComponent> toggle;
				for(IntersectionColorToggle<FunctionComponent> intersection : fComp.getFIntersections()) {
					if(!intersection.isHidden()) {
						intersectionFComp = intersection.getIntersectionComponent();
						if(intersectionFComp == fComp) {
							intersectionFComp = intersection.getDependencyComponent();
						}
						toggle = fIntersectionsPanel2.findToggleForComponent(intersectionFComp);
						toggle.setSelected(true);
					}
				}
			}
		});
	}

	private void setupFIntersection2Listener(final ColorableToggle<FunctionComponent> fIntersection2) {
		fIntersection2.addActionListener(new ActionListener() {//ActionListener to detect only events triggered by mouse

			public void actionPerformed(ActionEvent e) {//ActionListener to detect only events triggered by mouse
				IndexedToggle<FunctionComponent> fIntersection = fIntersectionsPanel1.getSelectedToggle();
				if(fIntersection != null) {
					if(fIntersection2.isSelected()) {
						intersectionsColorPanel.addFIntersection(fIntersection.getDependencyComponent(),
								fIntersection2.getDependencyComponent());
					} else {
						intersectionsColorPanel.removeFIntersection(fIntersection.getDependencyComponent(),
								fIntersection2.getDependencyComponent());
					}
					GraphPanel.getInstance().repaint();
				}
			}
		});
	}


	protected List<String> loadFunctionsInput() {
		List<String> functionsInput = new ArrayList<String>();
		for(FunctionComponent functionComponent : functionsSplit.getItems()) {
			functionsInput.add(functionComponent.getExpression());
		}
		return functionsInput;
	}

	protected void reInitiateFunctionComponents() {
		for(FunctionComponent functionComponent : functionsSplit.getItems()) {
			functionComponent.reInitiateFunction();
		}
	}


	@Override
	protected void updateExpressions(ExpressionProcessor expressionProcessor) {
		for(FunctionComponent functionComponent : functionsSplit.getItems()) {
			functionComponent.updateExpressions(expressionProcessor);
		}
	}

	public List<FunctionComponent> getFunctionComponents() {
		return functionsSplit.getItems();
	}

	public ColorToggle<FunctionComponent> getFunctionColorToggle(FunctionComponent fComp) {
		return (ColorToggle<FunctionComponent>) fColorPanel.findToggleForComponent(fComp);
	}
}
