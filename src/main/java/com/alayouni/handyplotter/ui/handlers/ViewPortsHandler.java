package com.alayouni.handyplotter.ui.handlers;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessorImpl;
import com.alayouni.handyplotter.ui.components.other.ColorToggle;
import com.alayouni.handyplotter.ui.components.other.ColorableToggle;
import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.Colorable;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.viewport.ViewPortPanel;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsChangeEvent;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemsListener;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/31/13
 * Time: 9:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortsHandler extends Handler {
	private MultiSplitPane<ViewPortComponent> viewPortsSplit;
	private final GraphPanel graphPanel;

	protected final ExpressionProcessorImpl viewPortsExpressionProcessor = new ExpressionProcessorImpl(ViewPortComponent.VIEW_PORT_PREFIX);

	protected static final ItemsListener<ViewPortComponent> MULTI_SPLIT_INDEXES_LISTENER = createSplitIndexesListener();

	public ViewPortsHandler(MultiSplitPane<ViewPortComponent> viewPortsSplit) {
		this.viewPortsSplit = viewPortsSplit;
		graphPanel = GraphPanel.getInstance();
		viewPortsHandler = this;
		initViewPortsSplitListeners();
	}

	private void initViewPortsSplitListeners() {
		this.viewPortsSplit.addItemsListener(MULTI_SPLIT_INDEXES_LISTENER);
		this.viewPortsSplit.addItemsListener(new ItemsListener<ViewPortComponent>() {

			public void itemsChanged(ItemsChangeEvent<ViewPortComponent> e) {
				if(e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
					itemAdded(e);
				} else {
					itemRemoved(e);
				}
				updateExpressionHolders(viewPortsExpressionProcessor);
				rebuildAll(false, null, RebuildCause.VIEW_PORT_ITEMS_CHANGE);
			}

			private void itemAdded(ItemsChangeEvent<ViewPortComponent> e) {
				int addedViewPortIndex = e.getItemIndex(),
						viewPortsCount = viewPortsSplit.getItems().size();
				final ViewPortComponent viewPortComponent = e.getComponent();
				setupRebuildExpressionInputsHolder(viewPortComponent);

				ViewPortPanel viewPortPanel = graphPanel.addViewPortPanel(viewPortComponent, ViewPortsHandler.this);
				viewPortComponent.setViewPortPanel(viewPortPanel);

				mapAllFunctionsToAddedViewPort(viewPortComponent);

				viewPortsExpressionProcessor.buildItemAddedExpressionReplacements(addedViewPortIndex, viewPortsCount);
			}

			private void itemRemoved(ItemsChangeEvent<ViewPortComponent> e) {
				ViewPortComponent viewPortComponent = e.getComponent();
				graphPanel.removeViewPortPanel(viewPortComponent);

				unMapAllFunctionsFromRemovedViewPort(viewPortComponent);

				int removedViewPortIndex  = e.getItemIndex(),
						viewPortsCount = viewPortsSplit.getItems().size();
				viewPortsExpressionProcessor.buildItemRemovedExpressionReplacements(removedViewPortIndex, viewPortsCount);
			}
		});
	}

	private void mapAllFunctionsToAddedViewPort(ViewPortComponent viewPortComponent) {
		List<FunctionComponent> functionComponentList = functionsHandler.getFunctionComponents();
		ViewPortPanel viewPortPanel = viewPortComponent.getViewPortPanel();
		ColorToggle<FunctionComponent> fColorToggle;
		ColorableToggle<FunctionComponent> fColorableToggle;
		for(FunctionComponent fComp : functionComponentList) {
			fColorToggle = functionsHandler.getFunctionColorToggle(fComp);
			fColorableToggle = viewPortPanel.addFunctionToggle(fComp);
			setupFMappingToggleListener(viewPortComponent, fColorableToggle);
			fColorToggle.addDependentColorable(fColorableToggle);
			viewPortComponent.addMappedFunction(fComp);
		}
	}

	private void unMapAllFunctionsFromRemovedViewPort(ViewPortComponent viewPortComponent) {
		//no need to un-map since the view port is gonna be removed, just remove corresponding
		//function colorables from the list of dependent colorables ... see code below
		List<FunctionComponent> functionComponentList = functionsHandler.getFunctionComponents();
		ViewPortPanel viewPortPanel = viewPortComponent.getViewPortPanel();
		ColorToggle<FunctionComponent> fColorToggle;
		ColorableToggle<FunctionComponent> fColorableToggle;
		for(FunctionComponent fComp : functionComponentList) {
			fColorToggle = functionsHandler.getFunctionColorToggle(fComp);
			fColorableToggle = viewPortPanel.getFunctionColorableToggle(fComp);
			fColorToggle.removeDependentColorable(fColorableToggle);
		}
	}

	public List<Colorable> addAndMapAddedFunctionToAllViewPorts(final FunctionComponent fComp) {
		ViewPortPanel viewPortPanel;
		List<Colorable> fColorableToggles = new ArrayList<Colorable>();
		ColorableToggle<FunctionComponent> fColorableToggle;
		for(final ViewPortComponent viewPortComponent : viewPortsSplit.getItems()) {
			viewPortPanel = viewPortComponent.getViewPortPanel();

			fColorableToggle = viewPortPanel.addFunctionToggle(fComp);
			setupFMappingToggleListener(viewPortComponent, fColorableToggle);

			fColorableToggles.add(fColorableToggle);
			viewPortComponent.addMappedFunction(fComp);
		}
		return fColorableToggles;
	}

	public void fireFunctionRemoved(FunctionComponent functionComponent) {
		ViewPortPanel viewPortPanel;
		for(ViewPortComponent viewPortComponent : viewPortsSplit.getItems()) {
			viewPortPanel = viewPortComponent.getViewPortPanel();
			viewPortPanel.removeFunctionToggle(functionComponent);
			viewPortComponent.removeMappedFunction(functionComponent);
		}
	}

	private void setupFMappingToggleListener(final ViewPortComponent viewPortComponent,
	                                         final ColorableToggle<FunctionComponent> fColorableToggle) {
		fColorableToggle.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				FunctionComponent fComp = fColorableToggle.getDependencyComponent();
				if(fColorableToggle.isSelected()) {
					viewPortComponent.addMappedFunction(fComp);
				} else {
					viewPortComponent.removeMappedFunction(fComp);
				}
				rebuildAll(false, null, RebuildCause.VIEW_PORTS_MAPPING_CHANGE);
			}
		});

	}

	public ViewPortComponent findReferringViewPortIfAny(AxisComponent axisComponent) {
		for(ViewPortComponent viewPortComponent : viewPortsSplit.getItems()) {
			if(viewPortComponent.getHAxis() == axisComponent) {
				return viewPortComponent;
			}
			if(viewPortComponent.getVAxis() == axisComponent) {
				return viewPortComponent;
			}
		}
		return null;
	}

	@Override
	protected void updateExpressions(ExpressionProcessor expressionProcessor) {
		for(ViewPortComponent viewPortComponent : viewPortsSplit.getItems()) {
			viewPortComponent.updateExpressions(expressionProcessor);
		}
	}

	public void reinitiateViewPorts() throws Exception {
		for(ViewPortComponent viewPortComponent : viewPortsSplit.getItems()) {
			viewPortComponent.reloadParams();
		}
	}

	public List<FunctionComponent> getAllFunctions() {
		return functionsHandler.getFunctionComponents();
	}

	public void loadViewPortsAxises() throws Exception {
		for(ViewPortComponent viewPort : viewPortsSplit.getItems()) {
			viewPort.readAxisesInput();
		}
	}
}
