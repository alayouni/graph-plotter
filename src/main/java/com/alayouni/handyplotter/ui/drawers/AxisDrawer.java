package com.alayouni.handyplotter.ui.drawers;

import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisDynamicLocation;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AxisDrawer extends Drawer {
	//for perfect symmetry should be an even number
	protected static final int GRADUATION_TICK_LENGTH = 8;

	protected static final Color GRID_COLOR = Color.LIGHT_GRAY;

	protected static class TicksDef {
		Double startTickValue = null;
		Double scaledScreenStartTick = null;
		Double unit = null;
	}



	/**
	 * assumes axis.min < axis.max
	 * @return
	 */
	protected TicksDef computeTicksDef(AxisComponent axis, Double orthogonalAxisLocation) throws Exception {
		TicksDef ticksDef = new TicksDef();
		Double maxAxis = axis.getMaxValue(),
				minAxis = axis.getMinValue(),
				offset = axis.getOffsetValue();
		ticksDef.unit = axis.getUnitValue();

		if(axis.isOffsetAuto()) {
			offset = orthogonalAxisLocation;
		}

		if(ticksDef.unit == null || maxAxis == null || minAxis == null || offset == null) {
			return null;
		}
		ticksDef.unit = Math.abs(ticksDef.unit);

		long k = Math.round((minAxis - offset) / ticksDef.unit);
		ticksDef.startTickValue = offset + k * ticksDef.unit;
		if(ticksDef.startTickValue < minAxis) {
			ticksDef.startTickValue += ticksDef.unit;
		}
		if(ticksDef.startTickValue > maxAxis) {
			//ticksDef.scaledScreenStartTick is null it'll indicate that no graduation will be visible
			return ticksDef;
		}
		ticksDef.scaledScreenStartTick = ticksDef.startTickValue - minAxis;

		return ticksDef;
	}

	public double computeAxisLocation(AxisComponent axis, AxisComponent orthogonalAxis) throws Exception {
		AxisDynamicLocation dynamicLocation = axis.parseDynamicLocation();
		double location;
		if(dynamicLocation != null) {
			location = computeDynamicAxisLocation(orthogonalAxis, dynamicLocation);
			return location;
		} else {
			location = axis.getLocationValue();
			return location;
		}
	}

	private double computeDynamicAxisLocation(AxisComponent orthogonalAxis, AxisDynamicLocation dynamicLocation) {
		double min = orthogonalAxis.getMinValue(),
				max = orthogonalAxis.getMaxValue();
		switch (dynamicLocation) {
			case MIN:
				return min;
			case MAX:
				return max;
			default:
				return min + (max - min) / 2;
		}
	}

	public abstract int graduateAxis(Graphics g,
	                                 AxisComponent axis,
	                                 double axisLocation,
	                                 double orthogonalAxisLocation,
	                                 boolean hideAxis,
	                                 boolean showGrid) throws Exception;

	public abstract void drawAxis(Graphics g,
	                              AxisComponent axis,
	                              int locationPixel);
}
