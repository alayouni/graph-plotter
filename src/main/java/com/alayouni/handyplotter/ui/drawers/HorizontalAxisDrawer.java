package com.alayouni.handyplotter.ui.drawers;

import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisDynamicLocation;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class HorizontalAxisDrawer extends AxisDrawer {

	@Override
	public int graduateAxis(Graphics g,
	                        AxisComponent hAxis,
	                        double hAxisLocation,
	                        double vAxisLocation,
	                        boolean hideHAxis,
	                        boolean showVGrid) throws Exception {
		int yPixel = (int)((hAxisLocation - minY) * ratioY);

		TicksDef ticksDef = this.computeTicksDef(hAxis, vAxisLocation);

		int y1 = yPixel - GRADUATION_TICK_LENGTH / 2,
		y2 = y1 + GRADUATION_TICK_LENGTH;
		final int yStr = computeYStr(yPixel, hAxis);
		if(showVGrid) {
			y1 = 0;
			y2 = height;
		}
		final double unit = ticksDef.unit,
				scaledScreenLength = hAxis.getMaxValue() - hAxis.getMinValue();
		for(double screenTick = ticksDef.scaledScreenStartTick, axisTickValue = ticksDef.startTickValue;
		    screenTick < scaledScreenLength + xStep;
		    screenTick += unit, axisTickValue += unit) {

			int xPixel = (int)(screenTick * ratioX);
			if(xPixel == width) {
				xPixel -= 1;
			}
			if(showVGrid) {
				g.setColor(GRID_COLOR);
			}
			this.drawLine(xPixel, y1, xPixel, y2, g);

			if(hAxis.isShowTicksValues() && !hideHAxis) {
				g.setColor(hAxis.getColor());
				String xStr = hAxis.getDecimalFormatter().format(axisTickValue, xStep);
				//not centering the text for performance
				this.drawString(xStr, xPixel, yStr, g);
			}
		}
		return yPixel;
	}

	private int computeYStr(int yPixel, AxisComponent hAxis) throws Exception {
		AxisDynamicLocation dynamicLocation = hAxis.parseDynamicLocation();
		if(dynamicLocation == AxisDynamicLocation.MIN) {
			//(FONT_SIZE / 3 ) as an estimation of the baseline location
			return yPixel + GRADUATION_TICK_LENGTH / 2 + FONT_SIZE / 3 + 2;
		} else {
			return yPixel - GRADUATION_TICK_LENGTH / 2 - FONT_SIZE - 2;
		}
	}

	@Override
	public void drawAxis(Graphics g, AxisComponent hAxis, int yPixel) {
		if(yPixel <= height && yPixel >= 0) {
			g.setColor(hAxis.getColor());
			this.drawLine(0, yPixel, width, yPixel, g);
		}
	}

}
