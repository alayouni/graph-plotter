package com.alayouni.handyplotter.ui.drawers;

import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisDynamicLocation;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class VerticalAxisDrawer extends AxisDrawer {


	@Override
	public void drawAxis(Graphics g, AxisComponent vAxis, int xPixel) {
		if(xPixel <= width && xPixel >= 0) {
			g.setColor(vAxis.getColor());
			this.drawLine(xPixel, 0, xPixel, height, g);
		}
	}

	@Override
	public int graduateAxis(Graphics g,
	                        AxisComponent vAxis,
	                        double vAxisLocation,
	                        double hAxisLocation,
	                        boolean hideVAxis,
	                        boolean showHGrid) throws Exception {
		int xPixel = (int)((vAxisLocation - minX) * ratioX);

		TicksDef ticksDef = this.computeTicksDef(vAxis, hAxisLocation);

		int x1 = xPixel - GRADUATION_TICK_LENGTH / 2,
		x2 = x1 + GRADUATION_TICK_LENGTH;
		int xStr = x2 + 2;
		if(showHGrid) {
			x1 = 0;
			x2 = width;
		}
		final double unit = ticksDef.unit,
				scaledScreenLength = vAxis.getMaxValue() - vAxis.getMinValue();

		//parsing the dynamic location might affect performance => calculating it out of the loop
		boolean isMaxDynamicLocation = vAxis.parseDynamicLocation() == AxisDynamicLocation.MAX;
		for(double screenTick = ticksDef.scaledScreenStartTick, axisTickValue = ticksDef.startTickValue;
		    screenTick < scaledScreenLength + yStep;
		    screenTick += unit, axisTickValue += unit) {

			int yPixel = (int)(screenTick * ratioY);
			if(yPixel == 0) {
				yPixel += 1;
			}
			if(showHGrid) {
				g.setColor(GRID_COLOR);
			}

			this.drawLine(x1, yPixel, x2, yPixel, g);

			if(vAxis.isShowTicksValues() && !hideVAxis) {
				g.setColor(vAxis.getColor());
				String yStr = vAxis.getDecimalFormatter().format(axisTickValue, yStep);
				//not centering the text just for performance
				if(isMaxDynamicLocation) {
					xStr = this.computeXStr(yStr, g, xPixel);
				}
				this.drawString(yStr, xStr, yPixel, g);
			}
		}
		return xPixel;
	}

	private int computeXStr(String yStr, Graphics g, int xPixel) throws Exception {
		int strWidth = g.getFontMetrics().stringWidth(yStr);
		return xPixel - GRADUATION_TICK_LENGTH / 2 - strWidth - 2;
	}

}
