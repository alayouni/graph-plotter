package com.alayouni.handyplotter.ui.drawers;

import com.alayouni.handyplotter.expressionutils.MathUtils;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.viewport.ViewPortPanel;
import com.alayouni.handyplotter.utils.DecimalFormatter;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Drawer {
	protected static final int FONT_SIZE = 10;
	protected static final Font TEXT_FONT = new Font(Font.SANS_SERIF, Font.BOLD, FONT_SIZE);

	protected static List<FunctionComponent> mappedFComponents;
	protected static List<FunctionComponent> allFComponents;

	protected static DecimalFormatter xDecimalFormatter;
	protected static DecimalFormatter yDecimalFormatter;

	//for performance access to the following should be direct
	protected static double minX;
	protected static double minY;
	protected static double maxX;
	protected static double maxY;
	protected static double ratioX;
	protected static double ratioY;

	protected static double xStep;
	protected static double yStep;

	protected static int width;
	protected static int height;

	/**
	 * will be set depending on unitX sign
	 */
	private static boolean reverseX = false;

	/**
	 * will be set depending on unitY sign
	 */
	private static boolean reverseY = true;

	/**
	 * to improve performance should be calculated only once on reInit
	 */
	private static int stringHeightToAddOnNonReverseY = 0;

	/**
	 * not static for easy mocking when testing
	 * @param g
	 */
	public void reInit(Graphics g, ViewPortComponent viewPortComp, ViewPortPanel viewPortPanel) throws Exception {
		List<FunctionComponent> fComps = viewPortComp.getMappedFunctions();
		this.reInitFunctionsMapping(fComps);
		reInitIntersections();

		width = viewPortPanel.getWidth();
		height = viewPortPanel.getHeight();

		AxisComponent hAxis = viewPortComp.getHAxis();
		AxisComponent vAxis = viewPortComp.getVAxis();
		minX = hAxis.getMinValue();
		minY = vAxis.getMinValue();
		maxX = hAxis.getMaxValue();
		maxY = vAxis.getMaxValue();
		ratioX = width / (maxX - minX);
		ratioY = height / (maxY - minY);

		xStep = 1 / ratioX;
		yStep = 1 / ratioY;
		MathUtils.PIXEL_WIDTH = Math.abs(xStep);
		MathUtils.PIXEL_HEIGHT = Math.abs(yStep);

		xDecimalFormatter = hAxis.getDecimalFormatter();
		yDecimalFormatter = vAxis.getDecimalFormatter();

		reverseX = hAxis.getUnitValue() < 0;
		reverseY = vAxis.getUnitValue() > 0;
		g.setFont(TEXT_FONT);

		FontMetrics metrics = g.getFontMetrics();
		stringHeightToAddOnNonReverseY = (metrics.getAscent() - metrics.getDescent());
	}

	private void reInitFunctionsMapping(List<FunctionComponent> fComps) {
		for(FunctionComponent fComp : allFComponents) {
			fComp.setInViewPort(false);
		}
		mappedFComponents = fComps;
		for(FunctionComponent fComp : mappedFComponents) {
			fComp.setInViewPort(true);
		}
	}

	private void reInitIntersections() {
		for(FunctionComponent functionComponent : mappedFComponents) {
			functionComponent.reInitIntersections();
		}
	}

	/**
	 * not static for easy mocking when testing
	 * @param x
	 * @param y
	 * @param g
	 */
	protected void drawPixel(int x, int y, Graphics g) {
		g.drawLine(reverseX ? width - x : x,
				reverseY ? height - y : y,
				reverseX ? width - x : x,
				reverseY ? height - y : y);
	}

	/**
	 * not static for easy mocking when testing
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param g
	 */
	protected void drawLine(int x1, int y1, int x2, int y2, Graphics g) {
		g.drawLine(reverseX ? width - x1 : x1,
				reverseY ? height - y1 : y1,
				reverseX ? width - x2 : x2,
				reverseY ? height - y2 : y2);
	}

	/**
	 * not static for easy mocking when testing
	 * @param str
	 * @param x
	 * @param y
	 * @param g
	 */
	protected void drawString(String str, int x, int y, Graphics g) {
		if(reverseX) {
			int stringWidth = g.getFontMetrics().stringWidth(str);
			x += stringWidth;
		}
		if(!reverseY) {
			y += stringHeightToAddOnNonReverseY;
		}
		g.drawString(str,
				reverseX ? width - x : x,
				reverseY ? height - y : y);
	}

	public void setAllFunctions(List<FunctionComponent> allFComponents) {
		Drawer.allFComponents = allFComponents;
	}
}
