package com.alayouni.handyplotter.ui.drawers;

import com.alayouni.handyplotter.expressionutils.MathUtils;
import com.alayouni.handyplotter.ui.components.other.IntersectionColorToggle;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class FunctionsDrawer extends Drawer{
	private static final int INTERSECTION_RADIUS = 3;


	public void drawFunctions(Graphics g) throws Exception {
		for(FunctionComponent f : mappedFComponents) {
			drawFunction(g, f);
		}
	}


	private void drawFunction(Graphics g, FunctionComponent f) throws Exception {
		Double x = minX, y, lastY = null;
		Integer yPixel;
		for(int xPixel = 0; xPixel <= width; xPixel++, x+= xStep) {
			y = f.f(x);
			if(y!= null && !y.isNaN()) {
				if(!f.isHidden()) {
					g.setColor(f.getColor());
					if(lastY != null && f.isJoinPixels()) {//lastYPixel != null guarantees lastY != null
						//linking a finite value to an infinite value (i.e. the top/bottom of the view port)
						//should happen, but linking successive infinite values should never happen
						this.linkSuccessiveXPixelsIfLinkCrossesViewPort(xPixel - 1, lastY, y, g);
					} else if(y >= minY && y <= maxY){
						yPixel = (int)Math.round((y - minY) * ratioY);
						this.drawPixel(xPixel, yPixel, g);
					}
				}
				if(lastY != null) {
					this.drawFunctionIntersections(g, x, y, lastY, f);
				} else {
					f.reInitIntersections();
				}

			} else {
				f.reInitIntersections();
			}
			lastY = y;
		}
		this.drawVerticalAxisesIntersections(g, f);
	}

	private void linkSuccessiveXPixelsIfLinkCrossesViewPort(int x0, Double y0, Double y1, Graphics g) {
		if(y0.isInfinite() && y1.isInfinite()) {
			return;
		}
		//should never convert to pixels unless sure conversion from double to pixel won't exceed integer limit
		if(y0.isInfinite()) {
			linkInfiniteYPixelToFiniteYPixelIfInViewPort(y0, y1, x0 + 1, g);
		} else if(y1.isInfinite()) {
			linkInfiniteYPixelToFiniteYPixelIfInViewPort(y1, y0, x0, g);
		} else if((y0 <= maxY && y0 >= minY) || (y1 <= maxY && y1 >= minY)
				|| (y0 <= minY && y1 >= maxY) || (y1 <= minY && y0 >= maxY)){
			//to link pixels [minY, maxY] and [min(y0, y1), max(y0, y1)] must intersect
			Double middle = (y1 + y0) / 2;
			if(middle <= minY) {//link will be a vertical line
				if(y0 < middle) {//means y0 is out of the interval (view-port)
					if(y1 >= maxY) {
						this.drawLine(x0 + 1, 0, x0 + 1, height, g);
					} else {
						int yPixel = (int)Math.round((y1 - minY) * ratioY);
						this.drawLine(x0 + 1, 0, x0 + 1, yPixel, g);
					}
				} else {//means y1 is out of view-port
					if(y0 >= maxY) {
						this.drawLine(x0, 0, x0, height, g);
					} else {
						int yPixel = (int)Math.round((y0 - minY) * ratioY);
						this.drawLine(x0, 0, x0, yPixel, g);
					}
				}
			} else if(middle >= maxY) {//link will be a vertical line
				if(y0 > middle) {//means y0 is out of the interval (view-port)
					if(y1 <= minY) {
						this.drawLine(x0 + 1, height, x0 + 1, 0, g);
					} else {
						int yPixel = (int)Math.round((y1 - minY) * ratioY);
						this.drawLine(x0 + 1, height, x0 + 1, yPixel, g);
					}
				} else {//means y1 is out of view-port
					if(y0 <= minY) {
						this.drawLine(x0, height, x0, 0, g);
					} else {
						int yPixel = (int)Math.round((y0 - minY) * ratioY);
						this.drawLine(x0, height, x0, yPixel, g);
					}
				}
			} else {//link will be 2 vertical lines
				int middlePixel = (int)Math.round((middle - minY) * ratioY);
				int yPixel0, yPixel1;
				if(y0 <= minY) {
					yPixel0 = 0;
				} else if(y0 >= maxY) {
					yPixel0 = height;
				} else {
					yPixel0 = (int)Math.round((y0 - minY) * ratioY);
				}
				if(y1 <= minY) {
					yPixel1 = 0;
				} else if(y1 >= maxY) {
					yPixel1 = height;
				} else {
					yPixel1 = (int)Math.round((y1 - minY) * ratioY);
				}
				this.drawLine(x0, yPixel0, x0, middlePixel, g);
				this.drawLine(x0 + 1, middlePixel, x0 +1, yPixel1, g);
			}
		}
	}


	private void linkInfiniteYPixelToFiniteYPixelIfInViewPort(Double infiniteY, Double finiteY, int xPixel, Graphics g) {
		//should never convert to pixels unless sure conversion from double to pixel won't exceed integer limit
		if(infiniteY == Double.NEGATIVE_INFINITY) {
			if(finiteY < minY) {
				return;
			}
			if(finiteY >= maxY) {
				this.drawLine(xPixel, 0, xPixel, height, g);
				return;
			}
			int y1Pixel = (int) Math.round((finiteY - minY) * ratioY);
			this.drawLine(xPixel, 0, xPixel, y1Pixel, g);
		} else {//positive Infinity
			if(finiteY > maxY) {
				return;
			}
			if(finiteY <= minY) {
				this.drawLine(xPixel, height, xPixel, 0, g);
				return;
			}
			int y1Pixel = (int)Math.round((finiteY - minY) * ratioY);
			this.drawLine(xPixel, height, xPixel, y1Pixel, g);
		}
	}

	private void drawFunctionIntersections(final Graphics g,
	                                       final Double x,
	                                       final Double currentY1,
	                                       final Double lastY1,
	                                       final FunctionComponent f) throws Exception {
		FunctionComponent fIntersecting;
		for(IntersectionColorToggle<FunctionComponent> functionIntersection : f.getFIntersections()) {
			fIntersecting = functionIntersection.getIntersectionComponent();
			if(fIntersecting == f) {
				fIntersecting = functionIntersection.getDependencyComponent();
			}
			if(!functionIntersection.isHidden() &&
					fIntersecting.isInViewPort() &&
					fIntersecting.getIndex() < f.getIndex()) {
				//the index condition guarantees that an intersection gets drawn only once
				drawFunctionIntersection(g, x, currentY1, lastY1, functionIntersection, fIntersecting);
			}
		}
	}

	private void drawFunctionIntersection(final Graphics g,
	                                       final Double x,
	                                       Double currentY1,
	                                       Double lastY1,
	                                       final IntersectionColorToggle<FunctionComponent> fIntersection,
	                                       final FunctionComponent fIntersecting) throws Exception {
		Double lastY2 = fIntersection.getLastComputedValue(),
		currentY2 = fIntersecting.f(x);
		fIntersection.setLastComputedValue(currentY2);

		if(lastY2 == null || currentY2 == null ||
				(lastY2.isInfinite() && lastY1.isInfinite()) ||
				(currentY1.isInfinite() && currentY2.isInfinite())) {
			//currentY1 and lastY1 are assumed to be non null.
			fIntersection.setLastDiffDirection(null);
			return;
		}

		Double lastDiff = lastY2 - lastY1,
		currentDiff = currentY2 - currentY1;

		if(lastDiff.isInfinite() && currentDiff.isInfinite()) {
			int direction = currentY1.isInfinite() ? MathUtils.sign(currentY1) : -MathUtils.sign(currentY2);
			fIntersection.setLastDiffDirection(direction);
			return;
		}

		//if we're here means only one of {currentY1, currentY2, lastY1, lastY2} might be infinite
		Integer currentDiffDirection = MathUtils.sign(currentDiff - lastDiff),
		lastDiffDirection = fIntersection.getLastDiffDirection();

		if((MathUtils.sign(lastDiff) != MathUtils.sign(currentDiff)) && (lastDiffDirection == currentDiffDirection)) {//there's an intersection
			DoublePoint intersection = computeFIntersection(x, currentY1, lastY1, currentY2, lastY2);
			Point intersectionPixels = convertToPixels(intersection.x, intersection.y);

			this.drawIntersection(g, intersectionPixels.x, intersectionPixels.y, intersection.x, intersection.y, fIntersection);
		} else if(lastDiffDirection != null && lastDiffDirection != currentDiffDirection) {
			//we just reached a maxima or minima => check if lastDiff was close enough to zero, if so there's an intersection
			if(Math.abs(lastDiff) <= yStep) {//zeroY is guaranteed to be positive => intersection here
				Point intersectionPixels = convertToPixels(x - xStep, lastY1);
				this.drawIntersection(g, intersectionPixels.x, intersectionPixels.y, x - xStep, lastY1, fIntersection);
			}
		}
		fIntersection.setLastDiffDirection(currentDiffDirection);
	}

	/**
	 * <p>computes the intersection between [A1B1] = [(x, y1), (lastX, lastY1)] and [A2, B2] = [(x, y2), (lastX, lastY2)],
	 * and converts the result in pixels.</p>
	 * <p>
	 *     case where one of {y1, y2, lastY1, lastY2} is infinite will be treated.
	 * </p>
	 * @param x
	 * @param y1
	 * @param lastY1
	 * @param y2
	 * @param lastY2
	 * @return
	 */
	private DoublePoint computeFIntersection(double x,
	                                         Double y1,
	                                         Double lastY1,
	                                         Double y2,
	                                         Double lastY2) {
		//it's guaranteed that this method gets called only when at least 3 of {y1, lastY1, y2, lastY2} are finite
		//meaning only 1 of them might be infinite
		if(y1.isInfinite() || lastY1.isInfinite()) {
			return new DoublePoint(x, (y2 + lastY2) / 2);
		}
		if(y2.isInfinite() || lastY2.isInfinite()) {
			return new DoublePoint(x, (y1 + lastY1) / 2);
		}

		//Assuming [A1,B1] and [A2, B2] equations are respectively a1 . x + b1 = y and a2 . x + b2 = y
		//we first need to calculate a1, b1, a2 and b2
		double a1 = (y1 - lastY1) / xStep,
		b1 = y1 - a1 * x,
		a2 = (y2 - lastY2) / xStep,
		b2 = y2 - a2 * x;

		//calculate intersection
		DoublePoint intersection = new DoublePoint();
		intersection.x = (b2 -b1) / (a1 - a2);
		intersection.y = a1 * intersection.x + b1;
		return intersection;
	}


	private void drawVerticalAxisesIntersections(Graphics g, FunctionComponent f) throws Exception {
		//todo to replace by f(x)
//		for(IntersectionColorToggle<AxisComponent> intersection : f.getVAxisesIntersections()) {
//			if(!intersection.isHidden()) {
//				this.drawVerticalAxisIntersection(g, f, intersection);
//			}
//		}
	}

	private void drawVerticalAxisIntersection(Graphics g,
	                                          FunctionComponent f,
	                                          IntersectionColorToggle<AxisComponent> intersection) throws Exception {
		//todo to replace by f(x)
//		AxisComponent vAxis = intersection.getIntersectionComponent();
//		Double x = vAxis.getLocationValue(),
//		y = f.f(x);
//		if(y != null && y >= minY && y <= maxY && vAxis.isDefinedIn(y)) {
//			Point pt = convertToPixels(x, y);
//			drawIntersection(g, pt.x, pt.y, x, y, intersection);
//		}
	}




	private Point convertToPixels(double x, double y) {
		Point pt = new Point();
		pt.x = (int) Math.round((x - minX) * ratioX);
		pt.y = (int)Math.round((y - minY) * ratioY);
		return pt;
	}

	private void drawIntersection(Graphics g, int xPixel, int yPixel, double x, double y, IntersectionColorToggle intersection) {
		yPixel = height - yPixel;
		g.setColor(intersection.getColor());
		g.fillArc(xPixel - INTERSECTION_RADIUS,
				yPixel - INTERSECTION_RADIUS,
				INTERSECTION_RADIUS * 2 + 1,
				INTERSECTION_RADIUS * 2 + 1,
				0,
				360);

		if(intersection.isXVisible()) {
			String xStr = xDecimalFormatter.format(x, xStep);
			g.drawString(xStr, xPixel, yPixel + FONT_SIZE + INTERSECTION_RADIUS + 2);
		}

		if(intersection.isYVisible()) {
			String yStr = yDecimalFormatter.format(y, yStep);
			g.drawString(yStr, xPixel, yPixel - INTERSECTION_RADIUS - 2);
		}
	}

	private static class DoublePoint {
		double x;
		double y;

		private DoublePoint(double x, double y) {
			this.x = x;
			this.y = y;
		}

		private DoublePoint() {
		}
	}

}
