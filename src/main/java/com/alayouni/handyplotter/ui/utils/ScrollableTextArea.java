package com.alayouni.handyplotter.ui.utils;

import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInput;
import com.alayouni.handyplotter.ui.components.other.UnsafeRunnable;

import javax.swing.*;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/30/13
 * Time: 8:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScrollableTextArea extends JScrollPane implements RebuildExpressionInput {
	JTextArea textArea = null;
	private String lastTrimmedText = "";

	private UnsafeRunnable beforeBuildRunnable = null;

	public ScrollableTextArea() {
		super();
		setViewportView(getTextArea());
	}

	private JTextArea getTextArea() {
		if(textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}


	public String getText() {
		return getTextArea().getText();
	}


	public String getLastTrimmedText() {
		return this.lastTrimmedText;
	}


	public void setLastTrimmedText(String trimmedText) {
		this.lastTrimmedText = trimmedText;
	}

	public void setText(String text) {
		textArea.setText(text);
	}

	@Override
	public void addFocusListener(FocusListener focusListener) {
		textArea.addFocusListener(focusListener);
	}

	@Override
	public void addKeyListener(KeyListener keyListener) {
		textArea.addKeyListener(keyListener);
	}


	public void setBeforeBuildRunnable(UnsafeRunnable runnable) {
		this.beforeBuildRunnable = runnable;
	}


	public UnsafeRunnable getBeforeBuildRunnable() {
		return this.beforeBuildRunnable;
	}

	
	public boolean isValidateKey(KeyEvent e) {
		return (e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0);
	}
}
