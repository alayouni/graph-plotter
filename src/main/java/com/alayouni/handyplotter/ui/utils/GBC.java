package com.alayouni.handyplotter.ui.utils;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
   This class simplifies the use of the GridBagConstraints
   class.
*/
public class GBC extends GridBagConstraints {
	public static final double INFINITY = 9e+300;
	public GBC(){
		super();
	}
   /**
      Constructs a GBC with a given gridX and gridY position and
      all other grid bag constraint values set to the default.
      @param gridX the gridX position
      @param gridY the gridY position
   */
   public GBC(int gridX, int gridY)
   {
	   super();
      this.gridx = gridX;
      this.gridy = gridY;
	  this.setWeight(1., 1.);
   }

   /**
      Constructs a GBC with given gridX, gridY, gridWidth, gridHeight
      and all other grid bag constraint values set to the default.
      @param gridX the gridX position
      @param gridY the gridY position
      @param gridWidth the cell span in x-direction
      @param gridHeight the cell span in y-direction
   */
   public GBC(int gridX, int gridY, int gridWidth, int gridHeight)
   {
	  this(gridX, gridY);
      this.gridwidth = gridWidth;
      this.gridheight = gridHeight;
   }
   
   public GBC setGridX(int gridX){
	   this.gridx=gridX;
	   return this;
   }
   
   public GBC setGridY(int gridY){
	   this.gridy=gridY;
	   return this;
   }
   
   public GBC setGridWidth(int gridWidth){
	   this.gridwidth=gridWidth;
	   return this;
   }

   @SuppressWarnings("unused")
   public GBC setGridHeight(int gridHeight){
	   this.gridheight=gridHeight;
	   return this;
   }
   
   public GBC setGridLocation(int gridX,int gridY){
	   this.gridx=gridX;
	   this.gridy=gridY;
	   return this;
   }
   
   public GBC setGridSize(int gridWidth, int gridHeight){
	   this.gridwidth=gridWidth;
	   this.gridheight=gridHeight;
	   return this;
   }
   

   /**
      Sets the anchor.
      @param anchor the anchor value
      @return this object for further modification
   */
   public GBC setAnchor(int anchor)
   {
      this.anchor = anchor;
      return this;
   }
   
   /**
      Sets the fill direction.
      @param fill the fill direction
      @return this object for further modification
   */
   public GBC setFill(int fill)
   {
      this.fill = fill;
      return this;
   }

   /**
      Sets the cell weights.
      @param weightX the cell weight in x-direction
      @param weightY the cell weight in y-direction
      @return this object for further modification
   */
   public GBC setWeight(double weightX, double weightY)
   {
      this.weightx = weightX;
      this.weighty = weightY;
      return this;
   }
   
   public GBC setWeightX(double weightX)
   {
      this.weightx = weightX;
      return this;
   }
   
   public GBC setWeightY(double weightY)
   {
      this.weighty = weightY;
      return this;
   }

   /**
      Sets the insets of this cell.
      @param distance the spacing to use in all directions
      @return this object for further modification
   */
   public GBC setInsets(int distance)
   {
      this.insets = new Insets(distance, distance, distance, distance);
      return this;
   }

   /**
      Sets the insets of this cell.
      @param top the spacing to use on top
      @param left the spacing to use to the left
      @param bottom the spacing to use on the bottom
      @param right the spacing to use to the right
      @return this object for further modification
   */
   @SuppressWarnings("unused")
   public GBC setInsets(int top, int left, int bottom, int right)
   {
      this.insets = new Insets(top, left, bottom, right);
      return this;
   }
   
   public GBC setTopMargin(int top)
   {
	   if(insets==null)
		   insets=new Insets(top,0,0,0);
	   else
		   this.insets.top=top;
      return this;
   }
   
   public GBC setBottomMargin(int bottom)
   {
	   if(insets==null)
		   insets=new Insets(0,0,bottom,0);
	   else
		   this.insets.bottom=bottom;
      return this;
   }
   
   public GBC setLeftMargin(int left)
   {
	   if(insets==null)
		   insets=new Insets(0,left,0,0);
	   else
		   this.insets.left=left;
      return this;
   }
   
   public GBC setRightMargin(int right)
   {
	   if(insets==null)
		   insets=new Insets(0,0,0,right);
	   else
		   this.insets.right=right;
      return this;
   }

   /**
      Sets the internal padding
      @param iPadX the internal padding in x-direction

      @param iPadY the internal padding in y-direction
      @return this object for further modification
   */
   @SuppressWarnings("unused")
   public GBC setIPad(int iPadX, int iPadY)
   {
      this.ipadx = iPadX;
      this.ipady = iPadY;
      return this;
   }

   @SuppressWarnings("unused")
   public GBC setIPadX(int iPadX)
   {
      this.ipadx = iPadX;
      return this;
   }

   @SuppressWarnings("unused")
   public GBC setIPadY(int iPadY)
   {
      this.ipady = iPadY;
      return this;
   }


    public GBC setGridBounds(int gridX, int gridY, int gridWidth, int gridHeight) {
        return this.setGridLocation(gridX, gridY).setGridSize(gridWidth, gridHeight);
    }

    public GBC setMargins(int top, int left, int bottom, int right) {
        return this.setTopMargin(top).setLeftMargin(left).setBottomMargin(bottom).setRightMargin(right);
    }
}

