package com.alayouni.handyplotter.ui.utils.multisplitpane;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/24/13
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
class ItemSplitPane extends JSplitPane {

    public ItemSplitPane(int newOrientation) {
        super(newOrientation);
        initialize();
    }

	@SuppressWarnings("unused")
    public ItemSplitPane(int newOrientation, boolean newContinuousLayout) {
        super(newOrientation, newContinuousLayout);
        initialize();
    }

	@SuppressWarnings("unused")
    public ItemSplitPane(int newOrientation, Component newLeftComponent, Component newRightComponent) {
        super(newOrientation, newLeftComponent, newRightComponent);
        initialize();
    }

	@SuppressWarnings("unused")
    public ItemSplitPane(int newOrientation, boolean newContinuousLayout, Component newLeftComponent, Component newRightComponent) {
        super(newOrientation, newContinuousLayout, newLeftComponent, newRightComponent);
        initialize();
    }

    private void initialize() {
        setOneTouchExpandable(true);
        setContinuousLayout(true);
    }

    public void setComponent1(Component comp1) {
        if(getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
            setLeftComponent(comp1);
        } else {
            setTopComponent(comp1);
        }
    }

    public void setComponent2(Component comp2) {
        if(getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
            setRightComponent(comp2);
        } else {
            setBottomComponent(comp2);
        }

    }


    public Component getComponent1() {
        if(getOrientation() == JSplitPane.VERTICAL_SPLIT) {
            return getTopComponent();
        } else {
            return getLeftComponent();
        }
    }

    public Component getComponent2() {
        if(getOrientation() == JSplitPane.VERTICAL_SPLIT) {
            return getBottomComponent();
        } else {
            return getRightComponent();
        }
    }



    private static final Insets insets = new Insets(0, 0, 0, 0);
    @Override
    public Insets getInsets() {
        return insets;
    }

	@Override
	public Dimension getPreferredSize() {
		return computedPreferredSize;
	}


	final Dimension computedPreferredSize = new Dimension();
	public Dimension computePreferredSize() {
		computePreferredSizeIfCustomSplitPane(getComponent1());
		computePreferredSizeIfCustomSplitPane(getComponent2());

		if(getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
			computePreferredSizeHorizontalSplit();
		} else {
			computePreferredSizeVerticalSplit();
		}

		return computedPreferredSize;
	}


	/**
	 * presumes that if any of comp1 or comp2 is an instance of ItemSplitPane, comp1.computePreferredSize()
	 * (respectively comp2.computePreferredSize()) has already been invoked
	 */
	private void computePreferredSizeHorizontalSplit() {
		computedPreferredSize.height = getPreferredComponentHeight(getComponent1());
		if(computedPreferredSize.height == 0) {
			computedPreferredSize.height = getPreferredComponentHeight(getComponent2());
		}

		computedPreferredSize.width = getDividerSize();
		int comp1PreferredWidth = getPreferredComponentWidth(getComponent1());
		if(comp1PreferredWidth < getDividerLocation()) {
			comp1PreferredWidth = getDividerLocation();
		}
		computedPreferredSize.width += comp1PreferredWidth;
		computedPreferredSize.width += getPreferredComponentWidth(getComponent2());
	}

	/**
	 * presumes that if any of comp1 or comp2 is an instance of ItemSplitPane, comp1.computePreferredSize()
	 * (respectively comp2.computePreferredSize()) has already been invoked
	 */
	private void computePreferredSizeVerticalSplit() {
		computedPreferredSize.width = getPreferredComponentWidth(getComponent1());
		if(computedPreferredSize.width == 0) {
			computedPreferredSize.width = getPreferredComponentWidth(getComponent2());
		}

		computedPreferredSize.height = getDividerSize();
		int comp1PreferredHeight = getPreferredComponentHeight(getComponent1());
		if(comp1PreferredHeight < getDividerLocation()) {
			comp1PreferredHeight = getDividerLocation();
		}
		computedPreferredSize.height += comp1PreferredHeight;
		computedPreferredSize.height += getPreferredComponentHeight(getComponent2());
	}

	/**
	 * presumes that if comp is an instance of ItemSplitPane, comp.computePreferredSize() has already been invoked
	 * @param comp
	 * @return
	 */
	private int getPreferredComponentHeight(Component comp) {
		if(comp != null) {
			if(comp instanceof ItemSplitPane) {
				return ((ItemSplitPane)comp).computedPreferredSize.height;
			}
			return comp.getPreferredSize().height;
		}
		return 0;
	}

	/**
	 * presumes that if comp is an instance of ItemSplitPane, comp.computePreferredSize() has already been invoked
	 * @param comp
	 * @return
	 */
	private int getPreferredComponentWidth(Component comp) {
		if(comp != null) {
			if(comp instanceof ItemSplitPane) {
				return ((ItemSplitPane)comp).computedPreferredSize.width;
			}
			return comp.getPreferredSize().width;
		}
		return 0;
	}

	private void computePreferredSizeIfCustomSplitPane(Component comp) {
		if(comp != null &&
				comp instanceof ItemSplitPane) {
			((ItemSplitPane)comp).computePreferredSize();
		}
	}

	public int getPreferredComponent1Length() {
		return orientation == JSplitPane.HORIZONTAL_SPLIT ? getComponent1().getPreferredSize().width :
															getComponent1().getPreferredSize().height;
	}

	@SuppressWarnings("unused")
	public int getPreferredComponent2Length() {
		return orientation == JSplitPane.HORIZONTAL_SPLIT ? getComponent2().getPreferredSize().width :
															getComponent2().getPreferredSize().height;
	}

	public boolean isComponent2Empty() {
		return !(getComponent2() instanceof ItemSplitPane);
	}
}
