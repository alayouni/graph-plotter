package com.alayouni.handyplotter.ui.utils.multisplitpane;

import com.alayouni.handyplotter.ui.utils.GBC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/23/13
 * Time: 9:54 PM
 * To change this template use File | Settings | File Templates.
 */
class ItemWrapper<T extends Component> extends JPanel {
    private final ItemsHandler<T> itemsModel;
    private static final int BUTTONS_SIZE = 20;

    private final T component;

    private JPanel bottomPanel = null;
    private ZeroInsetsButton addButton = null;
    private ZeroInsetsButton deleteButton = null;

    public ItemWrapper(final T component, final ItemsHandler<T> itemsModel) {
        this.itemsModel = itemsModel;
        this.component = component;

        setLayout(new BorderLayout());
        add(component, BorderLayout.CENTER);
        add(getBottomPanel(), BorderLayout.SOUTH);
    }


    private JPanel getBottomPanel() {
        if(bottomPanel == null) {
	        JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            leftPanel.add(getAddButton());
            leftPanel.add(getDeleteButton());

	        bottomPanel = new JPanel(new GridBagLayout());
	        GBC gbc = new GBC(0, 0, 1, 1);
	        gbc.setFill(GBC.NONE).setAnchor(GBC.BASELINE_LEADING).setWeight(1., 1.);
	        bottomPanel.add(leftPanel, gbc);

	        if(component instanceof ItemExtension) {
		        Component extension = ((ItemExtension)component).getExtension();
		        gbc.setGridX(1).setAnchor(GBC.BASELINE_TRAILING);
		        bottomPanel.add(extension, gbc);
	        }
        }
        return bottomPanel;
    }

    private ZeroInsetsButton getAddButton() {
        if(addButton == null) {
            addButton = new ZeroInsetsButton("+");
            addButton.setPreferredSize(new Dimension(BUTTONS_SIZE, BUTTONS_SIZE));
	        addButton.setFocusable(false);
            addButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    itemsModel.addItem(component);
                }
            });
        }
        return addButton;
    }

    private ZeroInsetsButton getDeleteButton() {
        if(deleteButton == null) {
            deleteButton = new ZeroInsetsButton("-");
            deleteButton.setPreferredSize(new Dimension(BUTTONS_SIZE, BUTTONS_SIZE));
	        deleteButton.setFocusable(false);
            deleteButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    itemsModel.removeItem(component);
                }
            });
        }
        return deleteButton;
    }

    public T getComponent() {
        return component;
    }

    @SuppressWarnings("unused")
    public void setControlPanelVisible(boolean visible) {
        getBottomPanel().setVisible(visible);
    }

	@Override
	public Dimension getMinimumSize() {
		return getPreferredSize();
	}
}
