package com.alayouni.handyplotter.ui.utils.multisplitpane;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/23/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ItemGenerator<T extends Component> {

    T createItem(int index);
}
