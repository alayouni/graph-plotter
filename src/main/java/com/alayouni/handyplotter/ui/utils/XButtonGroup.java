package com.alayouni.handyplotter.ui.utils;

import javax.swing.*;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/24/13
 * Time: 6:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class XButtonGroup extends ButtonGroup {

	public AbstractButton getSelectedButton() {
		Enumeration<AbstractButton> buttons = this.getElements();
		AbstractButton selectedButton;
		while(buttons.hasMoreElements()) {
			selectedButton = buttons.nextElement();
			if(selectedButton.isSelected()) {
				return selectedButton;
			}
		}
		return null;
	}
}
