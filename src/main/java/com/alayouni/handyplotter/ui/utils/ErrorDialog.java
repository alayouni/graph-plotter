package com.alayouni.handyplotter.ui.utils;

import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.handlers.Handler;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/28/13
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ErrorDialog extends JDialog {
	private static final Dimension MSG_SIZE = new Dimension(700, 200);
	private static final Dimension STACK_TRACE_SIZE = new Dimension(700, 600);

	private JButton ok = null;
	private JButton viewStackTrace = null;
	private JTextArea textArea = null;
	private static Handler handler;

	private ErrorDialog() {
		super(SwingUtilities.getWindowAncestor(GraphPanel.getInstance()));
		this.setModal(true);
		setTitle("Error");
		setSize(MSG_SIZE);

		int x = getOwner().getX() + (getOwner().getWidth() - STACK_TRACE_SIZE.width) / 2;
		int y = getOwner().getY() + (getOwner().getHeight() - STACK_TRACE_SIZE.height) / 2;
		setLocation(x, y);

		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.add(getScrollPane(), BorderLayout.CENTER);
		contentPane.add(createSouthPanel(), BorderLayout.SOUTH);
		setContentPane(contentPane);
		getOKButton().requestFocus();
		setVisible(true);
		SwingUtilities.invokeLater(repaintGraphPanel);
	}

	private static final Runnable repaintGraphPanel = new Runnable() {

		public void run() {
			GraphPanel.getInstance().repaint();
		}
	};

	private JPanel createSouthPanel() {
		LayoutManager layout = new FlowLayout(FlowLayout.TRAILING);
		JPanel southPanel = new JPanel(layout);
		southPanel.add(getOKButton());
		southPanel.add(getViewStackTrace());
		return southPanel;
	}



	private JScrollPane getScrollPane() {
		return new JScrollPane(getTextArea());
	}

	private static final Insets INSETS = new Insets(10, 10, 10, 10);
	private JTextArea getTextArea() {
		if(textArea == null) {
			textArea = new JTextArea() {
				@Override
				public Insets getInsets() {
					return INSETS;
				}
			};
			textArea.setEditable(false);
			String msg = extractMessage(exception);
			textArea.setText(msg);
		}
		return textArea;
	}

	private String extractMessage(Throwable throwable) {
		if(throwable == null) {
			return "";
		}
		String msg = throwable.getMessage();
		if(msg == null || msg.trim().length() == 0) {
			return extractMessage(throwable.getCause());
		}

		return msg;
	}

	private JButton getOKButton() {
		if(ok == null) {
			ok = new JButton("OK");
			ok.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});

			ok.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_ENTER) {
						dispose();
					}
				}
			});
		}
		return ok;
	}


	private static final Rectangle TOP_LEFT_SCROLL_VISIBLE_REC = new Rectangle(0, 0, 20, 20);
	private Runnable scrollToTopLeftRunner = new Runnable() {

		public void run() {
			textArea.scrollRectToVisible(TOP_LEFT_SCROLL_VISIBLE_REC);
		}
	};

	private JButton getViewStackTrace() {
		if(viewStackTrace == null) {
			viewStackTrace = new JButton("View Stack Trace");
			viewStackTrace.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String stackTrace = ExceptionUtils.getFullStackTrace(exception);
					textArea.setText(stackTrace);
					SwingUtilities.invokeLater(scrollToTopLeftRunner);
					viewStackTrace.setVisible(false);
					ErrorDialog.this.setSize(STACK_TRACE_SIZE);

				}
			});
		}
		return viewStackTrace;
	}


	private static Exception exception;
	public static void showErrorDialogIfNotAlreadyBroken(final Exception e) {
		//no need to stop the spinning threads as showErrorDialog will do it
		if(!GraphPanel.getInstance().isBroken()) {
			showErrorDialog(e);
		}
	}

	public static void showErrorDialog(final Exception e) {
		//no need to stop the spinning threads as GraphPanel.setBroken will do it
		GraphPanel.getInstance().setBroken(true);
		exception = e;
		SwingUtilities.invokeLater(showErrorDialog);
	}

	public static void showSafeFailErrorDialog(final Exception e) {
		exception = e;
		handler.stopSpinningFieldsIfAny();
		SwingUtilities.invokeLater(showErrorDialog);
	}

	private static final Runnable showErrorDialog = new Runnable() {
		
		public void run() {
			handler.setLockFocusLostEvents(true);
			new ErrorDialog();
			handler.setLockFocusLostEvents(false);
		}
	};

	public static void setHandler(Handler handler) {
		ErrorDialog.handler = handler;
	}
}
