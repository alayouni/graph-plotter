package com.alayouni.handyplotter.ui.utils.multisplitpane;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/23/13
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */
class ItemsHandler<T extends Component> {
    private final ItemGenerator<T> itemGenerator;

    private final int splitOrientation;
    private final MultiSplitPane<T> multiSplitPane;

    private final List<T> items = new ArrayList<T>();

    private final List<ItemsListener<T>> itemsListeners = new ArrayList<ItemsListener<T>>();

    public ItemsHandler(final ItemGenerator<T> itemGenerator,
                        final MultiSplitPane<T> multiSplitPane,
                        final int splitOrientation) {
        this.itemGenerator = itemGenerator;
        this.multiSplitPane = multiSplitPane;
        this.splitOrientation = splitOrientation;
    }


    public void addItem(T after) {
	    int index = 0;
	    if(after != null) {
		    index = items.indexOf(after) + 1;
	    }
        T comp = itemGenerator.createItem(index);
        ItemWrapper<T> wrapper = new ItemWrapper<T>(comp, this);
	    ItemSplitPane newSplitPane = new ItemSplitPane(splitOrientation);
        if(after == null) {
            insertItemOnTop(wrapper, newSplitPane);
        } else {
            insertItemAfter(wrapper, after, newSplitPane);
        }

        getRootSplitPane().doLayout();
        items.add(index, comp);
        ItemsChangeEvent<T> e = new ItemsChangeEvent<T>(comp, newSplitPane, multiSplitPane, ItemsChangeEvent.ITEM_ADDED, index);
        fireItemsChanged(e);
    }

    private void insertItemOnTop(ItemWrapper<T> wrapper, ItemSplitPane newSplitPane) {
        switch (items.size()) {
            case 0:
                getRootSplitPane().setComponent1(wrapper);
                break;
            default:
	            int dividerLocation = getRootSplitPane().getDividerLocation();

	            Component wrapper0 = getRootSplitPane().getComponent1();
	            getRootSplitPane().setComponent1(wrapper);
	            newSplitPane.setComponent1(wrapper0);

	            Component comp2 = getRootSplitPane().getComponent2();
	            getRootSplitPane().setComponent2(newSplitPane);
	            newSplitPane.setComponent2(comp2);

	            if(!newSplitPane.isComponent2Empty()) {
		            newSplitPane.setDividerLocation(dividerLocation);
	            }

                break;
        }
    }

    private void insertItemAfter(ItemWrapper<T> wrapper, T after, ItemSplitPane newSplitPane) {
        ItemSplitPane containerSplit = (ItemSplitPane) SwingUtilities.getAncestorOfClass(ItemSplitPane.class, after);
	    int dividerLocation = containerSplit.getDividerLocation();
	    Component comp2 = containerSplit.getComponent2();
	    containerSplit.setComponent2(newSplitPane);
	    newSplitPane.setComponent1(wrapper);
	    newSplitPane.setComponent2(comp2);
	    containerSplit.setDividerLocation(dividerLocation);
    }


    public void removeItem(T component) {
        ItemSplitPane containerSplit = (ItemSplitPane) SwingUtilities.getAncestorOfClass(ItemSplitPane.class, component);
        if(containerSplit == getRootSplitPane()) {
	        removeFromRootSplitPane();
        } else {
            removeSplitPane(containerSplit);
        }

        getRootSplitPane().doLayout();
        int index = items.indexOf(component);
        items.remove(component);
        ItemsChangeEvent<T> e = new ItemsChangeEvent<T>(component, containerSplit, multiSplitPane, ItemsChangeEvent.ITEM_REMOVED, index);
        fireItemsChanged(e);
    }


    private void removeFromRootSplitPane() {
	    if(getRootSplitPane().isComponent2Empty()) {
		    getRootSplitPane().setComponent1(null);
	    } else {
		    ItemSplitPane nestedSplit = (ItemSplitPane)getRootSplitPane().getComponent2();
		    int dividerLocation = nestedSplit.getDividerLocation();
		    Component comp1 = nestedSplit.getComponent1();
		    Component comp2 = nestedSplit.getComponent2();
		    nestedSplit.setComponent1(null);
		    nestedSplit.setComponent2(null);
		    getRootSplitPane().setComponent1(comp1);
		    getRootSplitPane().setComponent2(comp2);
		    if(!getRootSplitPane().isComponent2Empty()) {
			    getRootSplitPane().setDividerLocation(dividerLocation);
		    }
	    }
    }

	private void removeSplitPane(ItemSplitPane splitPane) {
		ItemSplitPane containerSplit = (ItemSplitPane) SwingUtilities.getAncestorOfClass(ItemSplitPane.class, splitPane);
		int dividerLocation = containerSplit.getDividerLocation();
				Component comp2 = splitPane.getComponent2();
		splitPane.setComponent2(null);
		containerSplit.setComponent2(comp2);
		containerSplit.setDividerLocation(dividerLocation);
	}


    public List<T> getItems() {
        return items;
    }

    public void addItemsListener(ItemsListener <T> listener) {
        itemsListeners.add(listener);
    }

    private void fireItemsChanged(ItemsChangeEvent<T> e) {
	    multiSplitPane.computePreferredSize();
	    multiSplitPane.validate();
        for(ItemsListener<T> listener : itemsListeners) {
            listener.itemsChanged(e);
        }
    }

	private ItemSplitPane getRootSplitPane() {
		return multiSplitPane.getRootSplitPane();
	}
}
