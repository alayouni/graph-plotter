package com.alayouni.handyplotter.ui.utils.multisplitpane;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/30/13
 * Time: 9:09 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ItemExtension {
	Component getExtension();
}
