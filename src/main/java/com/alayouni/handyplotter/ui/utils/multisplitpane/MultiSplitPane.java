package com.alayouni.handyplotter.ui.utils.multisplitpane;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/22/13
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class MultiSplitPane<T extends Component> extends JScrollPane {
    private static final Color TITLE_COLOR = new Color(231,111,0);
    private static final Font TITLE_FONT = new Font(Font.SERIF, Font.BOLD, 14);
    private static final Color LINE_BORDER_COLOR = new Color(207,218,226);
    private static final int LINE_BORDER_THICKNESS = 2;
    private static final int BUTTONS_SIZE = 20;
	private static final int SCROLL_SPEED = 15;


    private final int splitPaneOrientation;
    private ZeroInsetsButton addButton = null;

    private ItemSplitPane rootSplitPane = null;
    private ItemsHandler<T> handler;

	private JPanel controlPanel = null;
	private JPanel containerPanel = null;
	private JPanel emptyPanel = null;

    public MultiSplitPane(String title,
                          final ItemGenerator<T> itemGenerator,
                          final int splitPaneOrientation) {

	    super();
	    this.setViewportView(getContainerPanel(title));
        this.splitPaneOrientation = splitPaneOrientation;
	    getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
	    getHorizontalScrollBar().setUnitIncrement(SCROLL_SPEED);
        handler = new ItemsHandler<T>(itemGenerator, this, splitPaneOrientation);
	    setupDividerListenerForSplitPane(getRootSplitPane());
	    setupDividerListenerForNewItems();


    }

	private JPanel getContainerPanel(String title) {
		if(containerPanel == null) {
			containerPanel = new JPanel(new BorderLayout()){
				@Override
				public Dimension getPreferredSize() {
					return preferredSize;
				}
			};
			containerPanel.setBorder(new TitledBorder(new LineBorder(LINE_BORDER_COLOR, LINE_BORDER_THICKNESS, true),
					title,
					TitledBorder.CENTER,
					TitledBorder.DEFAULT_POSITION,
					TITLE_FONT, TITLE_COLOR));

			containerPanel.add(getControlPanel(), BorderLayout.NORTH);
			containerPanel.add(getRootSplitPane(), BorderLayout.CENTER);
		}
		return containerPanel;
	}

	private JPanel getControlPanel() {
	    if(controlPanel == null) {
	        controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	        controlPanel.add(getAddButton());
	    }
        return controlPanel;
    }


    public ItemSplitPane getRootSplitPane() {
        if(rootSplitPane == null) {
            rootSplitPane = new ItemSplitPane(splitPaneOrientation);
	        rootSplitPane.setComponent2(getEmptyPanel());
        }
        return rootSplitPane;
    }

	private static final Dimension NULL_DIMENSION = new Dimension(0, 0);
	private JPanel getEmptyPanel() {
		if(emptyPanel == null) {
			emptyPanel = new JPanel();
			emptyPanel.setPreferredSize(NULL_DIMENSION);
			emptyPanel.setMaximumSize(NULL_DIMENSION);
			emptyPanel.setMinimumSize(NULL_DIMENSION);
		}
		return emptyPanel;
	}

    public ZeroInsetsButton getAddButton() {
        if(addButton == null) {
            addButton = new ZeroInsetsButton("+");
            addButton.setPreferredSize(new Dimension(BUTTONS_SIZE, BUTTONS_SIZE));
	        addButton.setFocusable(false);
            addButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    handler.addItem(null);
                }
            });
        }
        return addButton;
    }

    public List<T> getItems() {
        return handler.getItems();
    }

    public void addItemsListener(ItemsListener<T> listener) {
        handler.addItemsListener(listener);
    }

	public void addItem() {
		handler.addItem(null);
	}

	@SuppressWarnings("unused")
	public void removeItem(T item) {
		handler.removeItem(item);
	}



	private final Dimension preferredSize = new Dimension();
	public void computePreferredSize() {
		Dimension rootSplitPreferredSize = getRootSplitPane().computePreferredSize();
		Insets insets = containerPanel.getInsets();
		preferredSize.width = rootSplitPreferredSize.width + insets.left + insets.right;
		preferredSize.height = rootSplitPreferredSize.height + insets.top + insets.bottom;
		preferredSize.height += controlPanel.getPreferredSize().height;
	}

	private void setupDividerListenerForNewItems() {
		addItemsListener(new ItemsListener<T>() {

			public void itemsChanged(ItemsChangeEvent<T> e) {
				if(e.getEventType() == ItemsChangeEvent.ITEM_ADDED) {
					setupDividerListenerForSplitPane(e.getContainerSplitPane());
				}
			}
		});
	}

	private void setupDividerListenerForSplitPane(ItemSplitPane splitPane) {
		//hack to get the divider component
		Component dividerComp = splitPane.getComponent(0);

		DividersListener listener = new DividersListener(splitPane, dividerComp);
		dividerComp.addMouseListener(listener);
		dividerComp.addMouseMotionListener(listener);
	}

















	//_______________________divider listener / extending the behavior of dividers_________________________________
	//_____________________________________________________________________________________________________________
	private class DividersListener extends MouseAdapter {
		private int mouseLastLocation;
		private int dividerLocation;
		private ItemSplitPane closestUnfoldedAncestor = null;
		private final ItemSplitPane splitPane;
		private final Component divider;

		private DividersListener(final ItemSplitPane splitPane, final Component divider) {
			this.splitPane = splitPane;
			this.divider = divider;
		}

		@Override
		public void mousePressed(MouseEvent e) {
			mouseLastLocation = (splitPaneOrientation == JSplitPane.HORIZONTAL_SPLIT) ?
															e.getXOnScreen() :
															e.getYOnScreen();
			closestUnfoldedAncestor = splitPane;
			dividerLocation = splitPane.getDividerLocation();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			this.updateSplit(e);
			revalidate();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			this.updateSplit(e);
			computePreferredSize();
			revalidate();
			closestUnfoldedAncestor = null;
		}

		private void updateSplit(MouseEvent e) {
			final int mousePosition = (splitPaneOrientation == JSplitPane.HORIZONTAL_SPLIT) ?
					e.getXOnScreen():
					e.getYOnScreen();

			int diff = mousePosition - mouseLastLocation;
			if(diff < 0) {
				this.mouseLastLocation += this.shrinkAsPossible(diff);
			} else if(isDragEventAtDividerLevel(mousePosition)) {
				this.stretch(diff);
				this.mouseLastLocation = mousePosition;
			}
		}

		private int shrinkAsPossible(int diff) {
			if(closestUnfoldedAncestor != null && closestUnfoldedAncestor.getComponent1() != null) {
				int preferredLength = splitPane.getPreferredComponent1Length();
				int shrunkSize = shrink(diff);
				if(dividerLocation > preferredLength) {
					dividerLocation += shrunkSize;
					if(dividerLocation < preferredLength) {
						dividerLocation = preferredLength;
					}
				}
				updateSplitPanePreferredSize(shrunkSize);
				return shrunkSize;
			}
			return 0;
		}

		private int shrink(int diff) {
			int dividerLocation = closestUnfoldedAncestor.getDividerLocation();
			int preferredLength = closestUnfoldedAncestor.getPreferredComponent1Length();
			if(dividerLocation + diff < preferredLength) {
				return propagateShrinkAsNeeded(dividerLocation, preferredLength, diff);
			} else {
				closestUnfoldedAncestor.setDividerLocation(dividerLocation + diff);
				return diff;
			}
		}

		private int propagateShrinkAsNeeded(int dividerLocation, int preferredLength, int diff) {
			//this method assumes (dividerLocation - preferredLength) < |diff|
			//also diff is assumed to be negative as when shrinking diff is always negative
			int subDiff = 0;
			if(preferredLength < dividerLocation) {
				closestUnfoldedAncestor.setDividerLocation(preferredLength);
				subDiff = preferredLength - dividerLocation;
			}
			closestUnfoldedAncestor = findClosestUnfoldedAncestor(closestUnfoldedAncestor);
			if(closestUnfoldedAncestor != null) {
				return shrink(diff - subDiff) + subDiff;
			}
			return subDiff;
		}

		private ItemSplitPane findClosestUnfoldedAncestor(ItemSplitPane splitPane) {
			if(splitPane == rootSplitPane) {
				return null;
			}
			ItemSplitPane ancestorPane = (ItemSplitPane)SwingUtilities.getAncestorOfClass(ItemSplitPane.class, splitPane);
			if(ancestorPane.getPreferredComponent1Length() < ancestorPane.getDividerLocation()) {
				return ancestorPane;
			} else {
				return findClosestUnfoldedAncestor(ancestorPane);
			}

		}

		private void stretch(int diff) {
			closestUnfoldedAncestor = splitPane;
			dividerLocation += diff;
			splitPane.setDividerLocation(dividerLocation);
			updateSplitPanePreferredSize(diff);
		}

		private void updateSplitPanePreferredSize(int diff) {
			if(splitPaneOrientation == JSplitPane.HORIZONTAL_SPLIT) {
				preferredSize.width += diff;
			} else {
				preferredSize.height += diff;
			}
		}

		private boolean isDragEventAtDividerLevel(int mousePosition) {
			Point dividerOnScreen = divider.getLocation();
			SwingUtilities.convertPointToScreen(dividerOnScreen, splitPane);
			int dividerLocationOnScreen = (splitPaneOrientation == JSplitPane.HORIZONTAL_SPLIT) ? dividerOnScreen.x : dividerOnScreen.y;

			return mousePosition >= dividerLocationOnScreen;
		}
	}
}
