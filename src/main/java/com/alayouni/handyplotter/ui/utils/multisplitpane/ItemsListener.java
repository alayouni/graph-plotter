package com.alayouni.handyplotter.ui.utils.multisplitpane;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/24/13
 * Time: 10:31 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ItemsListener<T extends Component> {
    void itemsChanged(ItemsChangeEvent<T> e);
}
