package com.alayouni.handyplotter.ui.utils.multisplitpane;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/7/13
 * Time: 10:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class ZeroInsetsButton extends JButton {

	public ZeroInsetsButton(String text) {
		super(text);
	}

	private static final Insets INSETS = new Insets(0, 0, 0, 0);
	@Override
	public Insets getInsets() {
		return INSETS;
	}
}
