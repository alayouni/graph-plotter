package com.alayouni.handyplotter.ui.utils.multisplitpane;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/24/13
 * Time: 10:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ItemsChangeEvent<T extends Component> {
    public static final int ITEM_REMOVED = 0;
    public static final int ITEM_ADDED = 1;

    private final T component;
	private final MultiSplitPane<T> container;
    private final int eventType;
    private final int itemIndex;
	private final ItemSplitPane containerSplitPane;

    public ItemsChangeEvent(final T component, ItemSplitPane containerSplitPane, final MultiSplitPane<T> container, final int eventType, final int itemIndex) {
        this.component = component;
	    this.containerSplitPane = containerSplitPane;
	    this.container = container;
        this.eventType = eventType;
        this.itemIndex = itemIndex;
    }

    public T getComponent() {
        return component;
    }

	public ItemSplitPane getContainerSplitPane() {
		return containerSplitPane;
	}

	public int getEventType() {
        return eventType;
    }

    public int getItemIndex() {
        return itemIndex;
    }

	public MultiSplitPane<T> getContainer() {
		return container;
	}
}
