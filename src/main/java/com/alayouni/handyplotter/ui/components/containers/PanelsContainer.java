package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.utils.GBC;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/11/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class PanelsContainer extends JPanel {

	/**
	 * assumes panels.length > 1
	 * @param panels
	 */
	public PanelsContainer(JPanel... panels) {
		super(new GridBagLayout());
		GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.NONE).setAnchor(GBC.WEST);

		JPanel panel = panels[0];
		add(panel, gbc);

		gbc.setLeftMargin(15);
		for(int i = 1; i < panels.length - 1; i++) {
			gbc.gridx++;
			add(panels[i], gbc);
		}

		gbc.gridx++;
		gbc.setWeightX(GBC.INFINITY);
		add(panels[panels.length - 1], gbc);
	}
}
