package com.alayouni.handyplotter.ui.components.splitpane.items;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/30/13
 * Time: 11:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IndexedComponent {
	Dimension EXTENSION_TOGGLE_SIZE = new Dimension(34, 20);
	Font EXTENSION_TOGGLE_FONT = new Font(Font.SANS_SERIF, Font.BOLD, 10);
	Insets ZERO_INSETS = new Insets(0, 0, 0, 0);

	int getIndex();

	void setIndex(int index);
}
