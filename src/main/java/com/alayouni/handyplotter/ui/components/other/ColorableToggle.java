package com.alayouni.handyplotter.ui.components.other;

import com.alayouni.handyplotter.ui.components.splitpane.items.Colorable;
import com.alayouni.handyplotter.ui.components.splitpane.items.IndexedComponent;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/7/13
 * Time: 8:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ColorableToggle<T extends IndexedComponent> extends IndexedToggle<T> implements Colorable {

	public ColorableToggle(T dependencyComponent) {
		super(dependencyComponent);
	}


	public Color getColor() {
		return getForeground();
	}


	public void setColor(Color color) {
		setForeground(color);
	}
}
