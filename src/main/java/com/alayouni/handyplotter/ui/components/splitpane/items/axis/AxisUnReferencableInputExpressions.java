package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

/**
 * Axis UnReferencable input is dependency safe (no cycles can be caused by an un-referencable variable).
 * Note that these variables will be compiled with all the rest of code input in order to have
 * one compile per build and get better performance.
 * User: alayouni
 * Date: 11/18/13
 * Time: 10:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisUnReferencableInputExpressions {
	public static final int OFFSET_EXPRESSION = 1;
	public static final int LOCATION_EXPRESSION = 2;
	public static final int UNIT_EXPRESSION = 3;

	private String offsetExpression;
	private String locationExpression;
	private String unitExpression;

	private static Integer toString = null;

	public String getLocationExpression() {
		return locationExpression;
	}

	public void setLocationExpression(String locationExpression) {
		this.locationExpression = locationExpression;
	}

	public String getOffsetExpression() {
		return offsetExpression;
	}

	public void setOffsetExpression(String offsetExpression) {
		this.offsetExpression = offsetExpression;
	}

	public String getUnitExpression() {
		return unitExpression;
	}

	public void setUnitExpression(String unitExpression) {
		this.unitExpression = unitExpression;
	}

	public static void setToString(int toString) {
		AxisUnReferencableInputExpressions.toString = toString;
	}

	@Override
	public String toString() {
		if(toString == null) {
			throw new NullPointerException();
		}
		switch (toString) {
			case OFFSET_EXPRESSION:
				return getOffsetExpression();
			case LOCATION_EXPRESSION:
				return getLocationExpression();
			case UNIT_EXPRESSION:
				return getUnitExpression();
		}

		return "";
	}
}
