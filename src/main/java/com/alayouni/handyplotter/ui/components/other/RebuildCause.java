package com.alayouni.handyplotter.ui.components.other;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/21/13
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
public enum RebuildCause {
	FOCUS_LOST,
	VALIDATE_ACTION_TRIGGERED,
	VARIABLES_CHANGE,
	FUNCTION_ITEMS_CHANGE,
	VIEW_PORTS_MAPPING_CHANGE,
	VIEW_PORT_ITEMS_CHANGE,
	AXIS_ITEMS_CHANGE,
	BUILD_BUTTON_TRIGGERED,
	BOUNDARIES_CHANGE,
	STARTUP
}
