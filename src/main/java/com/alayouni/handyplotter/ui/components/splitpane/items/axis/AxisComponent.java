package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

import com.alayouni.handyplotter.expressionutils.ExpressionBuilder;
import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionsHolder;
import com.alayouni.handyplotter.expressionutils.VariableWrapper;
import com.alayouni.handyplotter.ui.components.other.*;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionChangeListener;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionException;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionField;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionValidator;
import com.alayouni.handyplotter.ui.components.splitpane.items.*;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemExtension;
import com.alayouni.handyplotter.utils.DecimalFormatter;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/6/13
 * Time: 6:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisComponent extends JPanel implements ItemExtension, IndexedComponent, Colorable, ExpressionsHolder,
		RebuildExpressionInputsHolder {
	private static final String SHOW_TICKS_VALUES_LABEL = "Draw Values";

	private static final Color DEFAULT_COLOR = Color.black;
	static final int MAXIMUM_TICKS_COUNT = 500;

	public static final String LOCATION_LABEL = "Location";
	public static final String OFFSET_LABEL = "Offset";
	public static final String UNIT_LABEL = "Unit";
	private static final String MIN_LABEL = "Min";
	private static final String MAX_LABEL = "Max";
	private static final String PRECISION_LABEL = "Precision";

	private static final Double DEFAULT_LOCATION = 0.;
	private static final Double LOCATION_STEP = 0.1;

	private static final Double DEFAULT_MIN = -10.;
	private static final Double MIN_STEP = 0.1;

	private static final Double DEFAULT_MAX = 10.;
	private static final Double MAX_STEP = 0.1;

	private static final Double DEFAULT_OFFSET = 0.;
	private static final Double OFFSET_STEP = 0.1;

	private static final Double DEFAULT_UNIT = 1.;
	private static final Double UNIT_STEP = 0.1;

	public static final String AXIS_PREFIX = "A";

	private static final Insets FIELD_INSETS = new Insets(5, 2, 0, 2);

	private boolean showTicksValues = true;
	private DecimalFormatter decimalFormatter = new DecimalFormatter(3);

	private int index;
	private JLabel nameLabel = null;
	private SimpleExpressionField<AxisDynamicLocation> locationField = null;
	private VariableWrapper minVariable = null;
	private SimpleExpressionField minField = null;
	private VariableWrapper maxVariable = null;
	private SimpleExpressionField maxField = null;
	private SimpleExpressionField<Boolean> offsetField = null;
	private SimpleExpressionField<Integer> unitField = null;
	private RebuildTextField precisionField = null;

	private JToggleButton showTicksValuesToggle = null;

	private JPanel fieldsContainer = null;

	private JPanel extensionPanel;

	private static final AxisDynamicLocationProcessor AXIS_DYNAMIC_LOCATION_PROCESSOR = new AxisDynamicLocationProcessor();
	private static final AxisTicksCountProcessor AXIS_TICKS_COUNT_PROCESSOR = new AxisTicksCountProcessor();
	private static final AxisAutoOffsetProcessor AXIS_AUTO_OFFSET_PROCESSOR = new AxisAutoOffsetProcessor();

	private final SimpleExpressionChangeListener simpleExpressionChangeListener = new SimpleExpressionChangeListener() {

		public void simpleExpressionChanged(SimpleExpressionField field) throws Exception {
			GraphPanel.getInstance().repaint();
		}
	};



	public AxisComponent(int index) {
		this.index = index;
		rebuildExpressionInputs = new RebuildExpressionInput[]{
				getMinField().getValueField(),
				getMinField().getStepField(),
				getMaxField().getValueField(),
				getMaxField().getStepField(),
				getLocationField().getValueField(),
				getLocationField().getStepField(),
				getOffsetField().getValueField(),
				getOffsetField().getStepField(),
				getUnitField().getValueField(),
				getUnitField().getStepField(),
				getDoublePrecisionField()};
		setLayout(new BorderLayout());
		add(getFieldsContainer(), BorderLayout.NORTH);
		setColor(DEFAULT_COLOR);
	}


	private SimpleExpressionField<AxisDynamicLocation> getLocationField() {
		if(locationField == null) {
			SimpleExpressionValidator noNull = new SimpleExpressionValidator() {

				public void validate(Double value) throws Exception {
					if(locationField.getSpecialValue() == null && value == null) {
						throw new SimpleExpressionException(locationField.getIdentifier() +" cannot be null");
					}
				}
			};
			locationField = new SimpleExpressionField<AxisDynamicLocation>(DEFAULT_LOCATION,
					LOCATION_STEP,
					AXIS_PREFIX,
					LOCATION_LABEL.toLowerCase(),
					index,
					noNull,
					AXIS_DYNAMIC_LOCATION_PROCESSOR);
			//DEFAULT_LOCATION and the CENTER dynamic value won't conflict
			locationField.setSpecialValue(AxisDynamicLocation.CENTER);
			initExpressionField(locationField, true);

		}
		return locationField;
	}

	private SimpleExpressionField getMaxField() {
		if(maxField == null) {
			SimpleExpressionValidator validator = new SimpleExpressionValidator() {

				public void validate(Double value) throws Exception {
					String identifier = maxField.getIdentifier();
					if(value == null) {
						throw new SimpleExpressionException(identifier +" cannot be null");
					} else {
						Double min = minField.getValue();
						if(min != null && min >= value) {
							String msg = String.format("(%s = %s) <= (%s.%s = %s), %s must always be > %s %s!",
									identifier,
									value,
									AxisComponent.this,
									MIN_LABEL.toLowerCase(),
									min,
									identifier,
									AxisComponent.this,
									MIN_LABEL.toLowerCase());
							throw new SimpleExpressionException(msg);
						}
						if(!isHidden() && areTooManyTicks()) {
							String msg = String.format("%s : too many ticks!", identifier);
							throw new SimpleExpressionException(msg);
						}
					}
				}
			};
			maxField = new SimpleExpressionField(DEFAULT_MAX,
					MAX_STEP,
					AXIS_PREFIX,
					MAX_LABEL.toLowerCase(),
					index,
					validator);
			maxField.addChangeListener(new SimpleExpressionChangeListener() {

				public void simpleExpressionChanged(SimpleExpressionField field) throws Exception {
					if(field.getVariable().getVariable() == null) {
						maxVariable.fireVariableValueChanged();
						maxVariable.updateDependents(true);
						GraphPanel.getInstance().repaint();
					}
				}
			});
			this.initExpressionField(maxField, false);

		}
		return maxField;
	}

	private SimpleExpressionField getMinField() {
		if(minField == null) {
			SimpleExpressionValidator validator = new SimpleExpressionValidator() {

				public void validate(Double value) throws Exception {
					String identifier = minField.getIdentifier();
					if(value == null) {
						throw new SimpleExpressionException(identifier +" cannot be null");
					} else {
						Double max = maxField.getValue();
						if(max != null && max <= value) {
							String msg = String.format("(%s = %s) >= (%s.%s = %s), %s must always be < %s %s!",
									identifier,
									value,
									AxisComponent.this,
									MAX_LABEL.toLowerCase(),
									max,
									identifier,
									AxisComponent.this,
									MAX_LABEL.toLowerCase());
							throw new SimpleExpressionException(msg);
						}
						if(!isHidden() && areTooManyTicks()) {
							String msg = String.format("%s : too many ticks!", identifier);
							throw new SimpleExpressionException(msg);
						}
					}
				}
			};
			minField = new SimpleExpressionField(DEFAULT_MIN,
					MIN_STEP,
					AXIS_PREFIX,
					MIN_LABEL.toLowerCase(),
					index,
					validator);

			minField.addChangeListener(new SimpleExpressionChangeListener() {

				public void simpleExpressionChanged(SimpleExpressionField field) throws Exception {
					if (field.getVariable().getVariable() == null) {
						minVariable.fireVariableValueChanged();
						minVariable.updateDependents(true);
						GraphPanel.getInstance().repaint();
					}
				}
			});
			this.initExpressionField(minField, false);
		}
		return minField;
	}

	private SimpleExpressionField<Boolean> getOffsetField() {
		if(offsetField == null) {
			SimpleExpressionValidator noNull = new SimpleExpressionValidator() {

				public void validate(Double value) throws Exception {
					if(offsetField.getSpecialValue() == null && value == null) {
						throw new SimpleExpressionException(AxisComponent.this + " offset cannot be null");
					}
				}
			};
			offsetField = new SimpleExpressionField<Boolean>(DEFAULT_OFFSET,
					OFFSET_STEP,
					AXIS_PREFIX,
					OFFSET_LABEL.toLowerCase(),
					index,
					noNull,
					AXIS_AUTO_OFFSET_PROCESSOR);
			//DEFAULT_OFFSET and the auto dynamic value won't conflict
			offsetField.setSpecialValue(true);
			initExpressionField(offsetField, true);
		}
		return offsetField;
	}

	private SimpleExpressionField<Integer> getUnitField() {
		if(unitField == null) {
			SimpleExpressionValidator validator = new SimpleExpressionValidator() {

				public void validate(Double value) throws Exception {
					String identifier = unitField.getIdentifier();
					if(unitField.getSpecialValue() == null) {
						if(value == null) {
							throw new SimpleExpressionException(identifier +" cannot be null");
						} else if(value == 0.) {
							String msg = String.format("%s cannot be zero!", identifier);
							throw new SimpleExpressionException(msg);
						}
					}
					if(!isHidden() && areTooManyTicks()) {
						String msg = String.format("%s : too many ticks!", identifier);
						throw new SimpleExpressionException(msg);
					}
				}
			};
			unitField = new SimpleExpressionField<Integer>(DEFAULT_UNIT,
					UNIT_STEP,
					AXIS_PREFIX,
					UNIT_LABEL.toLowerCase(),
					index,
					validator,
					AXIS_TICKS_COUNT_PROCESSOR);
			//DEFAULT_UNIT and the number of ticks won't conflict
			unitField.setSpecialValue(10);
			initExpressionField(unitField, true);
		}
		return unitField;
	}

	private boolean areTooManyTicks() throws Exception {
		Double unit = getUnitValue(),
				axisMin = getMinValue(),
				axisMax = getMaxValue();
		if(axisMin != null && axisMax != null && unit != null) {
			double ticksNumber = (axisMax - axisMin) / unit;
			if(Math.abs(ticksNumber) > MAXIMUM_TICKS_COUNT) {
				return true;
			}
		}
		return false;
	}

	private RebuildTextField getDoublePrecisionField() {
		if(precisionField == null) {
			precisionField = new RebuildTextField(String.valueOf(decimalFormatter.getPrecision()));
			precisionField.setBeforeBuildRunnable(new UnsafeRunnable() {

				public void run(RebuildCause rebuildCause) throws Exception {
					setPrecision();
					offsetField.firePrecisionUpdated();
					locationField.firePrecisionUpdated();
					unitField.firePrecisionUpdated();
					minField.firePrecisionUpdated();
					maxField.firePrecisionUpdated();
				}
			});
		}
		return precisionField;
	}


	private void setPrecision() throws Exception {
		final String identifier = toString() + " " + PRECISION_LABEL;
		String doublePrecisionTrimmed = precisionField.getText().trim();
		if(StringUtils.isEmpty(doublePrecisionTrimmed)) {
			throw new Exception(identifier + " cannot be empty");
		}

		try{
			int precision = Integer.parseInt(doublePrecisionTrimmed);
			if(precision <= 0) {
				throw new Exception(identifier + " must be positive");
			}
			decimalFormatter.setPrecision(precision);
			GraphPanel.getInstance().repaint();
		} catch(NumberFormatException e) {
			throw new Exception(identifier + " must have integer format", e);
		}

	}


	private JPanel getFieldsContainer() {
		if(fieldsContainer == null) {
			fieldsContainer = new JPanel(new GridBagLayout());

			GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.HORIZONTAL).setAnchor(GBC.CENTER).setWeight(1., 1.).setBottomMargin(5);
			fieldsContainer.add(new JLabel(LOCATION_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(MIN_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(MAX_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(OFFSET_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(UNIT_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(PRECISION_LABEL), gbc);

			gbc.setGridLocation(1, 0).setWeightX(GBC.INFINITY).setLeftMargin(5);
			fieldsContainer.add(getLocationField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getMinField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getMaxField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getOffsetField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getUnitField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getDoublePrecisionField(), gbc);
		}
		return fieldsContainer;
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
		//no need to set indexes for variables as setIndex comes always before a rebuild action, meaning all variables
		//will be re-instantiated
		minField.setIndex(index);

		maxField.setIndex(index);

		offsetField.setIndex(index);
		locationField.setIndex(index);
		unitField.setIndex(index);

		getNameLabel().setText(toString());
	}

	public JLabel getNameLabel() {
		if(nameLabel == null) {
			nameLabel = new JLabel(toString());
			nameLabel.setFont(EXTENSION_TOGGLE_FONT);
			nameLabel.setForeground(color);
		}
		return nameLabel;
	}

	private JToggleButton getShowTicksValuesToggle() {
		if(showTicksValuesToggle == null) {
			showTicksValuesToggle = new JToggleButton(SHOW_TICKS_VALUES_LABEL) {
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			showTicksValuesToggle.setPreferredSize(new Dimension(75, 20));
			showTicksValuesToggle.setFont(EXTENSION_TOGGLE_FONT);
			showTicksValuesToggle.setSelected(true);
			showTicksValuesToggle.setForeground(Color.black);
			showTicksValuesToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					setShowTicksValues(showTicksValuesToggle.isSelected());
					GraphPanel.getInstance().repaint();
				}
			});
			showTicksValuesToggle.setFocusable(false);
		}
		return showTicksValuesToggle;
	}




	public JPanel getExtension() {
		if(extensionPanel == null) {
			extensionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
			extensionPanel.add(getShowTicksValuesToggle());
			extensionPanel.add(getNameLabel());
		}
		return extensionPanel;
	}


	private Color color;

	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
		getNameLabel().setForeground(color);
		getShowTicksValuesToggle().setForeground(color);
	}

	@Override
	public String toString() {
		return AXIS_PREFIX + index;
	}

	private void initExpressionField(SimpleExpressionField field, boolean addRepaintChangeListener) {
		field.setInsets(FIELD_INSETS);
		field.setDecimalFormatter(decimalFormatter);
		field.setBorder(new LineBorder(Color.black));//not sure if using 1 border reference for all panels would bug
		if(addRepaintChangeListener) {
			field.addChangeListener(simpleExpressionChangeListener);
		}
	}

	public void reloadAxis() {
		minVariable = ExpressionBuilder.min.get(index);
		minField.setVariable(minVariable);

		maxVariable = ExpressionBuilder.max.get(index);
		maxField.setVariable(maxVariable);


		AxisUnReferencableVariables unReferencableVariables = ExpressionBuilder.axisUnReferencableVariables.get(index);

		VariableWrapper locationWrapper = unReferencableVariables.getLocationVariable();
		locationField.setVariable(locationWrapper);

		VariableWrapper offsetWrapper = unReferencableVariables.getOffsetVariable();
		offsetField.setVariable(offsetWrapper);

		VariableWrapper unitWrapper = unReferencableVariables.getUnitVariable();
		unitField.setVariable(unitWrapper);
	}

	public void fireExpressionsRebuilt() throws Exception {
		minField.fireExpressionsRebuilt();
		maxField.fireExpressionsRebuilt();
		locationField.fireExpressionsRebuilt();
		offsetField.fireExpressionsRebuilt();
		unitField.fireExpressionsRebuilt();

		//no need to call setPrecision() as it gets called before the re-build
	}


	public Double getLocationValue() {
		return getLocationField().getValue();
	}

	public Double getMinValue() {
		return getMinField().getValue();
	}

	public Double getMaxValue() {
		return getMaxField().getValue();
	}

	public Double getOffsetValue() {
		return getOffsetField().getValue();
	}

	public Double getUnitValue() throws Exception {
		Integer ticksCount = parseTicksCount();
		if(ticksCount != null && getMinValue() != null && getMaxValue() != null) {
			return (getMaxValue() - getMinValue()) / ticksCount;
		}
		return getUnitField().getValue();
	}

	public String getMinExpression() throws Exception {
		return minField.getExpression();
	}

	public String getMaxExpression() throws Exception {
		return maxField.getExpression();
	}

	public String getLocationExpression() throws Exception {
		return locationField.getExpression();
	}

	public String getOffsetExpression() throws Exception {
		return offsetField.getExpression();
	}

	public String getUnitExpression() throws Exception {
		return unitField.getExpression();
	}


	public void updateExpressions(ExpressionProcessor processor) {
		locationField.updateExpressions(processor);
		minField.updateExpressions(processor);
		maxField.updateExpressions(processor);
		offsetField.updateExpressions(processor);
		unitField.updateExpressions(processor);
	}

	public boolean isShowTicksValues() {
		return showTicksValues;
	}

	public void setShowTicksValues(boolean showTicksLabels) {
		this.showTicksValues = showTicksLabels;
	}

	public DecimalFormatter getDecimalFormatter() {
		return decimalFormatter;
	}


	private final RebuildExpressionInput[] rebuildExpressionInputs;

	public RebuildExpressionInput[] getRebuildExpressionInputs() {
		return rebuildExpressionInputs;
	}

	private boolean isHidden() {
		for(ViewPortComponent viewPort : ExpressionBuilder.viewPorts) {
			if(!viewPort.isHidden()) {
				if(viewPort.getVAxis() == this &&
						(!viewPort.isHideVAxis() || viewPort.isShowHorizontalGrid())) {
					return false;
				}
				if(viewPort.getHAxis() == this &&
						(!viewPort.isHideHAxis() || viewPort.isShowVerticalGrid())) {
					return false;
				}
			}
		}
		return true;
	}

	public void validateTooManyTicks() throws Exception {
		unitField.validateValue();
	}

	/**
	 * This method won't process any dependencies, and won't switch the state of the simple expression field to constant
	 * @param minValue
	 */
	public void setMinValue(double minValue) {
		minField.setValue(minValue);
	}

	/**
	 * This method won't process any dependencies, and won't switch the state of the simple expression field to constant
	 * @param maxValue
	 */
	public void setMaxValue(double maxValue) {
		maxField.setValue(maxValue);
	}

	public Integer parseTicksCount() throws Exception {
		Integer ticksCount = unitField.getSpecialValue();
		if(ticksCount != null && ticksCount > MAXIMUM_TICKS_COUNT) {
			String msg = String.format("%s : too many ticks!", unitField.getIdentifier());
			throw new SimpleExpressionException(msg);
		}
		return ticksCount;
	}

	public boolean isOffsetAuto() throws Exception {
		return offsetField.getSpecialValue() != null;
	}

	public AxisDynamicLocation parseDynamicLocation() throws Exception {
		return locationField.getSpecialValue();
	}

	public void stopSpinningFieldsIfAny() {
		locationField.stopSpinning();
		offsetField.stopSpinning();
		unitField.stopSpinning();
		minField.stopSpinning();
		maxField.stopSpinning();
	}

	public VariableWrapper getMaxVariable() {
		return maxVariable;
	}

	public VariableWrapper getMinVariable() {
		return minVariable;
	}

	public boolean isMinConstant() {
		return minVariable.getVariable() == null;
	}

	public boolean isMaxConstant() {
		return maxVariable.getVariable() == null;
	}

	public void switchMinFieldToConstantState() {
		minField.switchToConstantState();
	}

	public void switchMaxFieldToConstantState() {
		maxField.switchToConstantState();
	}
}
