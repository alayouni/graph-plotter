package com.alayouni.handyplotter.ui.components.splitpane.items;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/6/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Colorable {
	Color getColor();

	void setColor(Color color);
}
