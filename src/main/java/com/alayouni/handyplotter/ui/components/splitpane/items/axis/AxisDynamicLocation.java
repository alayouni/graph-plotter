package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/24/13
 * Time: 7:39 PM
 * To change this template use File | Settings | File Templates.
 */
public enum AxisDynamicLocation {
	MIN,
	CENTER,
	MAX
}
