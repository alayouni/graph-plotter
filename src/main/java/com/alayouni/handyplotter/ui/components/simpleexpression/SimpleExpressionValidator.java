package com.alayouni.handyplotter.ui.components.simpleexpression;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/28/13
 * Time: 9:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SimpleExpressionValidator {

    void validate(Double value) throws Exception;
}
