package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

import com.alayouni.handyplotter.expressionutils.VariableWrapper;

/**
 * Axis UnReferencable variables are dependency safe (no cycles can be caused by an un-referencable variable).
 * Note that these variables will be compiled with all the rest of code input in order to have
 * one compile per build and get better performance.
 * User: alayouni
 * Date: 11/18/13
 * Time: 10:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisUnReferencableVariables {
	VariableWrapper offsetVariable;
	VariableWrapper locationVariable;
	VariableWrapper unitVariable;

	public VariableWrapper getLocationVariable() {
		return locationVariable;
	}

	public void setLocationVariable(VariableWrapper locationVariable) {
		this.locationVariable = locationVariable;
	}

	public VariableWrapper getOffsetVariable() {
		return offsetVariable;
	}

	public void setOffsetVariable(VariableWrapper offsetVariable) {
		this.offsetVariable = offsetVariable;
	}

	public VariableWrapper getUnitVariable() {
		return unitVariable;
	}

	public void setUnitVariable(VariableWrapper unitVariable) {
		this.unitVariable = unitVariable;
	}
}
