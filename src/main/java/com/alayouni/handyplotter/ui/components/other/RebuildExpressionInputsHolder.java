package com.alayouni.handyplotter.ui.components.other;

import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInput;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/12/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RebuildExpressionInputsHolder {
	RebuildExpressionInput[] getRebuildExpressionInputs();
}
