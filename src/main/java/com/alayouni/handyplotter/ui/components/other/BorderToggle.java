package com.alayouni.handyplotter.ui.components.other;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/3/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class BorderToggle extends JToggleButton {
	private static final int MARGIN = 3;
	private static final Color DEFAULT_COLOR = Color.BLACK;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int x = MARGIN,
		y = MARGIN,
		width = getWidth() - 2 * MARGIN - 1,
		height = getHeight() - 2 * MARGIN - 1;
		g.setColor(DEFAULT_COLOR);
		g.drawRect(x, y, width, height);
	}
}
