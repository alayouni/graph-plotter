package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/24/13
 * Time: 8:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisAutoOffsetProcessor extends AbstractSpecialProcessor<Boolean> {
	private static final String AUTO_KEYWORD = "auto";

	public Boolean evaluateSpecialExpression(String text) throws Exception {
		String lowerCase = text.trim().toLowerCase();
		if(lowerCase.equals(AUTO_KEYWORD)) {
			return true;
		}
		return null;
	}


	public String specialValueToString(Boolean specialValue) {
		return AUTO_KEYWORD;
	}
}
