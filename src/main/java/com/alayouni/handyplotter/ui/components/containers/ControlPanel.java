package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.handlers.Handler;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/11/13
 * Time: 9:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class ControlPanel extends JPanel {
	private static final Color LINE_BORDER_COLOR = new Color(207,218,226);
	private static final int LINE_BORDER_THICKNESS = 1;
	private static final Dimension PLACE_HOLDER_PANEL_MIN_SIZE = new Dimension(30, 55);
	private static final String BUILD_BUTTON_TEXT = "Build";

	private IndexedTogglesPanel<FunctionComponent> fIntersectionPanel;

	private IndexedTogglesPanel<FunctionComponent> fIntersectingPanel;

	private Handler handler;

	private JButton buildButton;

	public ControlPanel(Handler handler) {
		super(new GridBagLayout());
		this.handler = handler;
		layoutComponents();
	}

	private void layoutComponents() {
		GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.NONE).setAnchor(GBC.NORTHWEST).setMargins(5, 10, 0, 0);
		JPanel intersectionsRoot = buildIntersectionsPanel();
		this.add(intersectionsRoot, gbc);

		gbc.gridx++;
		gbc.setWeightX(GBC.INFINITY);
		this.add(getBuildButton(), gbc);
	}

	private JPanel buildIntersectionsPanel() {
		// setMinimumSize doesn't work unless setPreferredSize is set. Setting preferred size will screw up the dynamic layout
		//=> using a card layout with un invisible place holder works fine for setting an initial size
		CardLayout cardLayout = new CardLayout();
		JPanel rootPanel = new JPanel(cardLayout);

		JPanel placeHolder = new JPanel();
		placeHolder.setPreferredSize(PLACE_HOLDER_PANEL_MIN_SIZE);
		rootPanel.add(placeHolder, "placeHolder");

		JPanel intersectionsPanel = new JPanel(new GridBagLayout());
		GBC gbc = new GBC(1, 0, 1, 1).setFill(GBC.NONE).setAnchor(GBC.NORTHWEST).setRightMargin(5);
		intersectionsPanel.add(getfIntersectionPanel(), gbc);

		JPanel intersectionsPanel2 = getfIntersectingPanel();
		gbc.gridy++;
		gbc.weighty = GBC.INFINITY;
		intersectionsPanel.add(intersectionsPanel2, gbc);

		JLabel intersectionLabel = new JLabel("<html><font size=6>&cap;</font></html>");
		gbc.setGridBounds(0, 0, 1, 2).setAnchor(GBC.CENTER);
		intersectionsPanel.add(intersectionLabel, gbc);
		final String cardName = "intersectionsPanel";
		rootPanel.add(intersectionsPanel, cardName);
		cardLayout.show(rootPanel, cardName);

		rootPanel.setBorder(new LineBorder(LINE_BORDER_COLOR, LINE_BORDER_THICKNESS));
		return rootPanel;
	}

	public IndexedTogglesPanel<FunctionComponent> getfIntersectingPanel() {
		if(fIntersectingPanel == null) {
			fIntersectingPanel = new IndexedTogglesPanel<FunctionComponent>(null);
		}
		return fIntersectingPanel;
	}

	public IndexedTogglesPanel<FunctionComponent> getfIntersectionPanel() {
		if(fIntersectionPanel == null) {
			fIntersectionPanel = new IndexedTogglesPanel<FunctionComponent>(new XButtonGroup(), FlowLayout.LEFT);
		}
		return fIntersectionPanel;
	}

	private JButton getBuildButton() {
		if(buildButton == null) {
			buildButton = new JButton(BUILD_BUTTON_TEXT);
			buildButton.setFocusable(false);
			buildButton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					handler.rebuildAll(true, null, RebuildCause.BUILD_BUTTON_TRIGGERED);
				}
			});
		}
		return buildButton;
	}
}
