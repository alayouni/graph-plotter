package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.components.other.IndexedToggle;
import com.alayouni.handyplotter.ui.components.splitpane.items.IndexedComponent;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/9/13
 * Time: 10:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndexedTogglesPanel<T extends IndexedComponent> extends JPanel {
	protected final XButtonGroup buttonGroup;

	public IndexedTogglesPanel(XButtonGroup buttonGroup) {
		this(buttonGroup, FlowLayout.CENTER);
	}

	public IndexedTogglesPanel(XButtonGroup buttonGroup, int orientation) {
		super(new FlowLayout(orientation, 0, 5));
		this.buttonGroup = buttonGroup;
	}


	public void addToggle(IndexedToggle<T> newToggle, int index) {
		add(newToggle, index);
		if(buttonGroup != null) {
			buttonGroup.add(newToggle);
		}
		revalidate();
	}

	public void removeToggle(T component) {
		IndexedToggle<T> toggleToRemove = findToggleForComponent(component);
		remove(toggleToRemove);
		if(buttonGroup != null) {
			buttonGroup.remove(toggleToRemove);
		}
		revalidate();
	}

	public IndexedToggle<T> findToggleForComponent(T component) {
		for(Component childToggle : getComponents()) {
			if(childToggle instanceof IndexedToggle && ((IndexedToggle<T>)childToggle).getDependencyComponent() == component) {
				return (IndexedToggle<T>) childToggle;
			}
		}
		return null;
	}

	public void resetAll() {
		IndexedToggle<T> childToggle;
		for(Component child : getComponents()) {
			if(child instanceof IndexedToggle) {
				childToggle = (IndexedToggle<T>)child;
				childToggle.setSelected(false);
				childToggle.setEnabled(true);
			}
		}
	}

	public IndexedToggle<T> getSelectedToggle() {
		if(buttonGroup != null) {
			return (IndexedToggle<T>)buttonGroup.getSelectedButton();
		}
		return null;
	}
}
