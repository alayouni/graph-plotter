package com.alayouni.handyplotter.ui.components.simpleexpression;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/28/13
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleExpressionException extends Exception {

    public SimpleExpressionException(String message) {
        super(message);
    }

}
