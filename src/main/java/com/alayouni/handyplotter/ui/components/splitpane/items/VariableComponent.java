package com.alayouni.handyplotter.ui.components.splitpane.items;

import com.alayouni.handyplotter.expressionutils.*;
import com.alayouni.handyplotter.ui.components.other.*;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionChangeListener;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionException;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionField;
import com.alayouni.handyplotter.ui.components.simpleexpression.SimpleExpressionValidator;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.handlers.Handler;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.ui.utils.ScrollableTextArea;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemExtension;
import com.alayouni.handyplotter.utils.DecimalFormatter;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/29/13
 * Time: 10:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class VariableComponent extends JPanel implements ItemExtension, IndexedComponent, ExpressionsHolder, RebuildExpressionInputsHolder {
	private static final String PRECISION_LABEL = "Precision";
	private static final int VALUE_LABEL_FONT_SIZE = 10;
	public static final String VARIABLE_PREFIX = "a";
	private static final int CODE_TOGGLE_SIZE = 20;
	private static final String DEFAULT_EXPRESSION = "return 1;";
	private static final Variable DEFAULT_VARIABLE = new Variable() {

		public Double f() throws Exception {
			return 1.;
		}
	};

	private int index;
	private VariableWrapper variable;
	private DecimalFormatter decimalFormatter = new DecimalFormatter(3);

	private CardLayout cardLayout = null;
	private JToggleButton codeToggle = null;
	private JPanel cardPanel = null;
	private SimpleExpressionField simpleExpressionField = null;
	private RebuildTextField precisionField;
	private static final String SIMPLE_EXPRESSION_FIELD = "simple";
	private ScrollableTextArea codeArea = null;
	private static final String CODE_AREA_FIELD = "code";

	private JLabel valueLabel = null;
	private JPanel labelNToggleContainer = null;
	private final Handler handler;

	private final VariableChangeListener variableChangeListener = new VariableChangeListener() {

		public void variableValueChanged(VariableWrapper v) throws Exception {
			updateValueLabel(v);
		}
	};

	public VariableComponent(int index, Handler handler) {
		super(new GridBagLayout());
		this.index = index;
		this.handler = handler;
		rebuildExpressionInputs = new RebuildExpressionInput[]{getCodeArea(),
				getSimpleExpressionField().getValueField(),
				getSimpleExpressionField().getStepField(),
				getDoublePrecisionField()};

		GBC gbc = new GBC(0, 0, 2, 1).setWeight(GBC.INFINITY, GBC.INFINITY).setFill(GBC.BOTH);
		this.add(getCardPanel(), gbc);

		gbc.gridy++;
		gbc.setWeight(1., 1.).setGridWidth(1);
		this.add(new JLabel(PRECISION_LABEL), gbc);

		gbc.gridx++;
		gbc.setWeightX(GBC.INFINITY).setLeftMargin(6);
		this.add(getDoublePrecisionField(), gbc);

		initiateVariable();
	}

	private JPanel getCardPanel() {
		if(cardPanel == null) {
			cardPanel = new JPanel(cardLayout = new CardLayout());
			cardPanel.add(getSimpleExpressionField(), SIMPLE_EXPRESSION_FIELD);
			cardPanel.add(getCodeArea(), CODE_AREA_FIELD);
			cardLayout.show(cardPanel, SIMPLE_EXPRESSION_FIELD);
		}
		return cardPanel;
	}


	private SimpleExpressionField getSimpleExpressionField() {
		if(simpleExpressionField == null) {
			SimpleExpressionValidator noNull = new SimpleExpressionValidator() {

				public void validate(Double value) throws SimpleExpressionException {
					if(value == null) {
						throw new SimpleExpressionException(VariableComponent.this + " cannot be null!");
					}
				}
			};
			simpleExpressionField = new SimpleExpressionField(1.,
					.1,
					VARIABLE_PREFIX,
					null,
					index,
					noNull);
			simpleExpressionField.setDecimalFormatter(decimalFormatter);
			simpleExpressionField.addChangeListener(new SimpleExpressionChangeListener() {

				public void simpleExpressionChanged(SimpleExpressionField field) throws Exception {
					if(field.getVariable().getVariable() == null) {
						variable.updateDependents(true);
						variable.fireVariableValueChanged();
						GraphPanel.getInstance().repaint();
					}
				}
			});
		}
		return simpleExpressionField;
	}

	private RebuildTextField getDoublePrecisionField() {
		if(precisionField == null) {
			precisionField = new RebuildTextField(String.valueOf(decimalFormatter.getPrecision()));
			precisionField.setBeforeBuildRunnable(new UnsafeRunnable() {

				public void run(RebuildCause rebuildCause) throws Exception {
					setPrecision();
					simpleExpressionField.firePrecisionUpdated();
					updateValueLabel(variable);
				}
			});
		}
		return precisionField;
	}


	private void setPrecision() throws Exception {
		final String identifier = toString() + " " + PRECISION_LABEL;
		String doublePrecisionTrimmed = precisionField.getText().trim();
		if(StringUtils.isEmpty(doublePrecisionTrimmed)) {
			throw new Exception(identifier + " cannot be empty");
		}

		try{
			int precision = Integer.parseInt(doublePrecisionTrimmed);
			if(precision <= 0) {
				throw new Exception(identifier + " must be positive");
			}
			decimalFormatter.setPrecision(precision);
			if(variable.getValue() != null) {
				updateValueLabel(variable);
			}
		} catch(NumberFormatException e) {
			throw new Exception(identifier + " must have integer format", e);
		}

	}

	@Override
	public String toString() {
		return variable.getIdentifier();
	}

	public String getExpression() throws Exception {
		if(getCodeToggle().isSelected()) {//codeArea is showing
			String text = getCodeArea().getText().trim();
			if(text.length() == 0) {
				text = "null";
			}
			return text;
		} else {
			return simpleExpressionField.getExpression();
		}
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
		this.variable.setIndex(index);
		simpleExpressionField.setIndex(index);
		updateValueLabel(variable);
	}

	public void reloadVariable() {
		variable = ExpressionBuilder.a.get(index);
		simpleExpressionField.setVariable(variable);
	}

	/**
	 * to be called after reloading variable after expressions get rebuilt
	 */
	public void fireExpressionsRebuilt() throws Exception {
		variable.addVariableChangeListener(variableChangeListener);
		simpleExpressionField.fireExpressionsRebuilt();
		updateValueLabel(variable);
		//no need to call setPrecision() as it gets called before the build
	}

	private void initiateVariable() {//temporary just to avoid null pointer exception
		variable = new VariableWrapper(VARIABLE_PREFIX, null, index, DEFAULT_EXPRESSION);
		variable.setVariable(DEFAULT_VARIABLE);
		variable.setValue(1.);
		ExpressionBuilder.a.add(index, variable);
	}

	public ScrollableTextArea getCodeArea() {
		if(codeArea == null) {
			codeArea = new ScrollableTextArea();
		}
		return codeArea;
	}

	private static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
	private JToggleButton getCodeToggle() {
		if(codeToggle == null) {
			codeToggle = new JToggleButton("{}") {
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			codeToggle.setPreferredSize(new Dimension(CODE_TOGGLE_SIZE, CODE_TOGGLE_SIZE));
			codeToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					//Been tested when codeToggle is not focusable this listener gets fired before any focusLost event

					//workaround to de-activate an eventual focusLost duplicated rebuild
					String valueFieldTrimmedText = handler.trimText(simpleExpressionField.getValueField().getText());
					simpleExpressionField.getValueField().setLastTrimmedText(valueFieldTrimmedText);

					String stepFieldTrimmedText = handler.trimText(simpleExpressionField.getStepField().getText());
					simpleExpressionField.getStepField().setLastTrimmedText(stepFieldTrimmedText);

					String precisionFieldTrimmedText = handler.trimText(precisionField.getText());
					precisionField.setLastTrimmedText(precisionFieldTrimmedText);
					//______________end de-activating eventual focusLost duplicated rebuild


					cardLayout.show(cardPanel, codeToggle.isSelected() ? CODE_AREA_FIELD : SIMPLE_EXPRESSION_FIELD);
					handler.rebuildAll(false, null, RebuildCause.VARIABLES_CHANGE);
				}
			});
			codeToggle.setFocusable(false);
		}
		return codeToggle;
	}


	private JLabel getValueLabel() {
		if(valueLabel == null) {
			valueLabel = new JLabel();
			valueLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, VALUE_LABEL_FONT_SIZE));
			updateValueLabel(variable);
		}
		return valueLabel;
	}


	public JPanel getExtension() {
		if(labelNToggleContainer == null) {
			labelNToggleContainer = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
			labelNToggleContainer.add(getValueLabel());
			labelNToggleContainer.add(getCodeToggle());
		}
		return labelNToggleContainer;
	}

	private void updateValueLabel(VariableWrapper v) {
		Double value = v.getValue();
		String prefix = variable.getIdentifier() + " = ";
		if(value == null) {
			valueLabel.setText(prefix + "null");
		} else {
			String formattedDouble = simpleExpressionField.formatDouble(value);
			valueLabel.setText(prefix + formattedDouble);
		}
	}



	public void updateExpressions(ExpressionProcessor processor) {
		String expression = codeArea.getText();
		expression = processor.processExpression(expression);
		codeArea.setText(expression);
		simpleExpressionField.updateExpressions(processor);
	}

	public void stopSpinningFieldIfRunning() {
		simpleExpressionField.stopSpinning();
	}

	private final RebuildExpressionInput[] rebuildExpressionInputs;

	public RebuildExpressionInput[] getRebuildExpressionInputs() {
		return rebuildExpressionInputs;
	}
}
