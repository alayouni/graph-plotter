package com.alayouni.handyplotter.ui.components.splitpane.items;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/29/13
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Hideable {

	boolean isHidden();

	void setHidden(boolean hidden);
}
