package com.alayouni.handyplotter.ui.components.splitpane.items;

import com.alayouni.handyplotter.expressionutils.*;
import com.alayouni.handyplotter.ui.components.other.IntersectionColorToggle;
import com.alayouni.handyplotter.ui.components.other.JoinPixelsToggle;
import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInput;
import com.alayouni.handyplotter.ui.components.other.RebuildExpressionInputsHolder;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.ScrollableTextArea;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemExtension;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/6/13
 * Time: 12:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class FunctionComponent extends JPanel implements ItemExtension, IndexedComponent, Colorable, Hideable, ExpressionsHolder,
																RebuildExpressionInputsHolder {
	private static final Dimension JOIN_PIXELS_TOGGLE_SIZE = new Dimension(20, 20);
	private static final Function DEFAULT_FUNCTION = new Function() {

		public Double f(double x) throws Exception {
			return null;
		}
	};
	private static final Color DEFAULT_COLOR = Color.black;
	public static final String FUNCTION_PREFIX = "f";

	private JLabel nameLabel = null;
	private ScrollableTextArea codeArea = null;
	private boolean hidden = false;

	private final List<IntersectionColorToggle<FunctionComponent>> fIntersections = new ArrayList<IntersectionColorToggle<FunctionComponent>>();

	private JToggleButton hideToggle;
	private JPanel extensionPanel;

	private JToggleButton joinPixels;

	private boolean inViewPort = false;


	private int index;
	private FunctionWrapper f;


	public FunctionComponent(int index) {
		this.index = index;
		rebuildExpressionInputs = new RebuildExpressionInput[]{getCodeArea()};
		f = new FunctionWrapper(index, "");
		f.setFunction(DEFAULT_FUNCTION);

		setLayout(new BorderLayout());
		add(getCodeArea(), BorderLayout.CENTER);
		setColor(DEFAULT_COLOR);
	}

	public ScrollableTextArea getCodeArea() {
		if(codeArea == null) {
			codeArea = new ScrollableTextArea();
		}
		return codeArea;
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
		f.setIndex(index);
		getNameLabel().setText(FUNCTION_PREFIX + index);
	}

	public JLabel getNameLabel() {
		if(nameLabel == null) {
			nameLabel = new JLabel(FUNCTION_PREFIX + index);
			nameLabel.setForeground(color);
		}
		return nameLabel;
	}


	public JPanel getExtension() {
		if(extensionPanel == null) {
			extensionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 0));
			extensionPanel.add(getHideToggle());
			extensionPanel.add(getJoinPixels());
			extensionPanel.add(getNameLabel());
		}
		return extensionPanel;
	}

	private JToggleButton getHideToggle() {
		if(hideToggle == null) {
			hideToggle = new JToggleButton("Hide"){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			hideToggle.setPreferredSize(EXTENSION_TOGGLE_SIZE);
			hideToggle.setFont(EXTENSION_TOGGLE_FONT);
			hideToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					setHidden(hideToggle.isSelected());
					GraphPanel.getInstance().repaint();
				}
			});
			hideToggle.setFocusable(false);
		}
		return hideToggle;
	}

	private JToggleButton getJoinPixels() {
		if(joinPixels == null) {
			joinPixels = new JoinPixelsToggle();
			joinPixels.setPreferredSize(JOIN_PIXELS_TOGGLE_SIZE);
			joinPixels.setToolTipText("Link pixels");
			joinPixels.setSelected(true);
			joinPixels.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					GraphPanel.getInstance().repaint();
				}
			});
			joinPixels.setFocusable(false);
		}
		return joinPixels;
	}

	public boolean isJoinPixels() {
		return getJoinPixels().isSelected();
	}



	public String getExpression() {
		String text = getCodeArea().getText().trim();
		if(text.length() == 0) {
			text = "null";
		}
		return text;
	}

	@Override
	public String toString() {
		return f.getIdentifier();
	}

	private Color color = DEFAULT_COLOR;

	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
		getNameLabel().setForeground(color);
		getHideToggle().setForeground(color);
	}




	//____________________________________intersections____________________________________________

	public List<IntersectionColorToggle<FunctionComponent>> getFIntersections() {
		return fIntersections;
	}

	public void addFIntersection(IntersectionColorToggle<FunctionComponent> intersection) {
		fIntersections.add(intersection);
	}

	public void removeFIntersection(IntersectionColorToggle<FunctionComponent> intersection) {
		fIntersections.remove(intersection);
	}

	public IntersectionColorToggle<FunctionComponent> findFIntersection(FunctionComponent fComp) {
		for(IntersectionColorToggle<FunctionComponent> intersection: fIntersections) {
			if(intersection.getIntersectionComponent() == fComp ||
					intersection.getDependencyComponent() == fComp) {
				return intersection;
			}
		}
		return null;
	}


	public boolean isHidden() {
		return hidden;
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
		getHideToggle().setSelected(hidden);
	}

	public Double f(double x) throws Exception {
		return f.f(x);
	}

	public void reInitiateFunction() {
		f = ExpressionBuilder.f.get(index);
	}


	public void updateExpressions(ExpressionProcessor processor) {
		String expression = codeArea.getText();
		expression = processor.processExpression(expression);
		codeArea.setText(expression);
	}

	public void reInitIntersections() {
		reInitIntersections(fIntersections);
	}

	private <V extends IndexedComponent> void reInitIntersections(List<IntersectionColorToggle<V>> intersections) {
		for(IntersectionColorToggle<V> intersection : intersections) {
			intersection.setLastComputedValue(null);
			intersection.setLastDiffDirection(null);
		}
	}

	public boolean isInViewPort() {
		return inViewPort;
	}

	public void setInViewPort(boolean inViewPort) {
		this.inViewPort = inViewPort;
	}

	private final RebuildExpressionInput[] rebuildExpressionInputs;

	public RebuildExpressionInput[] getRebuildExpressionInputs() {
		return rebuildExpressionInputs;
	}
}
