package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.components.other.IntersectionColorToggle;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/8/13
 * Time: 9:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntersectionsColorPanel extends JPanel {


	private final XButtonGroup buttonGroup;

	private JPanel fIntersectionsPanel = null;

	private IntersectionsLabelsPanel intersectionsLabelsPanel;


	public IntersectionsColorPanel(XButtonGroup buttonGroup) {
		super(new GridBagLayout());
		this.buttonGroup = buttonGroup;

		GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.NONE).setAnchor(GBC.NORTHWEST).setBottomMargin(8);
		fIntersectionsPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
		add(fIntersectionsPanel, gbc);

		//todo replace with f(x) color toggles
//		gbc.setGridY(1);
//		axisIntersectionsPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
//		add(axisIntersectionsPanel, gbc);

		gbc.setGridY(3).setWeightY(GBC.INFINITY);
		intersectionsLabelsPanel = new IntersectionsLabelsPanel(buttonGroup);
		add(intersectionsLabelsPanel, gbc);
	}

	public void addFIntersection(FunctionComponent functionComponent, FunctionComponent fIntersection) {
		IntersectionColorToggle<FunctionComponent> intersectionToggle = functionComponent.findFIntersection(fIntersection);
		if(intersectionToggle == null) {
			intersectionToggle = new IntersectionColorToggle<FunctionComponent>(functionComponent, fIntersection);
			functionComponent.addFIntersection(intersectionToggle);
			fIntersection.addFIntersection(intersectionToggle);
			intersectionsLabelsPanel.fireNewIntersectionAdded(intersectionToggle);
		}else {
			intersectionToggle.setHidden(false);
		}
		fIntersectionsPanel.add(intersectionToggle);
		buttonGroup.add(intersectionToggle);
		fIntersectionsPanel.revalidate();
		intersectionsLabelsPanel.firePossibleSelectionChanged();
	}

	public void removeFIntersection(FunctionComponent functionComponent, FunctionComponent fIntersectionComp) {
		//intersectionColorToggle should stay in memory ro recover previous settings if re-added back
		IntersectionColorToggle<FunctionComponent> intersectionToggle = functionComponent.findFIntersection(fIntersectionComp);
		intersectionToggle.setHidden(true);
		fIntersectionsPanel.remove(intersectionToggle);
		buttonGroup.remove(intersectionToggle);
		fIntersectionsPanel.revalidate();
		intersectionsLabelsPanel.firePossibleSelectionChanged();
	}


	public void functionRemoved(FunctionComponent fComponent) {
		FunctionComponent fIntersectionComp;
		for(IntersectionColorToggle<FunctionComponent> fIntersection : fComponent.getFIntersections()) {
			//even if fIntersection is just in fComponent memory but not under the panel/buttonGroup, following won't harm
			fIntersectionsPanel.remove(fIntersection);
			buttonGroup.remove(fIntersection);

			//for functions (unlike axises) the intersection relationship is bi-directional =>
			//the reference held by the other function must be deleted. fComponent is being deleted
			//=> no need to delete the reference from fComponent
			fIntersectionComp = fIntersection.getIntersectionComponent();
			if(fIntersectionComp == fComponent) {
				//the same reference to fIntersection is used by fComp and its intersection
				//make sure fIntersectionComp is different from fComp in order remove the intersection
				//from the remaining component (fComp is removed)
				fIntersectionComp = fIntersection.getDependencyComponent();
			}
			fIntersectionComp.removeFIntersection(fIntersection);
		}
		revalidate();
		intersectionsLabelsPanel.firePossibleSelectionChanged();
	}
}
