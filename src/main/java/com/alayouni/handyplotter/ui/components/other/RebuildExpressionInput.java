package com.alayouni.handyplotter.ui.components.other;

import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/12/13
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RebuildExpressionInput {
	boolean isValidateKey(KeyEvent e);

	String getText();

	String getLastTrimmedText();

	void setLastTrimmedText(String trimmedText);

	void addFocusListener(FocusListener focusListener);

	void addKeyListener(KeyListener keyListener);

	void setBeforeBuildRunnable(UnsafeRunnable runnable);

	UnsafeRunnable getBeforeBuildRunnable();
}
