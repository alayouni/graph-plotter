package com.alayouni.handyplotter.ui.components.other;

import com.alayouni.handyplotter.ui.components.splitpane.items.Colorable;
import com.alayouni.handyplotter.ui.components.splitpane.items.IndexedComponent;

import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/7/13
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ColorToggle<T extends IndexedComponent> extends ColorableToggle<T> {
	final List<Colorable> dependentColorables;

	public ColorToggle(final List<Colorable> dependentColorables, final T dependencyComponent) {
		super(dependencyComponent);
		this.dependentColorables = dependentColorables;
	}

	@Override
	public void setColor(Color color) {
		setForeground(color);
		if(dependentColorables != null) {
			for(Colorable colorable : dependentColorables) {
				colorable.setColor(color);
			}
		}
	}

	public void addDependentColorable(Colorable colorable) {
		if(dependentColorables != null) {
			dependentColorables.add(colorable);
			colorable.setColor(getColor());
		}
	}

	public void removeDependentColorable(Colorable colorable) {
		if(dependentColorables != null) {
			dependentColorables.remove(colorable);
		}
	}

}
