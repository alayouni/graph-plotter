package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/24/13
 * Time: 7:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisDynamicLocationProcessor extends AbstractSpecialProcessor<AxisDynamicLocation> {

	public AxisDynamicLocation evaluateSpecialExpression(String text) throws Exception {
		String upperCaseText = text.trim().toUpperCase();
		if(upperCaseText.equals(AxisDynamicLocation.MIN.toString())) {
			return AxisDynamicLocation.MIN;
		} else if(upperCaseText.equals(AxisDynamicLocation.CENTER.toString())){
			return AxisDynamicLocation.CENTER;
		} else if(upperCaseText.equals(AxisDynamicLocation.MAX.toString())) {
			return AxisDynamicLocation.MAX;
		}
		return null;
	}


	public String specialValueToString(AxisDynamicLocation specialValue) {
		return specialValue.toString().toLowerCase();
	}
}
