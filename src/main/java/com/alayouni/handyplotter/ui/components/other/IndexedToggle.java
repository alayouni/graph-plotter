package com.alayouni.handyplotter.ui.components.other;

import com.alayouni.handyplotter.ui.components.splitpane.items.IndexedComponent;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/31/13
 * Time: 9:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndexedToggle <T extends IndexedComponent> extends JToggleButton {
	final protected T dependencyComponent;

	private static final Dimension SIZE = new Dimension(25, 20);

	public IndexedToggle(T dependencyComponent) {
		super();
		this.dependencyComponent = dependencyComponent;
		setPreferredSize(SIZE);
		setFocusable(false);
	}

	@Override
	public String getText() {
		if(dependencyComponent != null) {
			return dependencyComponent.toString();
		}
		return "";
	}

	public T getDependencyComponent() {
		return dependencyComponent;
	}


	private static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
	@Override
	public Insets getInsets() {
		return ZERO_INSETS;
	}
}
