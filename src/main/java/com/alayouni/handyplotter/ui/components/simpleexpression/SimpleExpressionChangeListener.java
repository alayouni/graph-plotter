package com.alayouni.handyplotter.ui.components.simpleexpression;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/28/13
 * Time: 10:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SimpleExpressionChangeListener {

    void simpleExpressionChanged(SimpleExpressionField field) throws Exception;
}
