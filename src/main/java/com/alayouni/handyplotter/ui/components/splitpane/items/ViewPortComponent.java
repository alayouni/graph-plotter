package com.alayouni.handyplotter.ui.components.splitpane.items;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionsHolder;
import com.alayouni.handyplotter.expressionutils.Function;
import com.alayouni.handyplotter.expressionutils.VariableWrapper;
import com.alayouni.handyplotter.ui.components.other.*;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemExtension;
import com.alayouni.handyplotter.ui.viewport.ViewPortPanel;
import com.alayouni.handyplotter.utils.DecimalFormatter;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/31/13
 * Time: 5:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortComponent extends JPanel implements ItemExtension, IndexedComponent, Hideable, ExpressionsHolder, RebuildExpressionInputsHolder {
	private static final ImageIcon TOOLS_ICON = new ImageIcon(ViewPortComponent.class.getResource("/icons/tools.png"));
	private static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);

	private static final Color LINE_BORDER_COLOR = Color.BLACK;
	private static final int LINE_BORDER_THICKNESS = 1;

	private static final Dimension GRID_TOGGLES_SIZE = new Dimension(20, 20);
	private static final Dimension TOOLS_TOGGLES_SIZE = new Dimension(20, 20);
	private static final DecimalFormatter DEFAULT_FORMATTER = new DecimalFormatter(5);

	private static final String POSITION_TOP_LABEL = "Top";
	private static final String POSITION_LEFT_LABEL = "Left";
	private static final String WIDTH_LABEL = "Width";
	private static final String HEIGHT_LABEL = "Height";
	private static final String HORIZONTAL_AXIS_LABEL = "X Axis";
	private static final String VERTICAL_AXIS_LABEL = "Y Axis";

	private static final String HIDE_LABEL = "Hide";
	public static final String VIEW_PORT_PREFIX = "V";

	private int index;
	private final List<AxisComponent> axises;
	private List<FunctionComponent> mappedFunctions = new ArrayList<FunctionComponent>();
	private Function[] mappedFunctionsArray = new Function[0];

	private double top = 0;
	private double left = 0;
	private double widthRatio = 1;
	private double heightRatio = 1;

	private AxisComponent hAxis = null;
	private boolean hideHAxis = false;

	private AxisComponent vAxis = null;
	private boolean hideVAxis = false;

	private boolean showHorizontalGrid = false;
	private boolean showVerticalGrid = false;

	private boolean drawBorder = true;
	private boolean hidden = false;

	private ViewPortPanel viewPortPanel;

	private RebuildTextField positionTopField = null;
	private RebuildTextField positionLeftField = null;
	private RebuildTextField widthField = null;
	private RebuildTextField heightField = null;
	private JPanel hAxisPanel = null;
	private RebuildTextField hAxisField = null;
	private JToggleButton hideHAxisToggle = null;
	private GridToggle showHorizontalGridToggle = null;

	private JPanel vAxisPanel = null;
	private RebuildTextField vAxisField = null;
	private JToggleButton hideVAxisToggle = null;
	private GridToggle showVerticalGridToggle = null;

	private JLabel nameLabel = null;

	private JPanel extensionPanel = null;
	private BorderToggle borderToggle = null;
	private JToggleButton hideViewPortToggle = null;
	private JToggleButton showToolsToggle = null;


	private JPanel fieldsContainer = null;

	private JPanel zoomPanel = null;
	private JTextField zoomXField = null;
	private JTextField zoomYField = null;

	/**
	 * zoom values should be set only when a valid entry is parsed.
	 * a bad format in zoom fields should not break the build, it should just fail gracefully and keep the old values.
	 */
	private double zoomX = 0.75;
	private double zoomY = 0.75;

	/**
	 * <p>Will be used by the scrolling handler to notify each dependent on boundaries only once. If for instance
	 * hAxis.min and hAxis.max have a common dependent, in order to avoid notifying it twice because of a change on both
	 * min and max, boundaryDependentsWrapper is there for that.</p>
	 * <p>Will be set by ExpressionBuilder</p>
	 */
	private VariableWrapper boundaryDependentsWrapper;

	/**
	 * just for testing
	 */
	public ViewPortComponent() {
		axises = new ArrayList<AxisComponent>();
		rebuildExpressionInputs = new RebuildExpressionInput[]{
				getPositionLeftField(),
				getPositionTopField(),
				getWidthField(),
				getHeightField(),
				getHAxisField(),
				getVAxisField()

		};
	}

	public ViewPortComponent(int index, List<AxisComponent> axises) {
		super(new BorderLayout());
		this.index = index;
		this.axises = axises;
		rebuildExpressionInputs = new RebuildExpressionInput[]{
				getPositionLeftField(),
				getPositionTopField(),
				getWidthField(),
				getHeightField(),
				getHAxisField(),
				getVAxisField()
		};
		this.add(getFieldsContainer(), BorderLayout.CENTER);
	}

	private JPanel getFieldsContainer() {
		if(fieldsContainer == null) {
			fieldsContainer = new JPanel(new GridBagLayout());

			GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.HORIZONTAL).setAnchor(GBC.NORTH).setWeight(1., 1.).setBottomMargin(5);
			fieldsContainer.add(new JLabel(POSITION_LEFT_LABEL + " (%)"), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(WIDTH_LABEL + " (%)"), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(POSITION_TOP_LABEL + " (%)"), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(HEIGHT_LABEL + " (%)"), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(HORIZONTAL_AXIS_LABEL), gbc);

			gbc.gridy++;
			fieldsContainer.add(new JLabel(VERTICAL_AXIS_LABEL), gbc);

			gbc.setGridLocation(1, 0).setWeight(GBC.INFINITY, 1.).setLeftMargin(5);
			fieldsContainer.add(getPositionLeftField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getWidthField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getPositionTopField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getHeightField(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getHAxisPanel(), gbc);

			gbc.gridy++;
			fieldsContainer.add(getVAxisPanel(), gbc);

			gbc.gridy++;
			gbc.setWeightY(GBC.INFINITY).setGridX(0).setGridWidth(2).setLeftMargin(0);
			fieldsContainer.add(getZoomPanel(), gbc);

		}
		return fieldsContainer;
	}

	public RebuildTextField getPositionTopField() {
		if(positionTopField == null) {
			positionTopField = new RebuildTextField("0");
			setupHighLightViewPortListener(positionTopField);
		}
		return positionTopField;
	}

	public RebuildTextField getPositionLeftField() {
		if(positionLeftField == null) {
			positionLeftField = new RebuildTextField("0");
			setupHighLightViewPortListener(positionLeftField);
		}
		return positionLeftField;
	}

	public RebuildTextField getWidthField() {
		if(widthField == null) {
			widthField = new RebuildTextField("100");
			setupHighLightViewPortListener(widthField);
		}
		return widthField;
	}

	public RebuildTextField getHeightField() {
		if(heightField == null) {
			heightField = new RebuildTextField("100");
			setupHighLightViewPortListener(heightField);
		}
		return heightField;
	}

	private JPanel getHAxisPanel() {
		if(hAxisPanel == null) {
			hAxisPanel = new JPanel(new GridBagLayout());
			final double infinity = 9e100;
			GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.BOTH).setWeight(infinity, 1.).setMargins(0, 0, 0, 0);
			hAxisPanel.add(getHAxisField(), gbc);

			gbc.gridx++;
			gbc.setWeightX(1.);
			hAxisPanel.add(getShowHorizontalGridToggle(), gbc);

			gbc.gridx++;
			hAxisPanel.add(getHideHAxisToggle(), gbc);
		}
		return hAxisPanel;
	}

	public RebuildTextField getHAxisField() {
		if(hAxisField == null) {
			hAxisField = new RebuildTextField(AxisComponent.AXIS_PREFIX + "0");
			setupHighLightViewPortListener(hAxisField);
			//no need to read the axis as instantiating ViewPortComponent is triggered by it's multi-split pane
			//which will run a rebuild after this instantiation

		}
		return hAxisField;
	}

	private GridToggle getShowHorizontalGridToggle() {
		if(showHorizontalGridToggle == null) {
			showHorizontalGridToggle = new GridToggle(GridToggle.HORIZONTAL);
			showHorizontalGridToggle.setPreferredSize(GRID_TOGGLES_SIZE);
			showHorizontalGridToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					showHorizontalGrid = showHorizontalGridToggle.isSelected();
					try {
						vAxis.validateTooManyTicks();
					} catch (Exception e1) {
						GraphPanel.getInstance().setBroken(true);
					}
					updateView();
				}
			});
			showHorizontalGridToggle.setFocusable(false);
		}
		return showHorizontalGridToggle;
	}

	private JToggleButton getHideHAxisToggle() {
		if(hideHAxisToggle == null) {
			hideHAxisToggle = new JToggleButton(HIDE_LABEL){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			hideHAxisToggle.setFont(EXTENSION_TOGGLE_FONT);
			hideHAxisToggle.setPreferredSize(EXTENSION_TOGGLE_SIZE);
			hideHAxisToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					hideHAxis = hideHAxisToggle.isSelected();
					try {
						hAxis.validateTooManyTicks();
					} catch (Exception e1) {
						GraphPanel.getInstance().setBroken(true);
					}
					updateView();
				}
			});
			hideHAxisToggle.setFocusable(false);
		}
		return hideHAxisToggle;
	}

	private JPanel getVAxisPanel() {
		if(vAxisPanel == null) {
			vAxisPanel = new JPanel(new GridBagLayout());
			final double infinity = 9e100;
			GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.BOTH).setWeight(infinity, 1.).setMargins(0, 0, 0, 0);
			vAxisPanel.add(getVAxisField(), gbc);

			gbc.gridx++;
			gbc.setWeightX(1.);
			vAxisPanel.add(getShowVerticalGridToggle(), gbc);

			gbc.gridx++;
			vAxisPanel.add(getHideVAxisToggle(), gbc);
		}
		return vAxisPanel;
	}

	public RebuildTextField getVAxisField() {
		if(vAxisField == null) {
			vAxisField = new RebuildTextField(AxisComponent.AXIS_PREFIX + "1");
			setupHighLightViewPortListener(vAxisField);
			//no need to read the axis as instantiating ViewPortComponent is triggered by it's multi-split pane
			//which will run a rebuild after this instantiation
		}
		return vAxisField;
	}

	private GridToggle getShowVerticalGridToggle() {
		if(showVerticalGridToggle == null) {
			showVerticalGridToggle = new GridToggle(GridToggle.VERTICAL);
			showVerticalGridToggle.setPreferredSize(GRID_TOGGLES_SIZE);
			showVerticalGridToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					showVerticalGrid = showVerticalGridToggle.isSelected();
					try {
						hAxis.validateTooManyTicks();
					} catch (Exception e1) {
						GraphPanel.getInstance().setBroken(true);
					}
					updateView();
				}
			});
			showVerticalGridToggle.setFocusable(false);
		}
		return showVerticalGridToggle;
	}

	private JToggleButton getHideVAxisToggle() {
		if(hideVAxisToggle == null) {
			hideVAxisToggle = new JToggleButton(HIDE_LABEL){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			hideVAxisToggle.setFont(EXTENSION_TOGGLE_FONT);
			hideVAxisToggle.setPreferredSize(EXTENSION_TOGGLE_SIZE);
			hideVAxisToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					hideVAxis = hideVAxisToggle.isSelected();
					try {
						vAxis.validateTooManyTicks();
					} catch (Exception e1) {
						GraphPanel.getInstance().setBroken(true);
					}
					updateView();
				}
			});
			hideVAxisToggle.setFocusable(false);
		}
		return hideVAxisToggle;
	}

	private JPanel getExtensionPanel() {
		if(extensionPanel == null) {
			extensionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
			extensionPanel.add(getHideViewPortToggle());
			extensionPanel.add(getShowToolsToggle());
			extensionPanel.add(getBorderToggle());
			extensionPanel.add(getNameLabel());
		}
		return extensionPanel;
	}

	private BorderToggle getBorderToggle() {
		if(borderToggle == null) {
			borderToggle = new BorderToggle();
			borderToggle.setPreferredSize(GRID_TOGGLES_SIZE);
			borderToggle.setSelected(drawBorder);
			borderToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					drawBorder = borderToggle.isSelected();
					updateView();
				}
			});
			borderToggle.setFocusable(false);
		}
		return borderToggle;
	}

	private JToggleButton getShowToolsToggle() {
		if(showToolsToggle == null) {
			showToolsToggle = new JToggleButton(TOOLS_ICON) {
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			showToolsToggle.setPreferredSize(TOOLS_TOGGLES_SIZE);
			showToolsToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					viewPortPanel.setToolsVisible(showToolsToggle.isSelected());
				}
			});
			showToolsToggle.setSelected(true);
			showToolsToggle.setFocusable(false);
		}
		return showToolsToggle;
	}

	private JToggleButton getHideViewPortToggle() {
		if(hideViewPortToggle == null) {
			hideViewPortToggle = new JToggleButton(HIDE_LABEL){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			hideViewPortToggle.setFont(EXTENSION_TOGGLE_FONT);
			hideViewPortToggle.setPreferredSize(EXTENSION_TOGGLE_SIZE);
			hideViewPortToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					setHidden(hideViewPortToggle.isSelected());
				}
			});
			hideViewPortToggle.setFocusable(false);
		}
		return hideViewPortToggle;
	}

	private JLabel getNameLabel() {
		if(nameLabel == null) {
			nameLabel = new JLabel(toString());
			nameLabel.setFont(EXTENSION_TOGGLE_FONT);
			nameLabel.setFocusable(false);
		}
		return nameLabel;
	}

	private JPanel getZoomPanel() {
		if(zoomPanel == null) {
			zoomPanel = new JPanel(new GridBagLayout());

			GBC gbc = new GBC(0, 0, 1, 1).setFill(GBC.BOTH).setWeight(1., 1.).setRightMargin(4);
			JLabel zoomLabel = new JLabel("Zoom (%)");
			zoomPanel.add(zoomLabel, gbc);

			gbc.gridx++;
			gbc.setWeightX(GBC.INFINITY);
			JPanel zoomXPanel = createBorderedPanel(getZoomXField(), " X");
			zoomPanel.add(zoomXPanel, gbc);

			gbc.gridx++;
			gbc.setRightMargin(0);
			JPanel zoomYPanel = createBorderedPanel(getZoomYField(), " Y");
			zoomPanel.add(zoomYPanel, gbc);
		}
		return zoomPanel;
	}

	private JPanel createBorderedPanel(JTextField textField, String labelText) {
		JPanel borderedPanel = new JPanel(new BorderLayout(2, 0));
		borderedPanel.setBorder(new LineBorder(LINE_BORDER_COLOR, LINE_BORDER_THICKNESS));

		JLabel label = new JLabel(labelText);
		borderedPanel.add(label, BorderLayout.WEST);

		borderedPanel.add(textField, BorderLayout.CENTER);

		return borderedPanel;
	}

	private JTextField getZoomXField() {
		if(zoomXField == null) {
			zoomXField = new JTextField(DEFAULT_FORMATTER.format(zoomX * 100));
			zoomXField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_ENTER) {
						try {
							processXZoom();
						} catch (Exception e1) {
							ErrorDialog.showSafeFailErrorDialog(e1);
						}
					}
				}
			});
			zoomXField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					try {
						processXZoom();
					} catch (Exception e1) {
						ErrorDialog.showSafeFailErrorDialog(e1);
					}
				}
			});
			setupHighLightViewPortListener(zoomXField);
		}
		return zoomXField;
	}

	private JTextField getZoomYField() {
		if(zoomYField == null) {
			zoomYField = new JTextField(DEFAULT_FORMATTER.format(zoomY * 100));
			zoomYField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_ENTER) {
						try {
							processYZoom();
						} catch (Exception e1) {
							ErrorDialog.showSafeFailErrorDialog(e1);
						}
					}
				}
			});
			zoomYField.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) {
					try {
						processYZoom();
					} catch (Exception e1) {
						ErrorDialog.showSafeFailErrorDialog(e1);
					}
				}
			});
			setupHighLightViewPortListener(zoomYField);
		}
		return zoomYField;
	}

	public double processYZoom() throws Exception {
		String text = zoomYField.getText();
		Double parsedZoomY = readZoomValue("Y", text);
		zoomY = parsedZoomY;
		return zoomY;
	}

	public double processXZoom() throws Exception {
		String text = zoomXField.getText();
		Double parsedZoomX = readZoomValue("X", text);
		zoomX = parsedZoomX;
		return zoomX;
	}

	private Double readZoomValue(String label, String text) throws Exception {
		try {
			Double zoom = parseZoomValue(text);
			if(zoom == null) {
				throw new Exception(String.format("%s.zoom %s = '%s' must have double format", this, label, text));
			}  else if(zoom <= 0 || zoom > 1) {
				throw new Exception(String.format("%s.zoom %s must be between 0 and 100", this, label));
			}
			return zoom;
		} catch (NumberFormatException e) {
			throw new Exception(String.format("%s.zoom %s = '%s' must have double format", this, label, text));
		}
	}

	private Double parseZoomValue(String text) throws Exception{
		if(text.trim().length() > 0) {
			Double value = Double.parseDouble(text);
			return value / 100;
		}
		return null;
	}

	private final List<Component> focusableComponents = new ArrayList<Component>();
	private final FocusListener focusListener = new FocusAdapter() {//for coloring the borders of viewPortPanel on focus
		@Override
		public void focusGained(FocusEvent e) {
			if(hasFocus() && !viewPortPanel.isBeingEdited()) {
				viewPortPanel.setBeingEdited(true);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if(!hasFocus() && viewPortPanel.isBeingEdited()) {
				viewPortPanel.setBeingEdited(false);
			}
		}

		private boolean hasFocus() {
			for(Component comp : focusableComponents) {
				if(comp.hasFocus()) {
					return true;
				}
			}
			return false;
		}
	};

	/**
	 * for coloring view port panel border on focus
	 * @param component
	 */
	private void setupHighLightViewPortListener(Component component) {
		focusableComponents.add(component);
		component.addFocusListener(focusListener);
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
		try {
			if(hAxis != null) {
				hAxis.validateTooManyTicks();
			}
			if(vAxis != null) {
				vAxis.validateTooManyTicks();
			}
		} catch (Exception e) {
			GraphPanel.getInstance().setBroken(true);
		}
		viewPortPanel.setVisible(!hidden);
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
		getNameLabel().setText(VIEW_PORT_PREFIX + index);
	}


	public JPanel getExtension() {
		return getExtensionPanel();
	}

	@Override
	public String toString() {
		return VIEW_PORT_PREFIX + index;
	}


	private Double readRatio(RebuildTextField textField, final String fieldId) throws Exception {
		String text = textField.getText();
		if(StringUtils.isEmpty(text)) {
			throw new Exception(fieldId + " cannot be empty!");
		}

		try {
			double value = Double.parseDouble(text);
			return value / 100;
		} catch(NumberFormatException e) {
			throw new Exception(fieldId + " must have double format!", e);
		}
	}

	private AxisComponent readAxis(RebuildTextField axisField, final String fieldId) throws Exception {
		String trimmedText = axisField.getText().trim();
		if(trimmedText.startsWith("A")) {
			String indexText = trimmedText.substring(1);
			try {
				int index = Integer.parseInt(indexText);
				if(index < axises.size() && index >= 0) {
					return axises.get(index);
				}
			} catch(NumberFormatException ignored) {}
		}
		String errorMessage = fieldId + " must match the following format 'Ai', 'i' must match an existing Axis index. Example: A0";
		throw new Exception(errorMessage);
	}

	private void updateBoundaries() {
		viewPortPanel.updateBounds();
	}

	private void updateView() {
		viewPortPanel.repaint();
	}

	public int getXPixelsCount() {
		return viewPortPanel.getWidth();
	}

	public Double getXMinValue() {
		if(hAxis != null) {
			return hAxis.getMinValue();
		}
		return null;
	}

	public Double getXMaxValue() {
		if(hAxis != null) {
			return hAxis.getMaxValue();
		}
		return null;
	}

	public Double getYMinValue() {
		if(vAxis != null) {
			return vAxis.getMinValue();
		}
		return null;
	}

	public Double getYMaxValue() {
		if(vAxis != null) {
			return vAxis.getMaxValue();
		}
		return null;
	}


	public List<FunctionComponent> getMappedFunctions() {
		return mappedFunctions;
	}

	public void addMappedFunction(FunctionComponent fComp) {
		if(!mappedFunctions.contains(fComp)) {
			mappedFunctions.add(fComp);
		}
	}

	public void removeMappedFunction(FunctionComponent fComp) {
		mappedFunctions.remove(fComp);
	}


	public boolean isHidden() {
		return hidden;
	}

	public boolean isHideHAxis() {
		return hideHAxis;
	}

	public boolean isHideVAxis() {
		return hideVAxis;
	}

	public boolean isDrawBorder() {
		return drawBorder;
	}

	public boolean isShowHorizontalGrid() {
		return showHorizontalGrid;
	}

	public boolean isShowVerticalGrid() {
		return showVerticalGrid;
	}

	public AxisComponent getHAxis() {
		return hAxis;
	}

	public double getLeft() {
		return left;
	}

	public double getTop() {
		return top;
	}

	public AxisComponent getVAxis() {
		return vAxis;
	}

	public double getWidthRatio() {
		return widthRatio;
	}

	public double getHeightRatio() {
		return heightRatio;
	}

	public void setViewPortPanel(ViewPortPanel viewPortPanel) {
		this.viewPortPanel = viewPortPanel;
	}

	public void reloadParams() throws Exception {
		top = readRatio(positionTopField, POSITION_TOP_LABEL);
		left = readRatio(positionLeftField, POSITION_LEFT_LABEL);
		widthRatio = readRatio(widthField, WIDTH_LABEL);
		heightRatio = readRatio(heightField, HEIGHT_LABEL);
		//axises have already been read before the build through readAxisesInput()
		updateBoundaries();
	}

	public void setRatioBounds(double left, double top, double ratioWidth, double ratioHeight) {
		this.setRatioLocation(left, top);
		this.setRatioWidth(ratioWidth);
		this.setRatioHeight(ratioHeight);
	}


	public void setRatioLocation(double left, double top) {
		setRatioLeft(left);
		setRatioTop(top);
	}

	public void setRatioLeft(double left) {
		this.left = left;
		String formattedValue = formatRatio(left);
		positionLeftField.setText(formattedValue);
	}

	public void setRatioTop(double top) {
		this.top = top;
		String formattedValue = formatRatio(top);
		positionTopField.setText(formattedValue);
	}

	public void setRatioWidth(double width) {
		this.widthRatio = width;
		String formattedValue = formatRatio(width);
		widthField.setText(formattedValue);
	}

	public void setRatioHeight(double height) {
		this.heightRatio = height;
		String formattedValue = formatRatio(height);
		heightField.setText(formattedValue);
	}

	private String formatRatio(double ratio) {
		return DEFAULT_FORMATTER.format(ratio * 100.);
	}


	public void updateExpressions(ExpressionProcessor processor) {
		String textValue = hAxisField.getText();
		textValue = processor.processExpression(textValue);
		hAxisField.setText(textValue);

		textValue = vAxisField.getText();
		textValue = processor.processExpression(textValue);
		vAxisField.setText(textValue);

	}

	private final RebuildExpressionInput[] rebuildExpressionInputs;

	public RebuildExpressionInput[] getRebuildExpressionInputs() {
		return rebuildExpressionInputs;
	}

	public ViewPortPanel getViewPortPanel() {
		return viewPortPanel;
	}

	public void setBoundaryDependentsWrapper(VariableWrapper boundaryDependentsWrapper) {
		this.boundaryDependentsWrapper = boundaryDependentsWrapper;
	}

	public VariableWrapper getBoundaryDependentsWrapper() {
		return boundaryDependentsWrapper;
	}

	public void readAxisesInput() throws Exception {
		hAxis = readAxis(hAxisField, HORIZONTAL_AXIS_LABEL);
		vAxis = readAxis(vAxisField, VERTICAL_AXIS_LABEL);
	}

	private void clearBoundaryVariableDependencies(VariableWrapper boundaryVariable) {
		//create a new Set instance each call because having multiple boundaries refer to the same dependencies might
		//result in side effects when reprocessing dependencies (might not too... but just to be safe)
		Set<VariableWrapper> oneDependency = new HashSet<VariableWrapper>();
		oneDependency.add(boundaryDependentsWrapper);
		boundaryVariable.setDependencyVariables(oneDependency);
	}

	public Double getXUnit() throws Exception {
		return getHAxis().getUnitValue();
	}

	public Double getYUnit() throws Exception {
		return getVAxis().getUnitValue();
	}

	public boolean isXMinConstant() {
		return getHAxis().isMinConstant();
	}

	public boolean isYMinConstant() {
		return getVAxis().isMinConstant();
	}

	public boolean isXMaxConstant() {
		return getHAxis().isMaxConstant();
	}

	public boolean isYMaxConstant() {
		return getVAxis().isMaxConstant();
	}

	public void switchXMinToConstantState() {
		clearBoundaryVariableDependencies(getHAxis().getMinVariable());
		getHAxis().switchMinFieldToConstantState();
	}

	public void switchYMinToConstantState() {
		clearBoundaryVariableDependencies(getVAxis().getMinVariable());
		getVAxis().switchMinFieldToConstantState();
	}

	public void switchXMaxToConstantState() {
		clearBoundaryVariableDependencies(getHAxis().getMaxVariable());
		getHAxis().switchMaxFieldToConstantState();
	}

	public void switchYMaxToConstantState() {
		clearBoundaryVariableDependencies(getVAxis().getMaxVariable());
		getVAxis().switchMaxFieldToConstantState();
	}

	public void setXMinValue(double xMinValue) {
		getHAxis().setMinValue(xMinValue);
	}

	public void setXMaxValue(double xMaxValue) {
		getHAxis().setMaxValue(xMaxValue);
	}

	public void setYMinValue(double yMinValue) {
		getVAxis().setMinValue(yMinValue);
	}

	public void setYMaxValue(double yMaxValue) {
		getVAxis().setMaxValue(yMaxValue);
	}

	public void updateBoundaryDependents() throws Exception {
		boundaryDependentsWrapper.updateDependents(true);
	}

	public Function[] getMappedFunctionsArray() {
		return mappedFunctionsArray;
	}

	public void setMappedFunctionsArray(Function[] mappedFunctionsArray) {
		this.mappedFunctionsArray = mappedFunctionsArray;
	}
}
