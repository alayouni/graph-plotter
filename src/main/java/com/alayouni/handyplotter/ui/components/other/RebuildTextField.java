package com.alayouni.handyplotter.ui.components.other;

import javax.swing.*;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/18/13
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class RebuildTextField extends JTextField implements RebuildExpressionInput {
	UnsafeRunnable beforeBuildRunnable = null;

	public RebuildTextField() {
	}

	public RebuildTextField(String text) {
		super(text);
	}


	public boolean isValidateKey(KeyEvent e) {
		return e.getKeyCode() == KeyEvent.VK_ENTER;
	}

	private String lastTrimmed = "";

	public String getLastTrimmedText() {
		return this.lastTrimmed;
	}


	public void setLastTrimmedText(String trimmedText) {
		this.lastTrimmed = trimmedText;
	}


	public void setBeforeBuildRunnable(UnsafeRunnable runnable) {
		this.beforeBuildRunnable = runnable;
	}


	public UnsafeRunnable getBeforeBuildRunnable() {
		return beforeBuildRunnable;
	}

}
