package com.alayouni.handyplotter.ui.components.other;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/31/13
 * Time: 8:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class GridToggle extends JToggleButton {
	public static final int HORIZONTAL = 1;
	public static final int VERTICAL = 2;

	private static final int LINES_NUMBER = 3;
	private static final int LINES_SPACING = 4;
	private static final int MARGIN = 3;
	private static final Color LINES_COLOR = Color.black;

	final int orientation;

	public GridToggle(int orientation) {
		this.orientation = orientation;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(LINES_COLOR);
		switch (orientation) {
			case HORIZONTAL:
				drawHorizontalGrid(g);
				break;
			default:
				drawVerticalGrid(g);
				break;
		}
	}

	private void drawHorizontalGrid(Graphics g) {
		int linesHeight = computeLinesLength();
		int y = (getHeight() - linesHeight) / 2 + 2;
		int x1 = MARGIN + 1, x2 = getWidth() - MARGIN - 2;
		for(int i = 0; i < LINES_NUMBER; i++, y+=LINES_SPACING) {
			g.drawLine(x1, y, x2, y);
		}
	}

	private void drawVerticalGrid(Graphics g) {
		int linesWidth = computeLinesLength();
		int x = (getWidth() - linesWidth) / 2 + 1;
		int y1 = MARGIN + 1, y2 = getHeight() - MARGIN - 1;
		for(int i = 0; i < LINES_NUMBER; i++, x += LINES_SPACING) {
			g.drawLine(x, y1, x, y2);
		}
	}

	private int computeLinesLength() {
		return (LINES_NUMBER - 1) * (LINES_SPACING) + LINES_NUMBER;
	}
}
