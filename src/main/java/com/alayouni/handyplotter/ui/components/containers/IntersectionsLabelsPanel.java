package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.components.other.IntersectionColorToggle;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/24/13
 * Time: 6:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntersectionsLabelsPanel extends JPanel {
	private static final Dimension TOGGLES_SIZE = new Dimension(60, 20);
	private static final String SHOW_X_LABEL = "Show X";
	private static final String SHOW_Y_LABEL = "Show Y";

	final XButtonGroup intersectionsButtonGroup;

	private JToggleButton showXValueToggle = null;
	private JToggleButton showYValueToggle = null;

	public IntersectionsLabelsPanel(XButtonGroup intersectionsButtonGroup) {
		super(new FlowLayout(FlowLayout.LEADING, 0, 0));
		this.intersectionsButtonGroup = intersectionsButtonGroup;
		this.add(getShowXValueToggle());
		this.add(getShowYValueToggle());
		setEnabled(false);
	}

	/**
	 * if there's no selection or the selection is not an instance of IntersectionColorToggle the label toggles will be disabled,
     * otherwise the visible labels (if any) will be selected accordingly.
	 */
	public void firePossibleSelectionChanged() {
		AbstractButton selection = intersectionsButtonGroup.getSelectedButton();
		if(selection != null && selection instanceof IntersectionColorToggle) {
			this.setEnabled(true);
			IntersectionColorToggle intersection = (IntersectionColorToggle)selection;
			showXValueToggle.setSelected(intersection.isXVisible());
			showYValueToggle.setSelected(intersection.isYVisible());
		} else {
			this.setEnabled(false);
		}
	}

	public void fireNewIntersectionAdded(IntersectionColorToggle newIntersection) {
		newIntersection.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent e) {
				firePossibleSelectionChanged();
			}
		});
	}


	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		showXValueToggle.setEnabled(enabled);
		showYValueToggle.setEnabled(enabled);
		if(!enabled) {
			showXValueToggle.setSelected(false);
			showYValueToggle.setSelected(false);
		}
	}


	private static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
	private JToggleButton getShowXValueToggle() {
		if(showXValueToggle == null) {
			showXValueToggle = new JToggleButton(SHOW_X_LABEL){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			showXValueToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					IntersectionColorToggle selectedIntersection = getSelectedIntersectionIfAny();
					if(selectedIntersection != null) {
						selectedIntersection.setXVisible(showXValueToggle.isSelected());
						GraphPanel.getInstance().repaint();
					}
				}
			});
			showXValueToggle.setPreferredSize(TOGGLES_SIZE);
			showXValueToggle.setFocusable(false);
		}
		return showXValueToggle;
	}

	private JToggleButton getShowYValueToggle() {
		if(showYValueToggle == null) {
			showYValueToggle = new JToggleButton(SHOW_Y_LABEL){
				@Override
				public Insets getInsets() {
					return ZERO_INSETS;
				}
			};
			showYValueToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					IntersectionColorToggle selectedIntersection = getSelectedIntersectionIfAny();
					if(selectedIntersection != null) {
						selectedIntersection.setYVisible(showYValueToggle.isSelected());
						GraphPanel.getInstance().repaint();
					}
				}
			});
			showYValueToggle.setPreferredSize(TOGGLES_SIZE);
			showYValueToggle.setFocusable(false);
		}
		return showYValueToggle;
	}


	private IntersectionColorToggle getSelectedIntersectionIfAny() {
		return (IntersectionColorToggle) intersectionsButtonGroup.getSelectedButton();
	}
}
