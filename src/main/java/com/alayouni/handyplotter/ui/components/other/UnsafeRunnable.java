package com.alayouni.handyplotter.ui.components.other;

/**
 *
 * Unsafe meaning might throw an exception
 */
public interface UnsafeRunnable {
	void run(RebuildCause rebuildCause) throws Exception;
}
