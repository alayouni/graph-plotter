package com.alayouni.handyplotter.ui.components.simpleexpression;

import com.alayouni.handyplotter.expressionutils.ExpressionProcessor;
import com.alayouni.handyplotter.expressionutils.ExpressionsHolder;
import com.alayouni.handyplotter.expressionutils.VariableChangeListener;
import com.alayouni.handyplotter.expressionutils.VariableWrapper;
import com.alayouni.handyplotter.ui.components.other.RebuildTextField;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.GBC;
import com.alayouni.handyplotter.utils.DecimalFormatter;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/28/13
 * Time: 8:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleExpressionField<T> extends JPanel implements ExpressionsHolder {
	private static final Dimension FIELD_PREFERRED_SIZE = new Dimension(80, 25);

	private VariableWrapper variable = null;
	private final Double defaultValue;

	private double step;
	private final double defaultStepValue;

	private SimpleExpressionValidator validator = null;
	private SpecialExpressionProcessor<T> specialExpressionProcessor = null;

	/**
	 * used to preserve the precise value after a rebuild - will be un-flagged by a document listener and then flagged
	 * whenever needed, following are some details:<br>
	 * setText() will trigger the document listeners which will be notified
	 * in the same thread within the call. meaning after the setText() call all document listeners will be notified.
	 * The document listener will un-flag useBackupValueOnRebuild which is not always desired when triggered by setText()
	 * setText() is called either by the spinning mechanism, the updatePrecision() method or when setting a precise value
	 * through setValue()
	 * in all these cases useBackupValueOnRebuild should be flagged => thus they are followed by a flag operation
	 * right after setText() gets called
	 * ... all this headache to avoid overriding the precise value after a spin with a step finer than precision
	 * followed by a change of precision. A change of precision will re-build which will call processNewValue()
	 * which will re-read the value if useBackupValueOnRebuild is un-flagged.
	 * A re-read shouldn't happen if the last interaction was a spin
	 * A re-read should happen only if last interaction was user input through typed in valueField
	 */
	private boolean useBackupValueOnRebuild = true;

	private RebuildTextField valueField;
	private RebuildTextField stepField;
	int index;
	private DecimalFormatter decimalFormatter = new DecimalFormatter(3);

	private List<SimpleExpressionChangeListener> listeners = new ArrayList<SimpleExpressionChangeListener>();

	public SimpleExpressionField(Double defaultValue,
	                             double defaultStepValue,
	                             final String prefix,
	                             final String suffix,
	                             int index,
	                             SimpleExpressionValidator validator) {
		this.lastValue = this.defaultValue = defaultValue;
		//just temporarily to avoid null pointer exception
		variable = new VariableWrapper(prefix, suffix, index, "");
		variable.setValue(defaultValue);

		this.step = this.defaultStepValue = defaultStepValue;
		this.index = index;
		this.validator = validator;
		init();
	}

	public SimpleExpressionField(Double defaultValue,
	                             double defaultStepValue,
	                             final String prefix,
	                             final String suffix,
	                             int index,
	                             SimpleExpressionValidator validator,
	                             SpecialExpressionProcessor<T> specialExpressionProcessor) {
		this(defaultValue, defaultStepValue, prefix, suffix, index, validator);
		this.specialExpressionProcessor = specialExpressionProcessor;
	}


	private void init() {
		this.setLayout(new GridBagLayout());

		GBC gbc = new GBC();
		gbc.setWeight(1., 1.).setFill(GBC.BOTH).setGridBounds(0, 0, 1, 1).setMargins(0, 0, 5, 0);
		this.add(getValueField(), gbc);

		gbc.setWeightY(GBC.INFINITY).setGridY(1).setFill(GBC.HORIZONTAL).setAnchor(GBC.NORTH);
		this.add(getStepField(), gbc);
	}

	private boolean upPressed = false;
	private boolean downPressed = false;
	public RebuildTextField getValueField() {
		if(valueField == null) {
			valueField = new RebuildTextField();
			valueField.setPreferredSize(FIELD_PREFERRED_SIZE);
			if(defaultValue != null) {
				valueField.setText(formatDouble(defaultValue));
			} else {
				valueField.setText("null");
			}
			//adding the document listener after the setText calls so that no un-flagging happen when
			// initially setting the text
			valueField.getDocument().addDocumentListener(new DocumentListener() {

				public void insertUpdate(DocumentEvent e) {
					useBackupValueOnRebuild = false;
				}


				public void removeUpdate(DocumentEvent e) {
					useBackupValueOnRebuild = false;
				}


				public void changedUpdate(DocumentEvent e) {
					useBackupValueOnRebuild = false;
				}
			});
			valueField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if(!upPressed && e.getKeyCode() == KeyEvent.VK_UP) {
						upPressed = true;
						startSpinning(true);
					} else if(!downPressed && e.getKeyCode() == KeyEvent.VK_DOWN) {
						downPressed = true;
						startSpinning(false);
					}
				}

				@Override
				public void keyReleased(KeyEvent e) {
					if(e.getKeyCode() == KeyEvent.VK_UP) {
						stopSpinning();
					} else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
						stopSpinning();
					}

				}
			});
		}
		return valueField;
	}


	private final SpinRunnable spinRun = new SpinRunnable();

	private void startSpinning(boolean up) {
		if(variable.getVariable() == null && spinRun.isStopped() && getValue() !=null) {
			spinRun.initiate(up);
			new Thread(spinRun).start();
		}
	}

	public void stopSpinning() {
		upPressed = false;
		downPressed = false;
		if(!spinRun.isStopped()) {
			spinRun.stopSpinning();
		}
	}

	public RebuildTextField getStepField() {
		if(stepField == null) {
			stepField = new RebuildTextField();
			stepField.setPreferredSize(FIELD_PREFERRED_SIZE);
			stepField.setText(formatDouble(defaultStepValue));
		}
		return stepField;
	}


	private void parseStepValue() throws Exception {
		String trimmedExpr = stepField.getText().trim();
		if(trimmedExpr.isEmpty()) {
			step = defaultStepValue;
		} else {
			step = readValue(trimmedExpr);
			if(step == 0) {//step should never be zero
				step = defaultStepValue;
			}
		}

	}

	public String formatDouble(double d) {
		return decimalFormatter.format(d, step);
	}


	public Double getValue() {
		return variable.getValue();
	}

	private final VariableChangeListener variableChangeListener = new VariableChangeListener() {

		public void variableValueChanged(VariableWrapper v) throws Exception {
			updateStepField();
		}
	};
	private boolean processNewValue() throws Exception {
		variable.addVariableChangeListener(variableChangeListener);
		T specialValue = getSpecialValue();
		if(specialValue == null) {
			String trimmedText = valueField.getText().trim();
			Double constant = readValue(trimmedText);
			if(useBackupValueOnRebuild && constant != null) {
				variable.setValue(lastValue);
			}
			useBackupValueOnRebuild = true;
			if(constant != null) {
				variable.setVariable(null);
			}
		} else {
			variable.setValue(null);
			variable.setVariable(null);
		}
		if(validator != null && specialValue == null) {
			validator.validate(getValue());
		}

		boolean isConstantValue = (variable.getVariable() == null) && (specialValue == null);
		boolean wasConstant = stepField.isEditable();
		stepField.setFocusable(isConstantValue);
		stepField.setEditable(isConstantValue);
		if(!isConstantValue || !wasConstant) {
			// the only case where stepField should not be updated is when the value was a constant and
			// is still a constant. Only in that case there's a chance stepField was subject to user interaction and thus
			// the entered text should be kept as is to get parsed later
			// in all other cases stepField text must get updated
			updateStepField();
		}
		return isConstantValue;
	}

	private Double readValue(String trimmedExpr) {
		try {
			return Double.parseDouble(trimmedExpr);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**
	 * Changes will be fired only when they occur due to user interaction with the internal value component.
	 * Changes invoked by external components should be handled externally.
	 * @param listener
	 */
	public void addChangeListener(SimpleExpressionChangeListener listener) {
		listeners.add(listener);
	}


	private Double lastValue;
	/**
	 * Should be fired only after a user interaction with valueField. Changes invoked by external components
	 * should be handled externally.
	 */
	private void fireExpressionChanged() throws Exception {
		Double value = this.getValue();
		if(value != null && !value.equals(this.lastValue)) {
			this.lastValue = value;
			variable.setValue(value);
			if(validator != null) {
				validator.validate(value);
			}
			for(SimpleExpressionChangeListener listener : listeners) {
				listener.simpleExpressionChanged(this);
			}

		}
	}


	public void updateExpressions(ExpressionProcessor processor) {
		String expression = valueField.getText();
		String processedExpression = processor.processExpression(expression);
		if(!processedExpression.equals(expression)) {
			//setText should only be called when needed because it's being observed
			valueField.setText(processedExpression);
			useBackupValueOnRebuild = true;
		}
	}



	private final EventDispatchValueUpdater eventDispatchValueUpdater = new EventDispatchValueUpdater();

	/**
	 * Not using SwingWorker because it's using synchronization which is bad for performance.
	 */
	private class SpinRunnable implements Runnable {

		private static final int SPIN_FREQUENCY = 50;//in ms

		boolean upSpin;
		private boolean exit = true;


		public void run() {
			try {
				while(!exit) {
					spin();
				}
			} catch (Exception e) {
				stopSpinning();
				GraphPanel.getInstance().setBroken(true);
			}
		}

		private void spin() throws Exception {
			variable.incrementValue(upSpin ? step : -step);
			if(validator != null) {
				validator.validate(getValue());
			}
			SwingUtilities.invokeLater(eventDispatchValueUpdater);
			Thread.sleep(SPIN_FREQUENCY);
		}

		public void stopSpinning() {
			exit = true;
		}

		public void initiate(boolean upSpin) {
			this.upSpin = upSpin;
			exit = false;
		}

		public boolean isStopped() {
			return exit;
		}
	}

	private class EventDispatchValueUpdater implements Runnable{


		public void run() {
			getValueField().setText(formatDouble(getValue()));
			useBackupValueOnRebuild = true;
			try {
				fireExpressionChanged();
			} catch (Exception e) {
				stopSpinning();
				GraphPanel.getInstance().setBroken(true);
			}
		}
	}

	/**
	 * When a variable is used and not a constant, stepField is used to display the value of the variable, or the
	 * value representation of the special expression.<br/>
	 * Gets called also from processNewValue() when the value changes from a non constant to a constant, to revert
	 * the display of the previous step value.
	 */
	public void updateStepField() throws Exception {
		if(getSpecialValue() != null) {
			String text = valueField.getText().trim();
			String specialValueAsStr = specialExpressionProcessor.stringValue(text);
			stepField.setText(specialValueAsStr);
		} else if(variable.getVariable() != null) {
			if(validator != null) {
				validator.validate(getValue());
			}
			String formattedValue = formatDouble(getValue());
			stepField.setText(formattedValue);
		} else {
			stepField.setText(formatDouble(step));
		}
	}

	public VariableWrapper getVariable() {
		return variable;
	}

	public String getExpression() throws Exception {
		//if this method gets called means a rebuild is being prepared => backup value
		lastValue = getValue();
		String text = valueField.getText().trim();
		if(text.length() == 0) {
			text = "null";
		} else if(getSpecialValue() != null) {
			return specialExpressionProcessor.getVariableExpression(text);
		}
		return text;
	}

	public void setVariable(VariableWrapper variable) {
		this.variable = variable;
	}

	/**
	 * setVariable() must always be called on all expressionFields before firing an ExpressionRebuilt event.
	 * @throws Exception
	 */
	public void fireExpressionsRebuilt() throws Exception {
		if(processNewValue()) {
			parseStepValue();
		}
		this.lastValue = getValue();
	}

	private Insets insets = super.getInsets();
	@Override
	public Insets getInsets() {
		return insets;
	}

	public void setInsets(Insets insets) {
		this.insets = insets;
	}

	public void setDecimalFormatter(DecimalFormatter decimalFormatter) {
		this.decimalFormatter = decimalFormatter;
	}

	@Override
	public void addKeyListener(KeyListener keyListener) {
		valueField.addKeyListener(keyListener);
	}

	@Override
	public void addFocusListener(FocusListener focusListener) {
		valueField.addFocusListener(focusListener);
	}

	public void setIndex(int index) {
		this.index = index;
		variable.setIndex(index);
	}

	public String getIdentifier() {
		return variable.getIdentifier();
	}

	public void validateValue() throws Exception {
		if(validator != null) {
			validator.validate(getValue());
		}
	}

	public void firePrecisionUpdated() throws Exception {
		//this method gets triggered by precision field
		//meaning when it gets triggered this component doesn't have the focus
		//and has already it's value set correctly => no need to worry about the useBackupValueOnRebuild flag
		//mentioning this because setText() will un-flag useBackupValueOnRebuild, which might seem odd if one
		//doesn't keep in mind that this method is triggered only by a precision field after
		//value has been processed correctly back when this component got validated or lost focus
		if(variable.getVariable() == null && getValue() != null) {
			getValueField().setText(formatDouble(getValue()));
			useBackupValueOnRebuild = true;
		} else {
			updateStepField();
		}
	}


	/**
	 * This method does the following :
	 * <ul>
	 * <li>sets the text of valueField</li>
	 * <li>sets variable.value to value</li>
	 * <li>ensures useBackupValueOnRebuild gets flagged so that parsing valueField on next rebuild doesn't
	 * override the precise set          double value</li>
	 * </ul>
	 * <p>Note that variable.variable won't be set to null, if needed it should be set externally.</p>
	 * @see #switchToConstantState()
	 * @param value
	 */
	public void setValue(double value) {
		this.variable.setValue(value);
		valueField.setText(formatDouble(value));
		useBackupValueOnRebuild = true;
	}

	public T getSpecialValue() throws Exception {
		if(specialExpressionProcessor != null) {
			return specialExpressionProcessor.evaluateSpecialExpression(valueField.getText());
		}
		return null;
	}

	/**
	 * a rebuild should be performed whenever ready after calling this method
	 * @param specialValue
	 */
	public void setSpecialValue(T specialValue) {
		if(specialExpressionProcessor != null) {
			variable.setValue(null);
			variable.setVariable(null);
			String specialValueToString = specialExpressionProcessor.specialValueToString(specialValue);
			valueField.setText(specialValueToString);
			useBackupValueOnRebuild = true;
		}
	}

	public void switchToConstantState() {
		variable.setVariable(null);
		stepField.setFocusable(true);
		stepField.setEditable(true);
		stepField.setText(formatDouble(step));
	}

}
