package com.alayouni.handyplotter.ui.components.splitpane.items.axis;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/24/13
 * Time: 8:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class AxisTicksCountProcessor extends AbstractSpecialProcessor<Integer> {
	static final String REGEX = "(-?\\d+)(\\s*[t|T])";
	static final Pattern PATTERN = Pattern.compile(REGEX);


	public Integer evaluateSpecialExpression(String text) throws Exception {
		Matcher matcher = PATTERN.matcher(text.trim());
		if(matcher.find()) {
			String digits = matcher.group(1);
			return Integer.parseInt(digits);
		}
		return null;
	}


	public String specialValueToString(Integer specialValue) {
		return specialValue + "t";
	}
}
