package com.alayouni.handyplotter.ui.components.other;

import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.Hideable;
import com.alayouni.handyplotter.ui.components.splitpane.items.IndexedComponent;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/7/13
 * Time: 9:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntersectionColorToggle<T extends IndexedComponent> extends ColorToggle<FunctionComponent> implements Hideable {
	private final T intersectionComponent;

	private boolean hidden = false;

	/**
	 * to be used when drawing intersections.
	 */
	private Double lastComputedValue;

	private Integer lastDiffDirection;

	private boolean isXVisible = true;
	private boolean isYVisible = true;


	public IntersectionColorToggle(final FunctionComponent functionComp, final T intersectionComponent) {
		super(null, functionComp);
		this.intersectionComponent = intersectionComponent;
		setPreferredSize(new Dimension(60, 20));
		setFocusable(false);
	}

	private static final Insets INSETS = new Insets(0, 0, 5, 0);
	@Override
	public Insets getInsets() {
		return INSETS;
	}

	@Override
	public String getText() {
		if(dependencyComponent != null) {//when calling super on the constructor getText() get called before dependencyComponent gets initialized
			String hexColor = Integer.toHexString(getColor().getRGB()).substring(2);
			String text = String.format("<html><font color=#%s>%s<font size=6>&cap;</font>%s</font>",
					hexColor,
					dependencyComponent,
					intersectionComponent);
			//hack to apply html text, without this html text doesn't display
			if(super.getText().compareTo(text) != 0) {
				setText(text);
			}
			return text;
		}
		return "";
	}

	public T getIntersectionComponent() {
		return intersectionComponent;
	}


	public boolean isHidden() {
		return hidden;
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public Double getLastComputedValue() {
		return lastComputedValue;
	}

	public void setLastComputedValue(Double lastComputedValue) {
		this.lastComputedValue = lastComputedValue;
	}

	public Integer getLastDiffDirection() {
		return lastDiffDirection;
	}

	public void setLastDiffDirection(Integer lastDiffDirection) {
		this.lastDiffDirection = lastDiffDirection;
	}

	public boolean isXVisible() {
		return isXVisible;
	}

	public void setXVisible(boolean XVisible) {
		isXVisible = XVisible;
	}

	public boolean isYVisible() {
		return isYVisible;
	}

	public void setYVisible(boolean YVisible) {
		isYVisible = YVisible;
	}
}
