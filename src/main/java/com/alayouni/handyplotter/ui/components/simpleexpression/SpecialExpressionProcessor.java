package com.alayouni.handyplotter.ui.components.simpleexpression;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/24/13
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SpecialExpressionProcessor<T> {
	T evaluateSpecialExpression(String text) throws Exception;

	/**
	 * @param specialValue
	 * @return the string representation of a special value. To be displayed valueField of the SimpleExpressionField.
	 */
	String specialValueToString(T specialValue);

	/**
	 * @param text
	 * @return the evaluated special value represented by text. To be displayed in stepField of SimpleExpressionField.
	 */
	String stringValue(String text);

	String getVariableExpression(String text);
}
