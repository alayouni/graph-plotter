package com.alayouni.handyplotter.ui.components.containers;

import com.alayouni.handyplotter.ui.components.other.ColorToggle;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/7/13
 * Time: 8:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ColorsPanel extends JPanel {

	private IndexedTogglesPanel<FunctionComponent> fColorsPanel;
	private IndexedTogglesPanel<AxisComponent> axisesColorsPanel;

	private IntersectionsColorPanel intersectionsColorPanel;

	private JColorChooser colorChooser;
	private JPanel colorChooserSwatchesPanel;
	private MultiSplitPane<FunctionComponent> functionsSplit;

	private XButtonGroup buttonGroup;

	public ColorsPanel(MultiSplitPane<FunctionComponent> functionsSplit) {
		super(new BorderLayout(20, 10));
		this.functionsSplit = functionsSplit;
		this.buttonGroup = new XButtonGroup();
		this.layoutComponents();
	}

	private void layoutComponents() {
		JPanel colorPanelsContainer = new PanelsContainer(getfColorsPanel(), getAxisesColorsPanel());
		this.add(colorPanelsContainer, BorderLayout.NORTH);
		this.add(getColorChooserSwatchesPanel(), BorderLayout.WEST);
		this.add(getIntersectionsColorPanel(), BorderLayout.CENTER);

	}

	public IndexedTogglesPanel<FunctionComponent> getfColorsPanel() {
		if(fColorsPanel == null) {
			fColorsPanel = new IndexedTogglesPanel<FunctionComponent>(buttonGroup, FlowLayout.LEFT);
		}
		return fColorsPanel;
	}

	public IndexedTogglesPanel<AxisComponent> getAxisesColorsPanel() {
		if(axisesColorsPanel == null) {
			axisesColorsPanel = new IndexedTogglesPanel<AxisComponent>(buttonGroup, FlowLayout.LEFT);
		}
		return axisesColorsPanel;
	}

	public IntersectionsColorPanel getIntersectionsColorPanel() {
		if(intersectionsColorPanel == null) {
			intersectionsColorPanel = new IntersectionsColorPanel(buttonGroup);
		}
		return intersectionsColorPanel;
	}

	private JPanel getColorChooserSwatchesPanel() {
		if(colorChooser == null) {
			colorChooser = new JColorChooser(Color.black);
			colorChooserSwatchesPanel = colorChooser.getChooserPanels()[0];
			colorChooser.getSelectionModel().addChangeListener(new ChangeListener() {

				public void stateChanged(ChangeEvent e) {
					ColorToggle selectedColorToggle = (ColorToggle) buttonGroup.getSelectedButton();

					if(selectedColorToggle != null) {
						selectedColorToggle.setColor(colorChooser.getColor());
						GraphPanel.getInstance().repaint();
					}
				}
			});
		}
		return colorChooserSwatchesPanel;
	}

	private final static Insets insets = new Insets(5, 5, 5, 5);
	public Insets getInsets() {
		return insets;
	}
}
