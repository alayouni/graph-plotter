package com.alayouni.handyplotter.ui.components.other;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/21/13
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class JoinPixelsToggle extends JToggleButton {
	private static final Color PIXELS_COLOR = Color.BLACK;
	private static final Color LINE_COLOR = Color.BLACK;

	private static final int MARGIN = 5;
	private static final int PIXEL_DIAMETER = 5;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(PIXELS_COLOR);
		g.fillArc(MARGIN,
				MARGIN,
				PIXEL_DIAMETER,
				PIXEL_DIAMETER,
				0,
				360);

		g.fillArc(getWidth() - MARGIN - PIXEL_DIAMETER,
				getHeight() - MARGIN - PIXEL_DIAMETER,
				PIXEL_DIAMETER,
				PIXEL_DIAMETER,
				0,
				360);

		g.setColor(LINE_COLOR);
		g.drawLine(MARGIN + PIXEL_DIAMETER / 2,
				MARGIN + PIXEL_DIAMETER / 2,
				getWidth() - MARGIN - PIXEL_DIAMETER / 2,
				getHeight()- MARGIN - PIXEL_DIAMETER / 2);
	}
}
