package com.alayouni.handyplotter.ui.main;

import com.alayouni.handyplotter.expressionutils.ExpressionBuilder;
import com.alayouni.handyplotter.ui.components.containers.ColorsPanel;
import com.alayouni.handyplotter.ui.components.containers.ControlPanel;
import com.alayouni.handyplotter.ui.components.containers.IndexedTogglesPanel;
import com.alayouni.handyplotter.ui.components.containers.IntersectionsColorPanel;
import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.VariableComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.handlers.*;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;
import com.alayouni.handyplotter.ui.utils.multisplitpane.ItemGenerator;
import com.alayouni.handyplotter.ui.utils.multisplitpane.MultiSplitPane;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/14/13
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainFrame {
	private static final int LEFT_SUB_CONTAINERS_LOCATION = 200;
	private static final int ANCESTOR_SPLIT_DIVIDER_LOCATION = 410;
	private static final String TITLE = "Handy Plotter";
	private static final int CONTROL_PANEL_HEIGHT = 75;
	private static final int COLORS_PANEL_HEIGHT = 120;

	private MultiSplitPane<VariableComponent> variablesSplit = null;
	private MultiSplitPane<FunctionComponent> functionsSplit = null;
	private MultiSplitPane<ViewPortComponent> viewPortsSplit = null;
	private MultiSplitPane<AxisComponent> axisesSplit = null;
	private ColorsPanel colorsPanel = null;
	private ControlPanel controlPanel = null;

	Handler handler;

    private void buildFrame() {
	    ExpressionBuilder.setViewPorts(getViewPortsSplit().getItems());
	    JFrame frame = new JFrame();
        frame.setTitle(TITLE);

        JSplitPane contentPane = this.createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        contentPane.setDividerLocation(ANCESTOR_SPLIT_DIVIDER_LOCATION);
        contentPane.setLeftComponent(buildLeftContainer());


        contentPane.setRightComponent(buildRightSplitPane());
        frame.setContentPane(contentPane);

	    //setExtendedState for some reason misses mouse motion events, setting the bounds manually as a work around...
	    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	    frame.setBounds(0, 0, screenSize.width, screenSize.height);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


	    initHandlers();
	    applyDefaultItemsToSplits();
	    handler.setRebuildLocked(false);
	    handler.rebuildAll(false, null, RebuildCause.STARTUP);

        frame.setVisible(true);
    }

	private MultiSplitPane<FunctionComponent> getFunctionsSplit() {
		if(functionsSplit == null) {
			functionsSplit = new MultiSplitPane<FunctionComponent>("Functions",
					new ItemGenerator<FunctionComponent>() {

						public FunctionComponent createItem(int index) {
							return new FunctionComponent(index);
						}
					},
					JSplitPane.VERTICAL_SPLIT);

		}
		return functionsSplit;
	}

	private MultiSplitPane<VariableComponent> getVariablesSplit() {
		if(variablesSplit == null) {
			variablesSplit = new MultiSplitPane<VariableComponent>("Variables",
					new ItemGenerator<VariableComponent>() {

						public VariableComponent createItem(int index) {
							return new VariableComponent(index, handler);
						}
					},
					JSplitPane.VERTICAL_SPLIT);
		}
		return variablesSplit;
	}

	private JSplitPane buildExpressionsContainer() {
		JSplitPane expressionsContainer = this.createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		expressionsContainer.setLeftComponent(getVariablesSplit());
		expressionsContainer.setRightComponent(getFunctionsSplit());
		expressionsContainer.setDividerLocation(LEFT_SUB_CONTAINERS_LOCATION);

		return expressionsContainer;
	}

	private JSplitPane buildAxisesContainer() {
		JSplitPane axisesContainer = this.createSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		axisesContainer.setLeftComponent(getViewPortsSplit());
		axisesContainer.setRightComponent(getAxisesSplit());
		axisesContainer.setDividerLocation(LEFT_SUB_CONTAINERS_LOCATION);

		return axisesContainer;
	}

	private JSplitPane buildLeftContainer() {
		JSplitPane leftContainer = this.createSplitPane(JSplitPane.VERTICAL_SPLIT);
		leftContainer.setTopComponent(buildExpressionsContainer());
		leftContainer.setBottomComponent(buildAxisesContainer());
		leftContainer.setDividerLocation(getScreenSize().height / 2);
		return leftContainer;
	}

	public MultiSplitPane<AxisComponent> getAxisesSplit() {
		if(axisesSplit == null) {
			axisesSplit = new MultiSplitPane<AxisComponent>("Axis",
					new ItemGenerator<AxisComponent>() {

						public AxisComponent createItem(int index) {
							return new AxisComponent(index);
						}
					},
					JSplitPane.VERTICAL_SPLIT);
		}
		return axisesSplit;
	}

	public MultiSplitPane<ViewPortComponent> getViewPortsSplit() {
		if(viewPortsSplit == null) {
			viewPortsSplit = new MultiSplitPane<ViewPortComponent>("View Ports",
					new ItemGenerator<ViewPortComponent>() {

						public ViewPortComponent createItem(int index) {
							List<AxisComponent> axises = axisesSplit.getItems();
							return new ViewPortComponent(index, axises);
						}
					},
					JSplitPane.VERTICAL_SPLIT);
		}
		return viewPortsSplit;
	}

	private JSplitPane buildRightSplitPane() {
		JSplitPane rightRootContainer = this.createSplitPane(JSplitPane.VERTICAL_SPLIT);

		JSplitPane topSplit = this.buildTopSplitPane();
		rightRootContainer.setTopComponent(topSplit);

		colorsPanel = new ColorsPanel(functionsSplit);
		rightRootContainer.setBottomComponent(new JScrollPane(colorsPanel));

		rightRootContainer.setDividerLocation(computeRightRootSplitPaneDividerLocation());

		return rightRootContainer;
	}

	private JSplitPane buildTopSplitPane() {
		JSplitPane topSplit = this.createSplitPane(JSplitPane.VERTICAL_SPLIT);

		//ensure handler is not null
		handler = getHandler();
		controlPanel = new ControlPanel(handler);
		topSplit.setTopComponent(new JScrollPane(controlPanel));

		GraphPanel graphPanel = GraphPanel.getInstance();
		topSplit.setBottomComponent(graphPanel);

		topSplit.setDividerLocation(CONTROL_PANEL_HEIGHT);

		return topSplit;
	}



	private int computeRightRootSplitPaneDividerLocation() {
		Dimension windowSizeApprox = getScreenSize();
		windowSizeApprox.height -= 110;
		return windowSizeApprox.height - CONTROL_PANEL_HEIGHT - COLORS_PANEL_HEIGHT;
	}

	private static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
	private JSplitPane createSplitPane(int orientation) {
		JSplitPane split = new JSplitPane(orientation) {
			@Override
			public Insets getInsets() {
				return ZERO_INSETS;
			}
		};
		split.setOneTouchExpandable(true);
		split.setContinuousLayout(true);
		return split;
	}



	public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				new MainFrame().buildFrame();
			}
		});
    }

	private Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}


	private Handler getHandler() {
		if(handler == null) {
			//Variable handler is the most independent handler => use it as the main handler (still could use any of others)
			variablesSplit = getVariablesSplit();
			handler = new VariablesHandler(variablesSplit);
			GraphPanel.setHandler(handler);
			ErrorDialog.setHandler(handler);
		}
		return handler;
	}

	private void initHandlers() {
		//BoundariesHandler has already been initialized in BoundariesPanel
		//VariablesHandler has already been initialized here
		initFunctionsHandler();
		initAxisesHandler();
		initViewPortsHandler();

		final IntersectionsColorPanel intersectionsColorPanel =  colorsPanel.getIntersectionsColorPanel();
		handler.setIntersectionsColorPanel(intersectionsColorPanel);
	}

	private void initFunctionsHandler() {
		IndexedTogglesPanel<FunctionComponent> fColorPanel = colorsPanel.getfColorsPanel(),
		fIntersectionsPanel = controlPanel.getfIntersectionPanel(),
		fIntersectingPanel = controlPanel.getfIntersectingPanel();

		new FunctionsHandler(fColorPanel, fIntersectingPanel, functionsSplit);

		handler.setfIntersectionsPanel1(fIntersectionsPanel);
	}

	private void initAxisesHandler() {
		IndexedTogglesPanel<AxisComponent> axisColorPanel = colorsPanel.getAxisesColorsPanel();
		new AxisesHandler(axisColorPanel, axisesSplit);
	}

	private void initViewPortsHandler() {
		new ViewPortsHandler(viewPortsSplit);
	}

	private void applyDefaultItemsToSplits() {
		axisesSplit.addItem();
		axisesSplit.addItem();
		viewPortsSplit.addItem();
	}

}
