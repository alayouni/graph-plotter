package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.expressionutils.ExpressionBuilder;
import com.alayouni.handyplotter.expressionutils.MathUtils;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/23/13
 * Time: 4:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortMouseScrollHandler extends MouseAdapter {
	private static final int MINIMUM_DRAG_DIFF = 5;
	protected static final Image HAND_GRAB_CURSOR_IMAGE;
	protected static final Cursor HAND_GRAB_CURSOR;
	static {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		HAND_GRAB_CURSOR_IMAGE = toolkit.getImage(ViewPortMouseScrollHandler.class.getResource("/icons/hand_grab.png"));
		HAND_GRAB_CURSOR = toolkit.createCustomCursor(HAND_GRAB_CURSOR_IMAGE, new Point(9, 9), "hand_grab");
	}

	protected static final Image HAND_GRABBING_CURSOR_IMAGE;
	protected static final Cursor HAND_GRABBING_CURSOR;
	static {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		HAND_GRABBING_CURSOR_IMAGE = toolkit.getImage(ViewPortMouseScrollHandler.class.getResource("/icons/hand_grabbing.png"));
		HAND_GRABBING_CURSOR = toolkit.createCustomCursor(HAND_GRABBING_CURSOR_IMAGE, new Point(9, 9), "hand_grabbing");
	}
	final private ViewPortPanel viewPortPanel;
	final private ViewPortComponent viewPortComponent;
	private boolean activated = false;
	private boolean dragged = false;
	private Point pressLocation;

	private final ExpressionBuilder expressionBuilder;


	public ViewPortMouseScrollHandler(final ViewPortPanel viewPortPanel, final ViewPortComponent viewPortComponent) {
		this.viewPortPanel = viewPortPanel;
		this.viewPortComponent = viewPortComponent;
		expressionBuilder = ExpressionBuilder.getInstance();
	}

	protected void setCursor() {
		if(activated) {
			viewPortPanel.setCursor(HAND_GRABBING_CURSOR);
		} else {
			viewPortPanel.setCursor(HAND_GRAB_CURSOR);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pressLocation = e.getPoint();
		activated = viewPortPanel.isScrollHandlerActivated() && !viewPortPanel.isResizeMouseEvent(pressLocation);
		if(activated) {
			viewPortPanel.setCursor(HAND_GRABBING_CURSOR);
			pressLocation = e.getPoint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		try {
			if(activated) {
				activated = false;
				viewPortPanel.setCursor(HAND_GRAB_CURSOR);
				if(dragged) {
					dragged = false;
					processScrollEvent(e);
				}
			}
		} catch (Exception e1) {
			GraphPanel.getInstance().setBroken(true);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		try {
			if(activated) {
				if(!dragged) {
					initScrollIfMatchesMinimumDragDiff(e);
				}
				if(dragged) {
					processScrollEvent(e);
				}
			}
		} catch (Exception e1) {
			dragged = false;
			activated = false;
//			GraphPanel.getInstance().setBroken(true);
			ErrorDialog.showErrorDialogIfNotAlreadyBroken(e1);
		}
	}

	private void processScrollEvent(MouseEvent e) throws Exception {
		Point pt = e.getPoint();
		double xDiff = (pressLocation.x - pt.x) * orientedPixelWidth,
			yDiff = (pressLocation.y - pt.y) * orientedPixelHeight;
		viewPortComponent.setXMinValue(pressMinX + xDiff);
		viewPortComponent.setXMaxValue(pressMaxX + xDiff);
		viewPortComponent.setYMinValue(pressMinY + yDiff);
		viewPortComponent.setYMaxValue(pressMaxY + yDiff);
		viewPortComponent.updateBoundaryDependents();
		viewPortPanel.repaint();
	}



	private double pressMinX;
	private double pressMaxX;
	private double pressMinY;
	private double pressMaxY;
	private double orientedPixelWidth;
	private double orientedPixelHeight;
	private void initScrollIfMatchesMinimumDragDiff(MouseEvent e) throws Exception {
		if(matchesMinimumDragDiff(e)) {
			dragged = true;
			pressMinX = viewPortComponent.getXMinValue();
			pressMaxX = viewPortComponent.getXMaxValue();
			pressMinY = viewPortComponent.getYMinValue();
			pressMaxY = viewPortComponent.getYMaxValue();

			double xUnit = viewPortComponent.getXUnit(),
					yUnit = viewPortComponent.getYUnit();
			orientedPixelWidth = MathUtils.sign(xUnit) * MathUtils.PIXEL_WIDTH;
			orientedPixelHeight = -MathUtils.sign(yUnit) * MathUtils.PIXEL_HEIGHT;
			if(switchToConstantStatesIfNecessary()) {
				expressionBuilder.reprocessAllVariableDependencies(true);
				expressionBuilder.buildDependentsForAllVariables();
			}
		}
	}

	private boolean switchToConstantStatesIfNecessary() {
		boolean reprocess = false;
		if(!viewPortComponent.isXMaxConstant()) {
			viewPortComponent.switchXMaxToConstantState();
			reprocess = true;
		}
		if(!viewPortComponent.isXMinConstant()) {
			viewPortComponent.switchXMinToConstantState();
			reprocess = true;
		}
		if(!viewPortComponent.isYMaxConstant()) {
			viewPortComponent.switchYMaxToConstantState();
			reprocess = true;
		}
		if(!viewPortComponent.isYMinConstant()) {
			viewPortComponent.switchYMinToConstantState();
			reprocess = true;
		}
		return reprocess;
	}

	private boolean matchesMinimumDragDiff(MouseEvent e) {
		Point pt = e.getPoint();
		int diffX = Math.abs(pt.x  - pressLocation.x);
		if(diffX >= MINIMUM_DRAG_DIFF) {
			return true;
		}
		return Math.abs((pt.y - pressLocation.y)) >= MINIMUM_DRAG_DIFF;
	}
}
