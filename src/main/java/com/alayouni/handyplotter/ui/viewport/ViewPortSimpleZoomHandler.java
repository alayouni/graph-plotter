package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.handlers.Handler;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/25/13
 * Time: 9:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortSimpleZoomHandler {
	private final ViewPortComponent viewPortComponent;
	private final Handler handler;

	public ViewPortSimpleZoomHandler(final ViewPortComponent viewPortComponent,
	                                 final Handler handler) {
		this.viewPortComponent = viewPortComponent;
		this.handler = handler;
	}

	public void zoomIn() {
		try {
			double zoomX = viewPortComponent.processXZoom(),
					zoomY = viewPortComponent.processYZoom();
			this.zoom(zoomX, zoomY);
		} catch (Exception e) {
			ErrorDialog.showSafeFailErrorDialog(e);
		}
	}

	public void zoomOut() {
		try {
			double zoomX = 1 / viewPortComponent.processXZoom(),
					zoomY = 1 / viewPortComponent.processYZoom();
			this.zoom(zoomX, zoomY);
		} catch (Exception e) {
			ErrorDialog.showSafeFailErrorDialog(e);
		}
	}

	private void zoom(double zoomX, double zoomY) {
		AxisComponent xAxis = viewPortComponent.getHAxis(),
				yAxis = viewPortComponent.getVAxis();

		double xMin = xAxis.getMinValue(),
				xMax = xAxis.getMaxValue(),
				xRadius = (xMax - xMin) / 2,
				yMin = yAxis.getMinValue(),
				yMax = yAxis.getMaxValue(),
				yRadius= (yMax - yMin) / 2,
				xCenter = xMin + xRadius,
				yCenter = yMin + yRadius,
				newMinX = xCenter - zoomX * xRadius,
				newMaxX = xCenter + zoomX * xRadius,
				newMinY = yCenter - zoomY * yRadius,
				newMaxY = yCenter + zoomY * yRadius;
		xAxis.setMinValue(newMinX);
		xAxis.setMaxValue(newMaxX);
		yAxis.setMinValue(newMinY);
		yAxis.setMaxValue(newMaxY);
		handler.rebuildAll(false, null, RebuildCause.BOUNDARIES_CHANGE);
	}
}
