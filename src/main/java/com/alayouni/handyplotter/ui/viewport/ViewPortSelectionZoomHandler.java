package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.ui.components.other.RebuildCause;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.handlers.Handler;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/23/13
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortSelectionZoomHandler extends MouseAdapter implements KeyListener {
	protected static final Image ZOOM_CURSOR_IMAGE;
	protected static final Cursor ZOOM_CURSOR;
	static {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		ZOOM_CURSOR_IMAGE = toolkit.getImage(ViewPortMouseScrollHandler.class.getResource("/icons/select-zoom.png"));
		ZOOM_CURSOR = Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
	}

	private static final int MIN_REC_SIZE = 20;

	private final ViewPortPanel viewPortPanel;
	private final ViewPortComponent viewPortComponent;
	private boolean activated = false;
	private Point pressLocation;
	private final Rectangle selectRectangle;
	private final Handler handler;
	private final ViewPortSimpleZoomHandler simpleZoomHandler;

	public ViewPortSelectionZoomHandler(final ViewPortPanel viewPortPanel,
	                                    final Rectangle selectRectangle,
	                                    final Handler handler,
	                                    final ViewPortSimpleZoomHandler simpleZoomHandler) {
		this.viewPortPanel = viewPortPanel;
		this.viewPortComponent = viewPortPanel.getViewPortComponent();
		this.selectRectangle = selectRectangle;
		this.handler = handler;
		this.simpleZoomHandler = simpleZoomHandler;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1) {
			pressLocation = e.getPoint();
			activated = viewPortPanel.isZoomHandlerActivated() && !viewPortPanel.isResizeMouseEvent(pressLocation);
		} else if (activated) {
			this.cancelSelection();
			viewPortPanel.repaint();
			activated = false;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton() != MouseEvent.BUTTON1) {
			if(activated) {
				this.cancelSelection();
				viewPortPanel.repaint();
			} else if(viewPortPanel.isZoomHandlerActivated() && !viewPortPanel.isResizeMouseEvent(pressLocation)){
				simpleZoomHandler.zoomOut();
			}
		} else if(activated) {
			this.updateSelectRectangle(e);
			this.updateBoundaries();
		}
		activated = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(activated) {
			this.updateSelectRectangle(e);
			viewPortPanel.repaint();
		}
	}

	private void updateSelectRectangle(MouseEvent e) {
		Point pt = e.getPoint();
		selectRectangle.x = Math.min(pt.x, pressLocation.x);
		selectRectangle.y = Math.min(pt.y, pressLocation.y);
		selectRectangle.width = Math.abs(pt.x - pressLocation.x);
		selectRectangle.height = Math.abs(pt.y - pressLocation.y);
	}

	protected void updateBoundaries() {
		if(selectRectangle.width >= MIN_REC_SIZE && selectRectangle.height >= MIN_REC_SIZE) {
			this.computeAndSetBoundaries();
			this.cancelSelection();
			handler.rebuildAll(false, null, RebuildCause.BOUNDARIES_CHANGE);
		} else {
			this.cancelSelection();
			viewPortPanel.repaint();
		}
	}

	private void computeAndSetBoundaries() {
		AxisComponent xAxis = viewPortComponent.getHAxis(),
		yAxis = viewPortComponent.getVAxis();
		selectRectangle.y = viewPortPanel.getHeight() - selectRectangle.y - selectRectangle.height;
		double minX = xAxis.getMinValue(),
				maxX = xAxis.getMaxValue(),
				minY = yAxis.getMinValue(),
				maxY = yAxis.getMaxValue(),
				viewPortWidth = viewPortPanel.getWidth(),
				viewPortHeight = viewPortPanel.getHeight(),
				ratioX = (maxX - minX) / viewPortWidth,
				ratioY = (maxY - minY) / viewPortHeight,
				newMinX = selectRectangle.x * ratioX + minX,
				newMaxX = (selectRectangle.x + selectRectangle.width) * ratioX + minX,
				newMinY = selectRectangle.y * ratioY + minY,
				newMaxY = (selectRectangle.y + selectRectangle.height) * ratioY + minY;
		xAxis.setMinValue(newMinX);
		xAxis.setMaxValue(newMaxX);
		yAxis.setMinValue(newMinY);
		yAxis.setMaxValue(newMaxY);
	}

	protected void cancelSelection() {
		selectRectangle.width = -1;
		selectRectangle.height = -1;
	}


	public void keyTyped(KeyEvent e) {}


	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			this.cancelSelection();
			viewPortPanel.repaint();
			activated = false;
		}
	}


	public void keyReleased(KeyEvent e) {}
}
