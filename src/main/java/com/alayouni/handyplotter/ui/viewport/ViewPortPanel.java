package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.ui.components.containers.IndexedTogglesPanel;
import com.alayouni.handyplotter.ui.components.other.ColorableToggle;
import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.drawers.FunctionsDrawer;
import com.alayouni.handyplotter.ui.drawers.HorizontalAxisDrawer;
import com.alayouni.handyplotter.ui.drawers.VerticalAxisDrawer;
import com.alayouni.handyplotter.ui.graph.GraphPanel;
import com.alayouni.handyplotter.ui.handlers.ViewPortsHandler;
import com.alayouni.handyplotter.ui.utils.ErrorDialog;
import com.alayouni.handyplotter.ui.utils.XButtonGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/3/13
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortPanel extends JPanel {
	private static final ImageIcon ZOOM_IN_ICON = new ImageIcon(ViewPortPanel.class.getResource("/icons/zoomIn.png"));
	private static final ImageIcon ZOOM_OUT_ICON = new ImageIcon(ViewPortPanel.class.getResource("/icons/zoomOut.png"));

	static final int HIGHLIGHT_THICKNESS = 7;
	private static final Dimension TOOL_TOGGLE_SIZE = new Dimension(20, 20);

	private static final Color DEFAULT_BORDER_COLOR = Color.BLACK;

	private static final Color BEING_EDITED_COLOR = new Color(231,111,0);
	private static final Color FOCUSED_COLOR = DEFAULT_BORDER_COLOR;

	private static final Color SELECT_RECTANGLE_COLOR = Color.BLACK;

	private static final Stroke THICK_STROKE = new BasicStroke(HIGHLIGHT_THICKNESS);

	private final ViewPortComponent viewPortComponent;
	private final GraphPanel graphPanel;

	private IndexedTogglesPanel<FunctionComponent> functionsPanel = null;
	private JPanel toolsContainer = null;
	private JPanel toolsPanel = null;

	private JToggleButton moveToggle = null;
	private JToggleButton scrollToggle = null;
	private JToggleButton zoomToggle = null;
	XButtonGroup buttonGroup;

	private JButton zoomIn = null;
	private JButton zoomOut = null;

	private Rectangle selectRectangle = new Rectangle(0, 0, 0, 0);
	private final ViewPortSimpleZoomHandler simpleZoomHandler;


	/**
	 * static to use common references between all view port panel instances => not really a concern but
	 * better for memory
	 */
	private static HorizontalAxisDrawer horizontalAxisDrawer = new HorizontalAxisDrawer();
	private static VerticalAxisDrawer verticalAxisDrawer = new VerticalAxisDrawer();
	private static FunctionsDrawer functionsDrawer = new FunctionsDrawer();

	private boolean isBeingEdited = false;

	public ViewPortPanel(ViewPortComponent viewPortComponent, ViewPortsHandler handler) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));
		functionsDrawer.setAllFunctions(handler.getAllFunctions());
		this.viewPortComponent = viewPortComponent;
		this.graphPanel = GraphPanel.getInstance();
		simpleZoomHandler = new ViewPortSimpleZoomHandler(viewPortComponent, handler);
		setFocusable(true);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				requestFocus();
			}
		});

		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				repaint();
				graphPanel.moveToFront(ViewPortPanel.this);
			}

			@Override
			public void focusLost(FocusEvent e) {
				repaint();
			}
		});

		this.addMouseAdapter(new ViewPortMoveHandler(this));
		ViewPortMouseScrollHandler scrollHandler = new ViewPortMouseScrollHandler(this, viewPortComponent);
		this.addMouseAdapter(scrollHandler);
		this.addMouseAdapter(new ViewPortResizeHandler(this, scrollHandler));

		ViewPortSelectionZoomHandler viewPortSelectionZoomHandler = new ViewPortSelectionZoomHandler(this,
				selectRectangle,
				handler,
				simpleZoomHandler);
		this.addMouseAdapter(viewPortSelectionZoomHandler);
		this.addKeyListener(viewPortSelectionZoomHandler);

		this.updateBounds();
		layoutComponents();
	}

	@Override
	public boolean isOpaque() {
		return false;
	}


	public void updateBounds() {
		double containerWidth = graphPanel.getWidth(),
		containerHeight = graphPanel.getHeight(),
		top = viewPortComponent.getTop(),
		left = viewPortComponent.getLeft(),
		widthRatio = viewPortComponent.getWidthRatio(),
		heightRatio = viewPortComponent.getHeightRatio();

		int x = (int)(containerWidth * left),
		y = (int)(containerHeight * top),
		width = (int)(containerWidth * widthRatio),
		height = (int)(containerHeight * heightRatio);
		setBounds(x, y, width, height);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(!graphPanel.isBroken() && !graphPanel.isLockRepaint()) {
			try {
				//calling reInit from any drawer
				functionsDrawer.reInit(g, viewPortComponent, this);
				//both axises should be graduated before getting drawn, i.e. because graduateAxis() may draw
				//vertical or horizontal grid on top of the other axis which results in hiding it's color
				AxisComponent hAxis = viewPortComponent.getHAxis();
				AxisComponent vAxis = viewPortComponent.getVAxis();
				double hAxisLocation = horizontalAxisDrawer.computeAxisLocation(hAxis, vAxis),
						vAxisLocation = verticalAxisDrawer.computeAxisLocation(vAxis, hAxis);

				boolean showVGrid = viewPortComponent.isShowVerticalGrid(),
				hideHAxis = viewPortComponent.isHideHAxis();
				int yPixel = 0;
				if(showVGrid || !hideHAxis) {
					yPixel = horizontalAxisDrawer.graduateAxis(g, hAxis, hAxisLocation, vAxisLocation, hideHAxis, showVGrid);
				}


				boolean showHGrid = viewPortComponent.isShowHorizontalGrid(),
				hideVAxis = viewPortComponent.isHideVAxis();
				int xPixel = 0;
				if(showHGrid || !hideVAxis) {
					xPixel = verticalAxisDrawer.graduateAxis(g, vAxis, vAxisLocation, hAxisLocation, hideVAxis, showHGrid);
				}

				if(!hideHAxis) {
					horizontalAxisDrawer.drawAxis(g, hAxis, yPixel);
				}

				if(!hideVAxis) {
					verticalAxisDrawer.drawAxis(g, vAxis, xPixel);
				}

				functionsDrawer.drawFunctions(g);
				if(viewPortComponent.isDrawBorder()) {
					g.setColor(DEFAULT_BORDER_COLOR);
					g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
				}

				if(this.isFocusOwner() || isBeingEdited) {
					g.setColor(isBeingEdited ? BEING_EDITED_COLOR : FOCUSED_COLOR);
					this.highlightBorder(g);
				}

				if(selectRectangle.width > 0 && selectRectangle.height > 0) {
					g.setColor(SELECT_RECTANGLE_COLOR);
					g.drawRect(selectRectangle.x, selectRectangle.y, selectRectangle.width, selectRectangle.height);
				}

			} catch (Exception e) {
				ErrorDialog.showErrorDialogIfNotAlreadyBroken(e);
			}
		}
	}

	private void highlightBorder(Graphics g) {
		Graphics2D g2D = (Graphics2D)g;
		Stroke oldStroke = g2D.getStroke();
		g2D.setStroke(THICK_STROKE);
		g2D.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
		g2D.setStroke(oldStroke);
	}

	public ViewPortComponent getViewPortComponent() {
		return viewPortComponent;
	}

	public void setBeingEdited(boolean beingEdited) {
		isBeingEdited = beingEdited;
		repaint();
	}

	public boolean isBeingEdited() {
		return isBeingEdited;
	}

	private void layoutComponents() {
		this.add(getToolsContainer());
	}

	private JPanel getToolsContainer() {
		if(toolsContainer == null) {
			toolsContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 15, 0));
			toolsContainer.add(getToolsPanel());
			toolsContainer.add(getFunctionsPanel());
			toolsContainer.setOpaque(false);
		}
		return toolsContainer;
	}

	private JPanel getToolsPanel() {
		if(toolsPanel == null) {
			toolsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 2, 5));
			toolsPanel.setOpaque(false);
			toolsPanel.add(getScrollToggle());
			toolsPanel.add(getMoveToggle());
			toolsPanel.add(getZoomToggle());

			buttonGroup = new XButtonGroup();
			buttonGroup.add(getMoveToggle());
			buttonGroup.add(getZoomToggle());
			buttonGroup.add(getScrollToggle());

			toolsPanel.add(getZoomIn());
			toolsPanel.add(getZoomOut());
		}
		return toolsPanel;
	}

	private JToggleButton getMoveToggle() {
		if(moveToggle == null) {
			ImageIcon icon = new ImageIcon(ViewPortMoveHandler.MOVE_CURSOR_IMAGE);
			moveToggle = new JToggleButton(icon);
			moveToggle.setPreferredSize(TOOL_TOGGLE_SIZE);
			moveToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(moveToggle.isSelected()) {
						setCursor(ViewPortMoveHandler.MOVE_CURSOR);
					}
				}
			});
			moveToggle.setCursor(Cursor.getDefaultCursor());
			moveToggle.setFocusable(false);
		}
		return moveToggle;
	}

	private JToggleButton getScrollToggle() {
		if(scrollToggle == null) {
			ImageIcon icon = new ImageIcon(ViewPortMouseScrollHandler.HAND_GRAB_CURSOR_IMAGE);
			scrollToggle = new JToggleButton(icon);
			scrollToggle.setSelected(true);
			scrollToggle.setPreferredSize(TOOL_TOGGLE_SIZE);
			scrollToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(scrollToggle.isSelected()) {
						setCursor(ViewPortMouseScrollHandler.HAND_GRAB_CURSOR);
					}
				}
			});
			scrollToggle.setCursor(Cursor.getDefaultCursor());
			scrollToggle.setFocusable(false);
			this.setCursor(ViewPortMouseScrollHandler.HAND_GRAB_CURSOR);
		}
		return scrollToggle;
	}

	private JToggleButton getZoomToggle() {
		if(zoomToggle == null) {
			ImageIcon icon = new ImageIcon(ViewPortSelectionZoomHandler.ZOOM_CURSOR_IMAGE);
			zoomToggle = new JToggleButton(icon);
			zoomToggle.setPreferredSize(TOOL_TOGGLE_SIZE);
			zoomToggle.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if(zoomToggle.isSelected()) {
						setCursor(ViewPortSelectionZoomHandler.ZOOM_CURSOR);
					}
				}
			});
			zoomToggle.setCursor(Cursor.getDefaultCursor());
			zoomToggle.setFocusable(false);
		}
		return zoomToggle;
	}

	private JButton getZoomIn() {
		if(zoomIn == null) {
			zoomIn = new JButton(ZOOM_IN_ICON);
			zoomIn.setPreferredSize(TOOL_TOGGLE_SIZE);
			zoomIn.setFocusable(false);
			zoomIn.setCursor(Cursor.getDefaultCursor());
			zoomIn.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					simpleZoomHandler.zoomIn();
				}
			});
		}
		return zoomIn;
	}

	private JButton getZoomOut() {
		if(zoomOut == null) {
			zoomOut = new JButton(ZOOM_OUT_ICON);
			zoomOut.setPreferredSize(TOOL_TOGGLE_SIZE);
			zoomOut.setFocusable(false);
			zoomOut.setCursor(Cursor.getDefaultCursor());
			zoomOut.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					simpleZoomHandler.zoomOut();
				}
			});
		}
		return zoomOut;
	}

	private IndexedTogglesPanel<FunctionComponent> getFunctionsPanel() {
		if(functionsPanel == null) {
			functionsPanel = new IndexedTogglesPanel<FunctionComponent >(null, FlowLayout.LEFT);
			functionsPanel.setOpaque(false);
		}
		return functionsPanel;
	}

	protected boolean isMoveHandlerActivated() {
		return moveToggle.isSelected();
	}

	protected boolean isScrollHandlerActivated() {
		return scrollToggle.isSelected();
	}

	protected boolean isZoomHandlerActivated() {
		return zoomToggle.isSelected();
	}

	final Rectangle relocateBounds = new Rectangle(HIGHLIGHT_THICKNESS + 1, HIGHLIGHT_THICKNESS + 1, 0, 0);
	protected boolean isResizeMouseEvent(Point mouseLocation) {
		relocateBounds.width = getWidth() - 2 * HIGHLIGHT_THICKNESS - 2;
		relocateBounds.height = getHeight() - 2 * HIGHLIGHT_THICKNESS - 2;
		return !relocateBounds.contains(mouseLocation);
	}

	public void setToolsVisible(boolean visible) {
		toolsContainer.setVisible(visible);
	}

	public ColorableToggle<FunctionComponent> addFunctionToggle(final FunctionComponent fComp) {
		final ColorableToggle<FunctionComponent> fMappingToggle = new ColorableToggle<FunctionComponent>(fComp);
		fMappingToggle.setCursor(Cursor.getDefaultCursor());
		functionsPanel.addToggle(fMappingToggle, fComp.getIndex());
		fMappingToggle.setSelected(true);
		return fMappingToggle;
	}

	public void removeFunctionToggle(FunctionComponent fComp) {
		functionsPanel.removeToggle(fComp);
	}

	public ColorableToggle<FunctionComponent> getFunctionColorableToggle(FunctionComponent fComp) {
		return (ColorableToggle<FunctionComponent>) functionsPanel.findToggleForComponent(fComp);
	}


	/**
	 * adds mouseAdapter as a mouse listener and as mouse motion listener
	 * @param mouseAdapter
	 */
	private void addMouseAdapter(MouseAdapter mouseAdapter) {
		super.addMouseListener(mouseAdapter);
		super.addMouseMotionListener(mouseAdapter);
	}

}
