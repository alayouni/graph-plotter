package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.ui.graph.GraphPanel;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/23/13
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortResizeHandler extends MouseAdapter {
	private static final Cursor W_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
	private static final Cursor E_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
	private static final Cursor N_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
	private static final Cursor S_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
	private static final Cursor NW_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
	private static final Cursor SW_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
	private static final Cursor SE_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
	private static final Cursor NE_RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);

	private static final int MINIMUM_LENGTH = ViewPortPanel.HIGHLIGHT_THICKNESS * 2 + 2;
	Dimension pressSize;

	private final Resizer nwResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.x += diffX;
			bounds.width -= diffX;
			bounds.y += diffY;
			bounds.height -= diffY;
			return bounds;
		}
	};

	private final Resizer wResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.x += diffX;
			bounds.width -= diffX;
			return bounds;
		}
	};

	private final Resizer swResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.x += diffX;
			bounds.width -= diffX;
			bounds.height = pressSize.height + diffY;
			return bounds;
		}
	};

	private final Resizer sResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.height = pressSize.height + diffY;
			return bounds;
		}
	};

	private final Resizer seResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.width = pressSize.width + diffX;
			bounds.height = pressSize.height + diffY;
			return bounds;
		}
	};

	private final Resizer eResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.width = pressSize.width + diffX;
			return bounds;
		}
	};

	private final Resizer neResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.width = pressSize.width + diffX;
			bounds.y += diffY;
			bounds.height -= diffY;
			return bounds;
		}
	};

	private final Resizer nResizer = new Resizer() {
		@Override
		public Rectangle computeBounds(int diffX, int diffY) {
			Rectangle bounds = viewPortPanel.getBounds();
			bounds.y += diffY;
			bounds.height -= diffY;
			return bounds;
		}
	};

	private Resizer resizer;
	private final ViewPortPanel viewPortPanel;
	private Point pressLocation;
	private final GraphPanel graphPanel;
	private int graphPanelWidth;
	private int graphPanelHeight;
	private boolean activated = false;
	private final ViewPortMouseScrollHandler scrollHandler;

	public ViewPortResizeHandler(ViewPortPanel viewPortPanel, ViewPortMouseScrollHandler scrollHandler) {
		this.viewPortPanel = viewPortPanel;
		this.graphPanel = GraphPanel.getInstance();
		this.scrollHandler = scrollHandler;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		Point pt = e.getPoint();
		updateCursor(pt);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pressLocation = e.getPoint();
		activated = viewPortPanel.isResizeMouseEvent(pressLocation);
		if(activated) {
			pressSize = viewPortPanel.getSize();
			reloadContainerSize();
			updateCursor(pressLocation);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(activated) {
			Point pt = e.getPoint();
			resizer.resize(pt);
		}
	}



	@Override
	public void mouseReleased(MouseEvent e) {
		if(activated) {
			Point pt = e.getPoint();
			resizer.resize(pt);

		}
	}

	private void updateCursor(Point pt) {
		if(viewPortPanel.isResizeMouseEvent(pt)) {
			viewPortPanel.setCursor(inferResizeCursor(pt));
		} else if(viewPortPanel.isMoveHandlerActivated()){
			viewPortPanel.setCursor(ViewPortMoveHandler.MOVE_CURSOR);
		} else if(viewPortPanel.isScrollHandlerActivated()) {
			scrollHandler.setCursor();
		} else if(viewPortPanel.isZoomHandlerActivated()) {
			viewPortPanel.setCursor(ViewPortSelectionZoomHandler.ZOOM_CURSOR);
		}
	}

	private Cursor inferResizeCursor(Point pt) {
		if(pt.x <= ViewPortPanel.HIGHLIGHT_THICKNESS + 1) {
			if(pt.y <= ViewPortPanel.HIGHLIGHT_THICKNESS + 1) {
				resizer = nwResizer;
				return NW_RESIZE_CURSOR;
			}
			int southPosition = viewPortPanel.getHeight() - ViewPortPanel.HIGHLIGHT_THICKNESS - 1;
			if(pt.y < southPosition) {
				resizer = wResizer;
				return W_RESIZE_CURSOR;
			} else {
				resizer = swResizer;
				return SW_RESIZE_CURSOR;
			}
		}
		int eastPosition = viewPortPanel.getWidth() - ViewPortPanel.HIGHLIGHT_THICKNESS - 1;
		if(pt.x < eastPosition) {
			if(pt.y <= ViewPortPanel.HIGHLIGHT_THICKNESS + 1) {
				resizer = nResizer;
				return N_RESIZE_CURSOR;
			} else {
				resizer = sResizer;
				return S_RESIZE_CURSOR;
			}
		} else if(pt.y <= ViewPortPanel.HIGHLIGHT_THICKNESS + 1) {
			resizer = neResizer;
			return NE_RESIZE_CURSOR;
		}
		int southPosition = viewPortPanel.getHeight() - ViewPortPanel.HIGHLIGHT_THICKNESS - 1;
		if(pt.y < southPosition) {
			resizer = eResizer;
			return E_RESIZE_CURSOR;
		}
		resizer = seResizer;
		return SE_RESIZE_CURSOR;
	}

	private void reloadContainerSize() {
		graphPanelWidth = graphPanel.getWidth();
		graphPanelHeight = graphPanel.getHeight();
	}

	/**
	 * For performance, to avoid 8 if statements on each drag event, we're gonna use following Resizer abstract class.
	 */
	private abstract class Resizer {
		public void resize(Point pt) {
			int diffX = pt.x - pressLocation.x,
					diffY = pt.y - pressLocation.y;
			Rectangle bounds = computeBounds(diffX, diffY);
			if(bounds.width > MINIMUM_LENGTH && bounds.height > MINIMUM_LENGTH) {
				viewPortPanel.setBounds(bounds);
				updateRatioBounds(bounds.x, bounds.y, bounds.width, bounds.height);
			}
		}
		abstract Rectangle computeBounds(int diffX, int diffY);

		private void updateRatioBounds(double x, double y, double width, double height) {
			double left = x / graphPanelWidth,
					top = y / graphPanelHeight,
					ratioWidth = width / graphPanelWidth,
					ratioHeight = height / graphPanelHeight;
			viewPortPanel.getViewPortComponent().setRatioBounds(left, top, ratioWidth, ratioHeight);
		}
	}
}
