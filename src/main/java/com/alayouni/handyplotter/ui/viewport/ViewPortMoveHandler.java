package com.alayouni.handyplotter.ui.viewport;

import com.alayouni.handyplotter.ui.graph.GraphPanel;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/23/13
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class ViewPortMoveHandler extends MouseAdapter {
	protected static final Image MOVE_CURSOR_IMAGE;
	protected static final Cursor MOVE_CURSOR;
	static {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		MOVE_CURSOR_IMAGE = toolkit.getImage(ViewPortMouseScrollHandler.class.getResource("/icons/move.png"));
		MOVE_CURSOR = toolkit.createCustomCursor(MOVE_CURSOR_IMAGE, new Point(9, 9), "move_custom");
	}

	boolean activated = false;
	private Point pressLocation;
	private final ViewPortPanel viewPortPanel;

	private final GraphPanel graphPanel;
	private int graphPanelWidth;
	private int graphPanelHeight;

	public ViewPortMoveHandler(ViewPortPanel viewPortPanel) {
		this.viewPortPanel = viewPortPanel;
		graphPanel = GraphPanel.getInstance();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		pressLocation = e.getPoint();
		reloadContainerSize();
		activated = viewPortPanel.isMoveHandlerActivated() && !viewPortPanel.isResizeMouseEvent(pressLocation);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(activated) {
			updateLocation(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(activated) {
			updateLocation(e);
			activated = false;
		}
	}

	private void updateLocation(MouseEvent e) {
		Point currentMouseLocation = e.getPoint(),
				componentLocation = viewPortPanel.getLocation();
		int diffX = currentMouseLocation.x - pressLocation.x,
				diffY = currentMouseLocation.y - pressLocation.y;
		componentLocation.x += diffX;
		componentLocation.y += diffY;
		viewPortPanel.setLocation(componentLocation);
		updateRatioLocation(componentLocation.x, componentLocation.y);
	}

	private void updateRatioLocation(double x, double y) {
		double left = x / graphPanelWidth,
				top = y / graphPanelHeight;
		viewPortPanel.getViewPortComponent().setRatioLocation(left, top);
	}

	private void reloadContainerSize() {
		graphPanelWidth = graphPanel.getWidth();
		graphPanelHeight = graphPanel.getHeight();
	}
}
