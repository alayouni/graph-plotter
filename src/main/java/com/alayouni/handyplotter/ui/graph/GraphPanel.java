package com.alayouni.handyplotter.ui.graph;

import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.handlers.Handler;
import com.alayouni.handyplotter.ui.handlers.ViewPortsHandler;
import com.alayouni.handyplotter.ui.viewport.ViewPortPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/14/13
 * Time: 8:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class GraphPanel extends JLayeredPane {
	private static final String BROKEN_MSG = "BUILD REQUIRED!";
	private static final Integer COMPONENTS_DEPTH = 0;

	private static GraphPanel instance = null;

	private final JLabel brokenLabel;
	boolean broken = false;

	private boolean lockRepaint = false;

	private static Handler handler;

	private GraphPanel() {
		super();
		setLayout(null);
		setOpaque(true);
		brokenLabel = new JLabel(BROKEN_MSG);
		brokenLabel.setForeground(Color.RED);
		brokenLabel.setBounds(15, 15, 150, 20);
		brokenLabel.setVisible(false);
		this.add(brokenLabel, new Integer(0));
		setBackground(Color.white);
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				ViewPortPanel viewPortPanel;
				for (Component comp : getComponents()) {
					if (comp instanceof ViewPortPanel) {
						viewPortPanel = (ViewPortPanel)comp;
						viewPortPanel.updateBounds();
					}
				}
			}
		});
	}

	public static GraphPanel getInstance() {
		if(instance == null) {
			instance = new GraphPanel();
		}
		return instance;
	}

	public boolean isBroken() {
		return broken;
	}

	public void setBroken(boolean broken) {
		if(broken != this.broken) {
			this.broken = broken;
			if(broken) {
				handler.stopSpinningFieldsIfAny();
			}
			SwingUtilities.invokeLater(updateUIRunnable);
		}
	}

	private final Runnable updateUIRunnable = new Runnable() {

		public void run() {
			ViewPortComponent viewPortComponent;
			for(Component comp: getComponents()) {
				if(comp == brokenLabel) {
					comp.setVisible(broken);
				} else {
					viewPortComponent = ((ViewPortPanel)comp).getViewPortComponent();
					comp.setVisible(broken ? false : !viewPortComponent.isHidden());
				}
			}
		}
	};

	public ViewPortPanel addViewPortPanel(ViewPortComponent viewPortComponent, ViewPortsHandler handler) {
		ViewPortPanel viewPortPanel = new ViewPortPanel(viewPortComponent, handler);
		this.add(viewPortPanel, COMPONENTS_DEPTH);
		return viewPortPanel;
	}

	public void removeViewPortPanel(ViewPortComponent viewPortComponent) {
		ViewPortPanel viewPortPanel;
		for(Component component : getComponents()) {
			if(component instanceof ViewPortPanel) {
				viewPortPanel = (ViewPortPanel)component;
				if(viewPortPanel.getViewPortComponent() == viewPortComponent) {
					this.remove(viewPortPanel);
				}
			}
		}
	}

	public boolean isLockRepaint() {
		return lockRepaint;
	}

	public void setLockRepaint(boolean lockRepaint) {
		this.lockRepaint = lockRepaint;
	}

	public static void setHandler(Handler handler) {
		GraphPanel.handler = handler;
	}
}
