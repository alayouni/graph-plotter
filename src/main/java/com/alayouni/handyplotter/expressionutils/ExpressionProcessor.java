package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ExpressionProcessor {
	String processExpression(String expression);
}
