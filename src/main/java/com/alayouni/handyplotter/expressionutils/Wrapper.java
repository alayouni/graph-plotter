package com.alayouni.handyplotter.expressionutils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/18/13
 * Time: 10:33 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Wrapper {

    private Set<FunctionWrapper> dependencyFunctions = new HashSet<FunctionWrapper>();
    private Set<VariableWrapper> dependencyVariables = new HashSet<VariableWrapper>();

	protected final String prefix;
	protected final String suffix;
    protected int index;
    protected final String expression;

    protected Wrapper(final String prefix,
                      final String suffix,
                      Integer index,
                      final String expression) {
	    this.prefix = prefix;
	    this.suffix = suffix;
        this.index = index;
        this.expression = expression;
    }

    public Set<FunctionWrapper> getDependencyFunctions() {
        return dependencyFunctions;
    }

    public Set<VariableWrapper> getDependencyVariables() {
        return dependencyVariables;
    }

	public void setDependencyFunctions(Set<FunctionWrapper> dependencyFunctions) {
		this.dependencyFunctions = dependencyFunctions;
	}

	public void setDependencyVariables(Set<VariableWrapper> dependencyVariables) {
		this.dependencyVariables = dependencyVariables;
	}

	public int getIndex() {
        return index;
    }

	public void setIndex(int index) {
		this.index = index;
	}

	public String getExpression() {
		return expression;
	}

	public String getIdentifier() {
		String identifier = String.format("%s%s", prefix, index);
		if(suffix != null) {
			identifier += ("." + suffix);
		}
		return identifier;
	}
}
