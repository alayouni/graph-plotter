package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/16/13
 * Time: 8:01 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Variable{

    /**
     * Variables should not depend on x.
     * @return
     */
    Double f() throws Exception;
}
