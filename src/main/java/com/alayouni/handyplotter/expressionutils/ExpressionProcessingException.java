package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/22/13
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExpressionProcessingException extends Exception{
    public static final int RECURSIVE_EXCEPTION = 0;
    public static final int CYCLE_EXCEPTION = 1;
    public static final int UNDEFINED_ID_EXCEPTION = 2;
	public static final int UNDEFINED_AXIS_EXCEPTION = 3;
	public static final int EXPRESSION_EVAL_EXCEPTION = 4;
	public static final int UN_AUTHORIZED_CALL = 5;

    private final int exceptionType;

    public ExpressionProcessingException(final String message, final int exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }

	public ExpressionProcessingException(final String message, final int exceptionType, Exception e) {
		super(message, e);
		this.exceptionType = exceptionType;
	}

    public int getExceptionType() {
        return exceptionType;
    }
}
