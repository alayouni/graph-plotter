package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/10/13
 * Time: 3:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class MaxMinMethodParamsProcessor {
	//_______________________________________Constants___________________________________________________________________
	private static final Pattern VIEW_PORT_PREFIX_PATTERN = Pattern.compile("^" + ViewPortComponent.VIEW_PORT_PREFIX + "\\d+\\b");

	private static final Pattern MIN_METHOD_PATTERN = Pattern.compile(createOptimaMethodRegex("min"));
	private static final Pattern MAX_METHOD_PATTERN = Pattern.compile(createOptimaMethodRegex("max"));

	private static final Pattern MIN_METHOD_VIEW_PORT_PATTERN = Pattern.compile(createViewPortOptimaMethodRegex("min"));
	private static final Pattern MAX_METHOD_VIEW_PORT_PATTERN = Pattern.compile(createViewPortOptimaMethodRegex("max"));

	private static final String DOUBLE_PREFIXED_NUMBER_LIST_REGEX = createPrefixedNumberListRegex("double");
	private static final NumberParamProcessor DOUBLE_PARAM_PROCESSOR = new NumberParamProcessor() {

		public String processNumberParam(String param) {
			try {
				param = param.trim();
				Double.parseDouble(param);
			} catch(NumberFormatException e) {
				return param;
			}
			return String.format("((double)%s)", param);
		}


		public String wrapCommaSeparatedParams(String commaSeparatedParams) {
			return String.format("new double[] {%s}", commaSeparatedParams);
		}
	};


	private static final String INTEGER_PREFIXED_NUMBER_LIST_REGEX = createPrefixedNumberListRegex("int");
	private static final NumberParamProcessor INTEGER_PARAM_PROCESSOR = new NumberParamProcessor() {

		public String processNumberParam(String param) {
			try {
				param = param.trim();
				//double values should be casted
				Double.parseDouble(param);
			} catch(NumberFormatException e) {
				return param;
			}
			return String.format("((int)%s)", param);
		}


		public String wrapCommaSeparatedParams(String commaSeparatedParams) {
			return String.format("new int[] {%s}", commaSeparatedParams);
		}
	};

	private static final String LONG_PREFIXED_NUMBER_LIST_REGEX = createPrefixedNumberListRegex("long");
	private static final NumberParamProcessor LONG_PARAM_PROCESSOR = new NumberParamProcessor() {

		public String processNumberParam(String param) {
			try {
				param = param.trim();
				//double values should be casted
				Double.parseDouble(param);
			} catch(NumberFormatException e) {
				return param;
			}
			return String.format("((long)%s)", param);
		}


		public String wrapCommaSeparatedParams(String commaSeparatedParams) {
			return String.format("new long[] {%s}", commaSeparatedParams);
		}
	};

	private static final Map<String, NumberParamProcessor> paramsProcessors = new HashMap<String, NumberParamProcessor>();
	static {
		paramsProcessors.put(DOUBLE_PREFIXED_NUMBER_LIST_REGEX, DOUBLE_PARAM_PROCESSOR);
		paramsProcessors.put(INTEGER_PREFIXED_NUMBER_LIST_REGEX, INTEGER_PARAM_PROCESSOR);
		paramsProcessors.put(LONG_PREFIXED_NUMBER_LIST_REGEX, LONG_PARAM_PROCESSOR);
	}
	//____________________________________________End Constants_________________________________________________________

	public boolean containsViewPortOptimaCall(String expr) {
		return MIN_METHOD_VIEW_PORT_PATTERN.matcher(expr).find() ||
				MAX_METHOD_VIEW_PORT_PATTERN.matcher(expr).find();
	}

	public String processOptimaMethodsCalls(String expression) {
		expression = this.processOptimaMethodCalls(expression, MIN_METHOD_PATTERN);
		return this.processOptimaMethodCalls(expression, MAX_METHOD_PATTERN);
	}

	/**
	 *
	 * @param expression
	 * @param pattern one of MIN_METHOD_PATTERN or MAX_METHOD_PATTERN
	 * @return
	 */
	private String processOptimaMethodCalls(String expression, Pattern pattern) {
		Matcher matcher = pattern.matcher(expression);
		int paramsStart;
		String params, paramsReplacement, prefix, suffix;
		if(matcher.find()) {
			paramsStart = matcher.end(1);
			params = this.extractParamsBeforeWellFormedClosingParenthesis(expression, paramsStart);
			if(params != null) {
				paramsReplacement = this.replaceOptimaMethodParams(params);
				prefix = expression.substring(0, paramsStart);
				suffix = expression.substring(paramsStart + params.length());
				return prefix + this.processOptimaMethodCalls(paramsReplacement + suffix, pattern);
			}
		}
		return expression;
	}

	private String extractParamsBeforeWellFormedClosingParenthesis(String expression, int startParams) {
		String params = "";
		int openParenthesisCount = 1, index = startParams;
		char c;
		while(index < expression.length()) {
			c = expression.charAt(index);
			switch (c) {
				case '(':
					openParenthesisCount++;
					break;
				case ')':
					openParenthesisCount--;
					if(openParenthesisCount == 0) {
						return params;
					}
					break;
			}
			params += c;
			index++;
		}
		return null;
	}




	/**
	 * optima for min or max. min or max could be applied either to a list of numbers (double, int, long), or a view port and
	 * a list of Functions. if no function is specified with a view port params.trim() will be returned.
	 * @param params the text between (), parentheses excluded.
	 * @return
	 */
	private String replaceOptimaMethodParams(String params) {
		String trimmed = params.trim();
		if(VIEW_PORT_PREFIX_PATTERN.matcher(trimmed).find()) {
			return replaceOptimaFunctionAppliedToViewPort(trimmed);
		} else {//list of numbers
			return replaceOptimaFunctionAppliedToNumbers(trimmed);
		}
	}

	private String replaceOptimaFunctionAppliedToViewPort(String trimmedParams) {
		//separate view port parameter from functions (if any)
		int indexOfEndViewPortParam = trimmedParams.indexOf(",");
		if(indexOfEndViewPortParam > 0) {
			String viewPortParam = trimmedParams.substring(0, indexOfEndViewPortParam),
					functionsParams = trimmedParams.substring(indexOfEndViewPortParam + 1);
			return String.format("%s, new Function[]{%s}", viewPortParam, functionsParams);
		}
		return trimmedParams;
	}


	private String replaceOptimaFunctionAppliedToNumbers(String trimmedParams) {
		Pattern pattern;
		Matcher matcher;
		String commaSeparatedParams = trimmedParams;
		NumberParamProcessor processor;
		for(String regex : paramsProcessors.keySet()) {
			pattern = Pattern.compile(regex);
			matcher = pattern.matcher(trimmedParams);
			if(matcher.find()) {
				commaSeparatedParams = matcher.group(2).trim();
				processor = paramsProcessors.get(regex);
				return replaceTrimmedCommaSeparatedParams(commaSeparatedParams, processor);
			}
		}
		return replaceTrimmedCommaSeparatedParams(commaSeparatedParams, DOUBLE_PARAM_PROCESSOR);
	}

	private String replaceTrimmedCommaSeparatedParams(String commaSeparatedParams, NumberParamProcessor processor) {
		String replacement = this.castCommaSeparatedParamsWhereApplicable(commaSeparatedParams, processor);
		return processor.wrapCommaSeparatedParams(replacement);
	}

	private String castCommaSeparatedParamsWhereApplicable(String trimmedParams, NumberParamProcessor processor) {
		List<String> paramsPortions = this.splitIntoTopLevelParenthesisDelimitedPortions(trimmedParams);
		if(paramsPortions == null) {
			return trimmedParams;
		}
		String castedTrimmedParams = "", castedPortion;
		for(String portion : paramsPortions) {
			if(portion.startsWith("(")) {
				castedTrimmedParams += portion;
			} else {
				castedPortion = this.castCommaSeparatedNoParenthesisNumberParams(portion, processor);
				castedTrimmedParams += castedPortion;
			}
		}
		return castedTrimmedParams;
	}

	/**
	 * splits trimmedParams into a list of portions where in each couple of successive portions, at least one
	 * is well formed top level parenthesis delimited.
	 * @param trimmedParams
	 * @return
	 */
	private List<String> splitIntoTopLevelParenthesisDelimitedPortions(String trimmedParams) {
		List<String> paramsPortions = new ArrayList<String>();
		int startIndex = 0, index;
		String portion;
		String parenthesisParams;
		while(startIndex < trimmedParams.length()) {
			index = trimmedParams.indexOf("(", startIndex);
			if(index != -1) {
				portion = trimmedParams.substring(startIndex, index);
				paramsPortions.add(portion);
				parenthesisParams = this.extractParamsBeforeWellFormedClosingParenthesis(trimmedParams.substring(index + 1), 0);
				if(parenthesisParams == null) {
					return null;
				}
				parenthesisParams = String.format("(%s)", parenthesisParams);
				paramsPortions.add(parenthesisParams);
				startIndex = index + parenthesisParams.length();
			} else {
				portion = trimmedParams.substring(startIndex);
				paramsPortions.add(portion);
				return paramsPortions;
			}
		}
		return paramsPortions;
	}

	/**
	 * @param noParenthesisParams are assumed to have no parenthesis
	 * @param processor
	 * @return
	 */
	private String castCommaSeparatedNoParenthesisNumberParams(String noParenthesisParams, NumberParamProcessor processor) {
		StringTokenizer st = new StringTokenizer(noParenthesisParams, ",");
		String token, replacement = "";
		if(noParenthesisParams.startsWith(",")) {
			replacement = ",";
		}
		while(st.hasMoreTokens()) {
			token = st.nextToken();
			replacement += processor.processNumberParam(token) + ",";
		}
		if(replacement.length() > 0 && !noParenthesisParams.endsWith(",")) {//remove trailing comma
			return replacement.substring(0, replacement.length() - 1);
		}
		return replacement;
	}


	public Set<VariableWrapper> extractAxisDependencies(final String expr, Wrapper wrapper) throws ExpressionProcessingException {
		Set<VariableWrapper> axisDependencies = new HashSet<VariableWrapper>();
		String regex = "(V)(\\d+)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expr);
		int viewPortIndex, axisIndex;
		VariableWrapper minWrapper, maxWrapper;
		while(matcher.find()) {
			viewPortIndex = Integer.parseInt(matcher.group(2));
			axisIndex = this.extractHAxisIndexFromViewPort(viewPortIndex, wrapper);
			minWrapper = ExpressionBuilder.min.get(axisIndex);
			maxWrapper = ExpressionBuilder.max.get(axisIndex);
			axisDependencies.add(minWrapper);
			axisDependencies.add(maxWrapper);
		}
		return axisDependencies;
	}

	public Set<FunctionWrapper> extractImplicitFunctionsDependencies(final String expr, Wrapper wrapper) throws ExpressionProcessingException {
		Set<FunctionWrapper> functionDependencies = new HashSet<FunctionWrapper>(), mappedFunctions;
		String regex = "(\\(\\s*?V)(\\d+)(\\s*?\\))";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expr);
		int viewPortIndex;
		while(matcher.find()) {
			viewPortIndex = Integer.parseInt(matcher.group(2));
			mappedFunctions = extractImplicitMappedFunctionIndexesFromViewPort(viewPortIndex, wrapper);
			functionDependencies.addAll(mappedFunctions);
		}
		return functionDependencies;
	}


	private int extractHAxisIndexFromViewPort(int viewPortIndex, Wrapper wrapper) throws ExpressionProcessingException {
		verifyViewPortIndex(viewPortIndex, wrapper);
		ViewPortComponent viewPortComponent = ExpressionBuilder.viewPorts.get(viewPortIndex);
		if(viewPortComponent.getHAxis() != null) {
			return viewPortComponent.getHAxis().getIndex();
		} else {
			throw new ExpressionProcessingException(String.format("Undefined X Axis for %s %s referred by %s",
					ViewPortComponent.VIEW_PORT_PREFIX,
					viewPortIndex,
					wrapper.getIdentifier()),
					ExpressionProcessingException.UNDEFINED_AXIS_EXCEPTION);
		}
	}

	private Set<FunctionWrapper> extractImplicitMappedFunctionIndexesFromViewPort(int viewPortIndex, Wrapper wrapper) throws ExpressionProcessingException {
		verifyViewPortIndex(viewPortIndex, wrapper);
		ViewPortComponent viewPortComponent = ExpressionBuilder.viewPorts.get(viewPortIndex);
		Set<FunctionWrapper> mappedFunctionWrappers = new HashSet<FunctionWrapper>();
		int index;
		for(FunctionComponent fComp : viewPortComponent.getMappedFunctions()) {
			index = fComp.getIndex();
			mappedFunctionWrappers.add(ExpressionBuilder.f.get(index));
		}
		return mappedFunctionWrappers;
	}

	private void verifyViewPortIndex(int viewPortIndex, Wrapper wrapper) throws ExpressionProcessingException {
		if(viewPortIndex >= ExpressionBuilder.viewPorts.size()) {
			throw new ExpressionProcessingException(String.format("cannot find %s %s referred by %s",
					ViewPortComponent.VIEW_PORT_PREFIX,
					viewPortIndex,
					wrapper),
					ExpressionProcessingException.UNDEFINED_ID_EXCEPTION);
		}
	}





	private interface NumberParamProcessor {
		String processNumberParam(String numberParam);

		String wrapCommaSeparatedParams(String commaSeparatedParams);
	}

	private static String createPrefixedNumberListRegex(String prefix) {
		return "(" + prefix + "\\s*?\\{)((.?\n?)*?)(\\})";
	}

	private static String createOptimaMethodRegex(String methodName) {
		return "(?<!\\.)(\\b" + methodName + "\\s*?\\()";
	}

	private static String createViewPortOptimaMethodRegex(String methodName) {
		return "(?<!\\.)\\b" + methodName + "\\s*\\(\\s*V\\d+\\b";
	}
	//___________________________________________________________________________________________________
}
