package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/15/13
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class MathUtils {
	/**
	 * should be updated before each repaint (values change with the view port size)
	 * won't check for recursivity user should be careful. example if PIXEL_WIDTH depends on minX or maxX, and
	 * if one of those depend directly or indirectly (minX and maxX are variables ...) on PIXEL_WIDTH that's an infinite
	 * loop => assigning default constants and avoiding expressions for pixel size to avoid infinite loops by avoiding
	 * calculating it dynamically - doesn't worth it to over complicate things
	 */
	public static Double PIXEL_WIDTH = 1e-2;
	public static Double PIXEL_HEIGHT = 1e-2;

	public static Integer sign(Double x) {
		if(x == null) {
			return null;
		}
		return x >= 0 ? 1 : -1;
	}

	public static Integer sign(Integer x) {
		if(x == null) {
			return null;
		}
		return x >= 0 ? 1 : -1;
	}

	public static Integer sign(Long x) {
		if(x == null) {
			return null;
		}
		return x >= 0 ? 1 : -1;
	}

	public static Integer sign(Float x) {
		if(x == null) {
			return null;
		}
		return x >= 0 ? 1 : -1;
	}

	//________________________max_________________________________________
	public static Double max(double... doubleArray) {
		if(doubleArray.length > 0) {
			double max = doubleArray[0];
			for(double d : doubleArray) {
				if(d > max) {
					max = d;
				}
			}
			return max;
		}
		return null;
	}

	public static Integer max(int... intArray) {
		if(intArray.length > 0) {
			int max = intArray[0];
			for(int n : intArray) {
				if(n > max) {
					max = n;
				}
			}
			return max;
		}
		return null;
	}

	public static Long max(long... longArray) {
		if(longArray.length > 0) {
			long max = longArray[0];
			for(long l : longArray) {
				if(l > max) {
					max = l;
				}
			}
			return max;
		}
		return null;
	}

	public static Double max(ViewPortComponent viewPort) throws Exception {
		return max(viewPort, viewPort.getMappedFunctionsArray());
	}

	public static Double max(ViewPortComponent viewPortComponent, Function[] functions) throws Exception {
		OptimaProcessor.FUNCTIONS_ARRAY_MAX_EXTRACTOR.set(functions);
		return process(viewPortComponent, OptimaProcessor.FUNCTIONS_ARRAY_MAX_EXTRACTOR);
	}
	//______________________________end max________________________________________________


	//______________________________min_____________________________________________________
	public static Double min(double... doubleArray) {
		if(doubleArray.length > 0) {
			double min = doubleArray[0];
			for(double d : doubleArray) {
				if(d < min) {
					min = d;
				}
			}
			return min;
		}
		return null;
	}

	public static Integer min(int... intArray) {
		if(intArray.length > 0) {
			int min = intArray[0];
			for(int n : intArray) {
				if(n < min) {
					min = n;
				}
			}
			return min;
		}
		return null;
	}

	public static Long min(long... longArray) {
		if(longArray.length > 0) {
			long min = longArray[0];
			for(long l : longArray) {
				if(l < min) {
					min = l;
				}
			}
			return min;
		}
		return null;
	}

	public static Double min(ViewPortComponent viewPort) throws Exception {
		return min(viewPort, viewPort.getMappedFunctionsArray());
	}

	public static Double min(ViewPortComponent viewPortComponent, Function[] functions) throws Exception {
		OptimaProcessor.FUNCTIONS_ARRAY_MIN_EXTRACTOR.set(functions);
		return process(viewPortComponent, OptimaProcessor.FUNCTIONS_ARRAY_MIN_EXTRACTOR);
	}
	//_________________________________end min____________________________________________



	public static double closestPeriod(double x, double period, double offset) {
		long k = Math.round((x - offset) / period);
		double closestPeriod = offset + k * period;
		return closestPeriod;
	}

	public static Double tan(Double x) {
		double closestPeriod = closestPeriod(x, Math.PI, Math.PI/2);
		double diff  = closestPeriod - x;
		if(Math.abs(diff) < PIXEL_WIDTH) {
			return sign(diff) * Double.POSITIVE_INFINITY;
		} else {
			return Math.tan(x);
		}
	}


	private static <T> Double process(ViewPortComponent viewPort, GenericProcessor<T> processor) throws Exception {
		Double minX = viewPort.getXMinValue(), maxX = viewPort.getXMaxValue();
		if(minX != null && maxX != null) {
			int xPixelsCount = viewPort.getXPixelsCount();
			if(xPixelsCount > 0) {
				double step = (maxX - minX) / xPixelsCount,
						errorMargin = step / 100.;
				Double result = null;
				for(double x = minX; x < maxX + errorMargin; x += step) {
					result = processor.process(x, result);
				}
				return result;
			}
		}
		return null;
	}

}
