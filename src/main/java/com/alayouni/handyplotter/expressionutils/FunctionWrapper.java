package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/18/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FunctionWrapper extends Wrapper implements Function {

    private Function function;


    public FunctionWrapper(Integer index,
                           final String expression) {
        super(FunctionComponent.FUNCTION_PREFIX, null, index, expression);
    }

    public void setFunction(Function function) {
        this.function = function;
    }



    public Double f(double x) throws Exception {
        try {
            return function.f(x);
        } catch (Exception e) {
            throw new ScriptEvalException(toString() + "(x)", expression, e);
        }
    }

    @Override
    public String toString() {
        return getIdentifier();
    }
}
