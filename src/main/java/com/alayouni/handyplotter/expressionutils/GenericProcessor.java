package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/11/13
 * Time: 8:42 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GenericProcessor<T> {
	Double process(double x, Double lastResult) throws Exception;

	void set(T t);
}
