package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/12/13
 * Time: 12:14 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class OptimaProcessor implements GenericProcessor<Function[]> {
	protected boolean isValid(Double d) {
		return d != null && !d.isInfinite() && !d.isNaN();
	}

	public static final OptimaProcessor FUNCTIONS_ARRAY_MAX_EXTRACTOR =
			new OptimaProcessor() {
				private Function[] functions;

				public Double process(double x, Double max) throws Exception {
					Double fx;
					for(Function f : functions) {
						fx = f.f(x);
						if(isValid(fx) && (max == null || fx > max)) {
							max = fx;
						}
					}
					return max;
				}


				public void set(Function[] functions) {
					this.functions = functions;
				}
			};
	//_______________________________end max processors_________________________________________________________




	//_______________________________min processors_____________________________________________________________
	public static final OptimaProcessor FUNCTIONS_ARRAY_MIN_EXTRACTOR =
			new OptimaProcessor() {
				private Function[] functions;

				public Double process(double x, Double min) throws Exception {
					Double fx;
					for(Function f : functions) {
						fx = f.f(x);
						if(isValid(fx) && (min == null || fx < min)) {
							min = fx;
						}
					}
					return min;
				}


				public void set(Function[] functions) {
					this.functions = functions;
				}
			};
	//_______________________________end min processors_________________________________________________________

}
