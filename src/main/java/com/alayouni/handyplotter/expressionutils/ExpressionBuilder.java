package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.FunctionComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.VariableComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.ViewPortComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisComponent;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisUnReferencableInputExpressions;
import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisUnReferencableVariables;
import org.abstractmeta.toolbox.compilation.compiler.JavaSourceCompiler;
import org.abstractmeta.toolbox.compilation.compiler.impl.JavaSourceCompilerImpl;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/15/13
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExpressionBuilder {
	private static final String INFINITY_REGEX = "\\binfinity\\b";
	private static final String INFINITY_REPLACEMENT = "Double.POSITIVE_INFINITY";

	private static final String PIXEL_WIDTH_REGEX = "\\bpixelW\\b";
	private static final String PIXEL_WIDTH_REPLACEMENT = MathUtils.class.getSimpleName() + ".PIXEL_WIDTH";

	private static final String PIXEL_HEIGHT_REGEX = "\\bpixelH\\b";
	private static final String PIXEL_HEIGHT_REPLACEMENT = MathUtils.class.getSimpleName() + ".PIXEL_HEIGHT";

	private static final String EXPRESSION_UTILS_PACKAGE_NAME = ExpressionBuilder.class.getPackage().getName();
	private static final String EXPRESSION_BUILD_CLASS_NAME = ExpressionBuilder.class.getSimpleName();

	private static final String COMPILED_EXPRESSIONS_LOADER_NAME = CompiledExpressionsLoader.class.getSimpleName();
	private static final String COMPILED_EXPRESSIONS_LOADER_IMPL_NAME = COMPILED_EXPRESSIONS_LOADER_NAME + "Impl";
	private static final String COMPILED_EXPRESSIONS_LOADER_IMPL_FULL_NAME = CompiledExpressionsLoader.class.getName() + "Impl";

	private static final String VARIABLE_CLASS_NAME = Variable.class.getSimpleName();

	private static final String FUNCTION_CLASS_NAME = Function.class.getSimpleName();

	/**
	 * if 2 static methods have the same name under 2 different classes the latter will be considered.
	 * Example: max exists under Math and MathUtils, since MathUtils is the latter in the list MathUtils.max will
	 * be considered when processing expressions
	 */
	private static final Class[] UTIL_METHODS_CLASSES = {Math.class, MathUtils.class};

    public static List<VariableWrapper> a = new ArrayList<VariableWrapper>();
    public static List<FunctionWrapper> f = new ArrayList<FunctionWrapper>();
	public static List<VariableWrapper> min = new ArrayList<VariableWrapper>();
	public static List<VariableWrapper> max = new ArrayList<VariableWrapper>();
	public static List<AxisUnReferencableVariables> axisUnReferencableVariables = new ArrayList<AxisUnReferencableVariables>();
	public static List<ViewPortComponent> viewPorts = new ArrayList<ViewPortComponent>();


	/**
	 * Maps each util static method to its class
	 */
	private final Map<String, String> utilMethods;

	private final MaxMinMethodParamsProcessor maxMinMethodParamsProcessor;

	JavaSourceCompiler allExpressionsSourceCompiler;
	JavaSourceCompiler.CompilationUnit allExpressionsCompilationUnit;
	ClassLoader allExpressionsClassLoader;
	Class allExpressionsCompiledClass;

    private ExpressionBuilder() {
	    allExpressionsSourceCompiler = new JavaSourceCompilerImpl();
	    utilMethods = new HashMap<String, String>();
	    loadPublicStaticMethodNamesFromClass();
	    maxMinMethodParamsProcessor = new MaxMinMethodParamsProcessor();
    }

	private void loadPublicStaticMethodNamesFromClass() {
		for(Class cl : UTIL_METHODS_CLASSES) {
			for(Method method: cl.getMethods()) {
				if(Modifier.isStatic(method.getModifiers()) && Modifier.isPublic(method.getModifiers())) {
					utilMethods.put(method.getName(), cl.getSimpleName());
				}
			}
		}
	}

	/**
	 * UnReferencable variables cannot cause any dependency cycles and are so dependency safe.
	 * Dependency safe variables should still be compiled with all the rest of code input in order to
	 * have one compile per build and gain in terms of performance.
	 */
    public void buildExpressions(final List<String> functionExpressions,
                                     final List<String> variableExpressions,
                                     final List<String> minAxisExpressions,
                                     final List<String> maxAxisExpressions,
                                     final List<AxisUnReferencableInputExpressions> axisUnReferencableInput) throws Exception {
		reInitAll(functionExpressions, variableExpressions, minAxisExpressions, maxAxisExpressions, axisUnReferencableInput);
		verifyNoCallToViewPortOptimasFromFExpressions();
		buildAllDependencies();
		verifyAllDependencies();
		reprocessAllVariableDependencies(false);

		List<VariableWrapper> variablesOrderedByDependencies = buildDependentsForAllVariables();

		String compilableSource = buildCompilableExpressionsSourceCode(functionExpressions,
				variableExpressions,
				minAxisExpressions,
				maxAxisExpressions,
				axisUnReferencableInput);
		loadAllFromCompilableSource(compilableSource);

		computeAllVariablesValues(variablesOrderedByDependencies);
    }

	private String addReturnStatementToExpressionIfNotExisting(String expression) {
		final int returnLength = "return".length();
		int index = expression.indexOf("return");
		String boundaryChar;
		while(index != -1) {
			if(index > 0) {
				//make sure the predecessor character is a non word
				boundaryChar = expression.substring(index - 1, index);
				if(boundaryChar.matches("\\w")) {
					index += returnLength;
					index = expression.indexOf("return", index);
					continue;
				}
			}
			index += returnLength;
			if(index < expression.length()) {
				//make the next character is a non word
				boundaryChar = expression.substring(index, index + 1);
				if(boundaryChar.matches("\\w")) {
					index = expression.indexOf("return", index);
					continue;
				}
			}

			//if we're here means a 'return' word is detected under expression => return expression as is
			return expression;
		}

		//if we're here means no 'return' word has been found int he previous loop => add a return statement
		expression = "return " + expression;
		expression = expression.trim();
		if(!expression.endsWith(";")) {
			expression += ";";
		}

		return expression;
	}


	private void resetVariablesDependencies(List<VariableWrapper> variableWrappers) {
		for(VariableWrapper vw : variableWrappers) {
			vw.resetDependenciesAndDependents();
		}
	}

	private void resetAllVariableDependencies() {
		resetVariablesDependencies(ExpressionBuilder.a);
		resetVariablesDependencies(ExpressionBuilder.min);
		resetVariablesDependencies(ExpressionBuilder.max);
		resetDependenciesOfUnReferencableVariables();
		resetDependentsViewPortsBoundaries();
	}

	private void resetDependentsViewPortsBoundaries() {
		for(ViewPortComponent viewPort : viewPorts) {
			viewPort.getBoundaryDependentsWrapper().resetDependenciesAndDependents();
		}
	}

	private void resetDependenciesOfUnReferencableVariables() {
		VariableWrapper wrapper;
		for(AxisUnReferencableVariables unReferencableVariables : axisUnReferencableVariables) {
			wrapper = unReferencableVariables.getLocationVariable();
			wrapper.resetDependenciesAndDependents();

			wrapper= unReferencableVariables.getOffsetVariable();
			wrapper.resetDependenciesAndDependents();

			wrapper = unReferencableVariables.getUnitVariable();
			wrapper.resetDependenciesAndDependents();
		}
	}

	/**
	 * sets un-direct dependencies
	 */
	public void reprocessAllVariableDependencies(boolean resetDependencies) {
		if(resetDependencies) {
			resetAllVariableDependencies();
		}
		reprocessVariableDependencies(ExpressionBuilder.a);
		reprocessVariableDependencies(ExpressionBuilder.min);
		reprocessVariableDependencies(ExpressionBuilder.max);
		reprocessUnReferencableVariables();
		reprocessViewPortsBoundaryDependencies();
	}

	/**
	 * <p>
	 *     viewPort.getBoundaryDependentsWrapper() has been set as a dependency to all its boundary fields so that
	 *     it gets propagated as a dependency to any variable that depends on a boundary field.
	 * </p>
	 * <p>
	 *     after all variables get processed, boundary fields need to be reprocessed so they don't refer to
	 *     viewPort.getBoundaryDependentsWrapper()'s as a dependency unless it really is.
	 * </p>
	 * <p>
	 *     Should be called after reprocessing all variables.
	 * </p>
	 */
	private void reprocessViewPortsBoundaryDependencies() {
		VariableWrapper viewPortBoundaryDependentsWrapper;
		AxisComponent axis;
		for(ViewPortComponent viewPort: viewPorts) {
			viewPortBoundaryDependentsWrapper = viewPort.getBoundaryDependentsWrapper();
			axis = viewPort.getHAxis();
			reprocessViewPortAxisBoundaries(viewPortBoundaryDependentsWrapper, axis);

			axis = viewPort.getVAxis();
			reprocessViewPortAxisBoundaries(viewPortBoundaryDependentsWrapper, axis);
		}

	}

	private void reprocessViewPortAxisBoundaries(VariableWrapper viewPortBoundaryDependentsWrapper,
	                                             AxisComponent axis) {
		int axisIndex = axis.getIndex();
		VariableWrapper boundaryVariableWrapper = ExpressionBuilder.min.get(axisIndex);
		reprocessViewPortBoundaryDependencies(viewPortBoundaryDependentsWrapper, boundaryVariableWrapper);

		boundaryVariableWrapper = ExpressionBuilder.max.get(axisIndex);
		reprocessViewPortBoundaryDependencies(viewPortBoundaryDependentsWrapper, boundaryVariableWrapper);
	}

	private void reprocessViewPortBoundaryDependencies(VariableWrapper viewPortBoundaryDependentsWrapper,
	                                                    VariableWrapper boundaryVariableWrapper) {
		Set<VariableWrapper> indirectDependencies = boundaryVariableWrapper.getIndirectDependencies();
		if(!indirectDependencies.contains(viewPortBoundaryDependentsWrapper)) {
			Set<VariableWrapper> dependencies = boundaryVariableWrapper.getDependencyVariables();
			dependencies.remove(viewPortBoundaryDependentsWrapper);
		}
	}

    /**
     * sets indirect dependencies
     */
    private void reprocessVariableDependencies(List<VariableWrapper> variableWrappers) {
        for(VariableWrapper variableWrapper : variableWrappers) {
            variableWrapper.reProcessVariableDependencies();
        }
    }

	/**
	 * sets un-direct dependencies
	 */
	private void reprocessUnReferencableVariables() {
		VariableWrapper wrapper;
		for(AxisUnReferencableVariables unReferencableVariables : axisUnReferencableVariables) {
			wrapper = unReferencableVariables.getLocationVariable();
			wrapper.reProcessVariableDependencies();

			wrapper= unReferencableVariables.getOffsetVariable();
			wrapper.reProcessVariableDependencies();

			wrapper = unReferencableVariables.getUnitVariable();
			wrapper.reProcessVariableDependencies();
		}
	}


    /**
     * The order when building dependents must be from the most independent variable to the most dependent
     * so that when spinning a variable the dependent variables get updated in the right order
     */
    public List<VariableWrapper> buildDependentsForAllVariables() {
        List<VariableWrapper> orderedProcessedVariables = new ArrayList<VariableWrapper>();
        Set<VariableWrapper> variablesToProcess = initVariablesToProcessAsDependents();

        while(variablesToProcess.size() > 0) {
            for(VariableWrapper vw : variablesToProcess) {
                if(orderedProcessedVariables.containsAll(vw.getDependencyVariables())) {
                    orderedProcessedVariables.add(vw);
                    variablesToProcess.remove(vw);
                    vw.addAsDependentOfDependencies();
                    break;
                }
            }
        }
        return orderedProcessedVariables;
    }

	private Set<VariableWrapper> initVariablesToProcessAsDependents() {
		Set<VariableWrapper> variablesToProcess = new HashSet<VariableWrapper>(ExpressionBuilder.a);

		variablesToProcess.addAll(ExpressionBuilder.min);
		variablesToProcess.addAll(ExpressionBuilder.max);


		for(AxisUnReferencableVariables unReferencable : axisUnReferencableVariables) {
			variablesToProcess.add(unReferencable.getLocationVariable());
			variablesToProcess.add(unReferencable.getOffsetVariable());
			variablesToProcess.add(unReferencable.getUnitVariable());
		}

		for(ViewPortComponent viewPortComponent : viewPorts) {
			variablesToProcess.add(viewPortComponent.getBoundaryDependentsWrapper());
		}

		return variablesToProcess;
	}

    private void computeAllVariablesValues(List<VariableWrapper> variablesOrderedByDependencies) throws Exception {
        for(VariableWrapper vw: variablesOrderedByDependencies) {
	        if(vw.getVariable() != null) {
		        //viewPort boundaryDependentsWrappers are fake variables, they're just used to group
		        //unique and ordered boundaries dependents in one wrapper
                vw.f();
	        }
        }
    }

	private void processFunctionExpressions(List<String> fExpressions) {
		String expr;
		for(int i = 0; i < fExpressions.size(); i++) {
			expr = fExpressions.get(i);
			expr = processExpression(expr);
			expr = wrapExpression(expr, Function.class.getSimpleName(), "f(double x)");
			fExpressions.set(i, expr);
		}
	}

	private void processVariableExpressions(List<String> vExpressions) {
		String expr;
		for(int i = 0; i < vExpressions.size(); i++) {
			expr = vExpressions.get(i);
			expr = processExpression(expr);
			expr = wrapExpression(expr, VARIABLE_CLASS_NAME, "f()");
			vExpressions.set(i, expr);
		}
	}

	private void processUnreferencedVariableExpressions(List<AxisUnReferencableInputExpressions> unReferencableInputExpressions) {
		String expr;
		for(AxisUnReferencableInputExpressions unReferencableExpr : unReferencableInputExpressions) {
			expr = unReferencableExpr.getLocationExpression();
			expr = processExpression(expr);
			expr = wrapExpression(expr, VARIABLE_CLASS_NAME, "f()");
			unReferencableExpr.setLocationExpression(expr);

			expr = unReferencableExpr.getOffsetExpression();
			expr = processExpression(expr);
			expr = wrapExpression(expr, VARIABLE_CLASS_NAME, "f()");
			unReferencableExpr.setOffsetExpression(expr);

			expr = unReferencableExpr.getUnitExpression();
			expr = processExpression(expr);
			expr = wrapExpression(expr, VARIABLE_CLASS_NAME, "f()");
			unReferencableExpr.setUnitExpression(expr);
		}
	}

    private String processExpression(String expr) {
	    expr = expr.replaceAll(PIXEL_WIDTH_REGEX, PIXEL_WIDTH_REPLACEMENT);
	    expr = expr.replaceAll(PIXEL_HEIGHT_REGEX, PIXEL_HEIGHT_REPLACEMENT);
	    expr = expr.replaceAll(INFINITY_REGEX, INFINITY_REPLACEMENT);
	    expr = maxMinMethodParamsProcessor.processOptimaMethodsCalls(expr);
	    expr = addReturnStatementToExpressionIfNotExisting(expr);
	    expr = ensureReturnStatementWontHaveDoubleCastingProblems(expr);
        expr = prefixAllStaticMethodsCallsByClassName(expr);
        expr = prefixAllStaticFieldsByClassName(expr);
	    expr = replaceViewPorts(expr);
        expr = replaceVariables(expr);
        expr = replaceFunctions(expr);//functions should not force the followed by ( rule as they can be specified
	    //in the optima methods without any parenthesises
        return expr;
    }


    private String wrapExpression(String expr, final String interfaceName, final String methodSignature) {
        return "new " + interfaceName +"() {\n" +
		        "            @Override\n" +
                "            public Double " + methodSignature + " throws Exception {\n" +
                "                " + expr + "\n" +
                "            }\n" +
                "        }";
    }

	private String overrideGetFunctions(List<String> fExpressions) {
		return String.format("\n@Override\n" +
				"public %s[] getFunctions(){\n" +
				"return new %s[] {\n%s\n};\n" +
				"}\n",
				FUNCTION_CLASS_NAME,
				FUNCTION_CLASS_NAME,
				StringUtils.join(fExpressions, ", ")
		);
	}

	private <T> String overrideGetVariables(String methodName, List<T> vExpressions) {
		return String.format("\n@Override\n" +
				"public %s[] %s(){\n" +
				"return new %s[] {\n%s\n};\n" +
				"}\n",
				VARIABLE_CLASS_NAME,
				methodName,
				VARIABLE_CLASS_NAME,
				StringUtils.join(vExpressions, ", ")
		);
	}

	private String overrideAllUnReferencableVariables(List<AxisUnReferencableInputExpressions> unReferencableExpressions) {
		AxisUnReferencableInputExpressions.setToString(AxisUnReferencableInputExpressions.OFFSET_EXPRESSION);
		String getOffsetExpressions = overrideGetVariables("getOffsets", unReferencableExpressions);

		AxisUnReferencableInputExpressions.setToString(AxisUnReferencableInputExpressions.LOCATION_EXPRESSION);
		String getLocationExpressions = overrideGetVariables("getLocations", unReferencableExpressions);

		AxisUnReferencableInputExpressions.setToString(AxisUnReferencableInputExpressions.UNIT_EXPRESSION);
		String getUnitExpressions = overrideGetVariables("getUnits", unReferencableExpressions);

		return String.format("\n%s\n%s\n%s\n",
				getOffsetExpressions,
				getLocationExpressions,
				getUnitExpressions);
	}

	private String buildCompilableExpressionsSourceCode(final List<String> fExpressions,
	                                                  final List<String> vExpressions,
	                                                  final List<String> minExpressions,
	                                                  final List<String> maxExpressions,
	                                                  final List<AxisUnReferencableInputExpressions> unReferencableInput) {
		processFunctionExpressions(fExpressions);
		processVariableExpressions(vExpressions);
		processVariableExpressions(minExpressions);
		processVariableExpressions(maxExpressions);
		processUnreferencedVariableExpressions(unReferencableInput);

		final String getFunctionsSource = overrideGetFunctions(fExpressions),
		getVariablesSource = overrideGetVariables("getVariables", vExpressions),
		getMinSource = overrideGetVariables("getMin", minExpressions),
		getMaxSource = overrideGetVariables("getMax", maxExpressions),
		getUnReferencableSource = overrideAllUnReferencableVariables(unReferencableInput);

		final String source = String.format("package %s;\n\n" +
				"public class %s implements %s {\n%s\n%s\n%s\n%s\n%s\n}\n",
				EXPRESSION_UTILS_PACKAGE_NAME,
				COMPILED_EXPRESSIONS_LOADER_IMPL_NAME,
				COMPILED_EXPRESSIONS_LOADER_NAME,
				getFunctionsSource,
				getVariablesSource,
				getMinSource,
				getMaxSource,
				getUnReferencableSource);
		return source;
	}

	private void loadAllFromCompilableSource(String compilableSource) throws Exception {

		allExpressionsCompilationUnit = allExpressionsSourceCompiler.createCompilationUnit();
		allExpressionsCompilationUnit.addJavaSource(COMPILED_EXPRESSIONS_LOADER_IMPL_FULL_NAME, compilableSource);
		allExpressionsClassLoader = allExpressionsSourceCompiler.compile(allExpressionsCompilationUnit);
		allExpressionsCompiledClass = allExpressionsClassLoader.loadClass(COMPILED_EXPRESSIONS_LOADER_IMPL_FULL_NAME);
		CompiledExpressionsLoader compiledExpressionsLoader = (CompiledExpressionsLoader) allExpressionsCompiledClass.newInstance();

		populateAllFunctionWrappers(compiledExpressionsLoader.getFunctions());
		populateAllVariableWrappers(compiledExpressionsLoader.getVariables(), a);
		populateAllVariableWrappers(compiledExpressionsLoader.getMin(), min);
		populateAllVariableWrappers(compiledExpressionsLoader.getMax(), max);
		populateUnReferencableVariables(compiledExpressionsLoader);

	}

	private void populateAllFunctionWrappers(Function[] functions) {
		Function function;
		FunctionWrapper fw;
		for(int i = 0; i < f.size(); i++) {
			fw = f.get(i);
			function = functions[i];
			fw.setFunction(function);
		}
	}

	private void populateAllVariableWrappers(Variable[] loadedCompiledVariables, List<VariableWrapper> vws) {
		Variable variable;
		VariableWrapper vw;
		for(int i = 0; i < loadedCompiledVariables.length; i++) {
			vw = vws.get(i);
			variable = loadedCompiledVariables[i];
			vw.setVariable(variable);
		}
	}

	private void populateUnReferencableVariables(CompiledExpressionsLoader compiledExpressionsLoader) {
		Variable[] locations = compiledExpressionsLoader.getLocations();
		Variable[] offsets = compiledExpressionsLoader.getOffsets();
		Variable[] units = compiledExpressionsLoader.getUnits();
		AxisUnReferencableVariables unReferencableWrapper;
		VariableWrapper vw;
		for(int i = 0; i < axisUnReferencableVariables.size(); i++) {
			unReferencableWrapper = axisUnReferencableVariables.get(i);

			vw = unReferencableWrapper.getLocationVariable();
			vw.setVariable(locations[i]);

			vw = unReferencableWrapper.getUnitVariable();
			vw.setVariable(units[i]);

			vw = unReferencableWrapper.getOffsetVariable();
			vw.setVariable(offsets[i]);
		}
	}

	private String ensureReturnStatementWontHaveDoubleCastingProblems(String expr) {
		String regex = "(\\breturn )(.*?)(;)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expr);
		String returnExpr, castedExpr = expr;
		int shift = 0, castingLength = "(double)".length();
		while(matcher.find()) {
			returnExpr = matcher.group(2);
			if(isIntOrLong(returnExpr)) {
				castedExpr = String.format("%s%s%s",
						castedExpr.substring(0, matcher.start() + shift),
						"return (double)" + returnExpr + ";",
						expr.substring(matcher.end()));
				shift += castingLength;
			}
		}

		return castedExpr;
	}

	private boolean isIntOrLong(String expr) {
		try {
			Long.parseLong(expr);
			return true;
		} catch(NumberFormatException e) {
			return false;
		}
	}

    //_______________________ prefix all static methods calls by their class names____________________________
    private String prefixAllStaticMethodsCallsByClassName(String expr) {
	    String className;
	    for(String methodName : utilMethods.keySet()) {
		    className = utilMethods.get(methodName);
		    expr = prefixStaticMethodCallsByClassName(expr, className, methodName);

	    }
        return expr;
    }

    private String prefixStaticMethodCallsByClassName(String expr, final String className, final String methodName) {
	    //must be not preceded by a dot and followed by a (
        String regex = "(?<!\\.)\\b" + methodName + "\\s*\\(";
        expr = expr.replaceAll(regex, className + "." + methodName + "(");
        return expr;
    }
    //_______________________ end prefix all static methods calls by their class names____________________________

    //_______________________ prefix all public static fields calls by their class names____________________________
    private String prefixAllStaticFieldsByClassName(String expr) {
        for(Class cl : UTIL_METHODS_CLASSES) {
            for(Field field: cl.getFields()) {
                if(Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers())) {
                    expr = prefixStaticFieldByClassName(expr, cl.getSimpleName(), field.getName());
                }
            }
        }
        return expr;
    }

    private String prefixStaticFieldByClassName(String expr, final String className, final String fieldName) {
	    //must be not preceded by a dot and not followed by a (
	    String regex = "(?<!\\.)\\b" + fieldName + "\\b(?!\\()";
        expr = expr.replaceAll(regex, className + "." + fieldName);
        return expr;
    }
    //_______________________ end prefix all static methods calls by their class names____________________________


	//_______________________ replace indexed view ports by List elements from ExpressionBuilder.viewPorts____________________________
	private String replaceViewPorts(String expr) {
		String regex = "(?<!\\.)(\\bV)(\\d+\\b)(?!\\()";
		expr = expr.replaceAll(regex, EXPRESSION_BUILD_CLASS_NAME + ".viewPorts.get($2)");
		return expr;
	}
	//_______________________ end replace indexed variables by List elements from ExpressionBuilder.a________________________


    //_______________________ replace indexed variables by List elements from ExpressionBuilder.a____________________________
    private String replaceVariables(String expr) {
        String regex = "(?<!\\.)(\\ba)(\\d+\\b)(?!\\()";
        expr = expr.replaceAll(regex, EXPRESSION_BUILD_CLASS_NAME + ".a.get($2).getValue()");
        return expr;
    }
    //_______________________ end replace indexed variables by List elements from ExpressionBuilder.a________________________

    //_______________________ replace indexed functions by List elements from ExpressionBuilder.f____________________________
    private String replaceFunctions(String expr) {
	    String regex1 = "(?<!\\.)(\\bf)(\\d+)(\\s*?\\()", regex2 = "(\\bf)(\\d+\\b)";
        expr = expr.replaceAll(regex1, EXPRESSION_BUILD_CLASS_NAME + ".f.get($2).f(");
	    expr = expr.replaceAll(regex2, EXPRESSION_BUILD_CLASS_NAME + ".f.get($2)");
        return expr;
    }
    //_______________________ end replace indexed functions by List elements from ExpressionBuilder.f________________________

    //______________________________Verifying Dependencies__________________________________________

    Set<Wrapper> processedWrappers;

    private void verifyAllDependencies() throws Exception {
        processedWrappers = new HashSet<Wrapper>();
        verifyWrappersDependencies(ExpressionBuilder.f);
        verifyWrappersDependencies(ExpressionBuilder.a);
	    verifyWrappersDependencies(ExpressionBuilder.min);
	    verifyWrappersDependencies(ExpressionBuilder.max);
    }


    private void verifyWrappersDependencies(List<? extends Wrapper> wrappers) throws Exception {
        for(Wrapper wrapper : wrappers) {
            verifyDependenciesForWrapper(wrapper, new ArrayList<Wrapper>());
        }
    }


    private void verifyDependenciesForWrapper(Wrapper wrapper, List<Wrapper> pathElements) throws ExpressionProcessingException {
        if(!processedWrappers.contains(wrapper)) {//processed wrappers are guaranteed to participate in no cycle
            pathElements.add(wrapper);
            findDependenciesCyclesIfAny(pathElements, wrapper.getDependencyFunctions());
            findDependenciesCyclesIfAny(pathElements, wrapper.getDependencyVariables());
            pathElements.remove(pathElements.size() - 1);
            processedWrappers.add(wrapper);
        }
    }


    private <T extends Wrapper> void findDependenciesCyclesIfAny(List<Wrapper> pathElements,
                                                                Set<T> dependencies) throws ExpressionProcessingException {
        int indexInPathElements;

        for(Wrapper dependency : dependencies) {
            indexInPathElements = pathElements.indexOf(dependency);
            if(indexInPathElements >= 0) {
                pathElements.add(dependency);
                throw new ExpressionProcessingException("cycle detected " + pathElements.subList(indexInPathElements, pathElements.size()),
                                                        ExpressionProcessingException.CYCLE_EXCEPTION);
            } else {
                verifyDependenciesForWrapper(dependency, pathElements);
            }
        }
    }
    //______________________________End Verifying Dependencies__________________________________________






    //______________________________Building Dependencies__________________________________________
    private void  buildAllDependencies() throws Exception {
        buildAllDependenciesForWrappers(ExpressionBuilder.f);
        buildAllDependenciesForWrappers(ExpressionBuilder.a);
	    buildAllDependenciesForWrappers(ExpressionBuilder.min);
	    buildAllDependenciesForWrappers(ExpressionBuilder.max);
	    buildDependenciesForUnReferencableVariables();

	    //must be called AFTER building ExpressionBuilder.min and ExpressionBuilder.max dependencies
	    //otherwise its effect will be overridden
	    setViewPortsBoundaryDependentsWrapperAsDependencyOfBoundaryFields();
    }

	private void setViewPortsBoundaryDependentsWrapperAsDependencyOfBoundaryFields() {
		//this method will make sure all viewPort axises boundary fields refer to
		//viewPort.getBoundaryDependentsWrapper() as a dependency, so that any variable
		//that depends on a boundary field ends up depending on viewPort.getBoundaryDependentsWrapper() as well
		//which will later be used to build all viewPort boundaries dependents (in one "fake" wrapper) with no duplicates
		//and in the right dependencies order. Useful for the scrolling functionality, as when scrolling
		//more than one boundary field will be changing simultaneously and we don't wanna have duplicate
		//notifications sent to dependents
		VariableWrapper boundariesWrapper;
		for(ViewPortComponent viewPort : viewPorts) {
			//viewPort.getXXXAxis got refreshed before the build... axises are guaranteed to be valid
			boundariesWrapper = viewPort.getBoundaryDependentsWrapper();
			setAxisBoundaryDependency(boundariesWrapper, viewPort.getHAxis());
			setAxisBoundaryDependency(boundariesWrapper, viewPort.getVAxis());
		}
	}

	private void setAxisBoundaryDependency(VariableWrapper boundariesWrapper, AxisComponent axis) {
		//viewPort.getXXXAxis got refreshed before the build... axises are guaranteed to be valid
		int axisIndex = axis.getIndex();
		VariableWrapper vw = ExpressionBuilder.min.get(axisIndex);
		vw.addDirectVariableDependency(boundariesWrapper);

		vw = ExpressionBuilder.max.get(axisIndex);
		vw.addDirectVariableDependency(boundariesWrapper);
	}

    private <T extends Wrapper> void buildAllDependenciesForWrappers(List<T> wrappers) throws Exception {
        for(T wrapper : wrappers) {
	        this.buildDependenciesForWrapper(wrapper);
        }
    }

	private void buildDependenciesForUnReferencableVariables() throws Exception {
		VariableWrapper wrapper;
		for(AxisUnReferencableVariables unReferencableVariables : axisUnReferencableVariables) {
			wrapper = unReferencableVariables.getOffsetVariable();
			buildDependenciesForWrapper(wrapper);

			wrapper = unReferencableVariables.getLocationVariable();
			buildDependenciesForWrapper(wrapper);

			wrapper = unReferencableVariables.getUnitVariable();
			buildDependenciesForWrapper(wrapper);
		}
	}

	private <T extends Wrapper> void buildDependenciesForWrapper(T wrapper) throws Exception {
		String expression = wrapper.getExpression();
		Set<FunctionWrapper> functionDependencies = extractDependenciesFromExpression(expression, "(\\bf)(\\d+\\b)", ExpressionBuilder.f, wrapper);
		if(functionDependencies.contains(wrapper)) {
			throw new ExpressionProcessingException(wrapper.getIdentifier() + " cannot refer to itself!",
					ExpressionProcessingException.RECURSIVE_EXCEPTION);
		}
		Set<FunctionWrapper> implicitFunctionDependencies = maxMinMethodParamsProcessor.extractImplicitFunctionsDependencies(expression, wrapper);
		functionDependencies.addAll(implicitFunctionDependencies);

		Set<VariableWrapper> variableDependencies = extractDependenciesFromExpression(expression, "(\\ba)(\\d+\\b)", ExpressionBuilder.a, wrapper);
		if(variableDependencies.contains(wrapper)) {
			String identifier = wrapper.getIdentifier();
			throw new ExpressionProcessingException(identifier + " cannot refer to itself!",
					ExpressionProcessingException.RECURSIVE_EXCEPTION);
		}
		Set<VariableWrapper> axisDependencies = maxMinMethodParamsProcessor.extractAxisDependencies(expression, wrapper);
		variableDependencies.addAll(axisDependencies);

		wrapper.setDependencyFunctions(functionDependencies);
		wrapper.setDependencyVariables(variableDependencies);
	}


    private <T extends Wrapper> Set<T> extractDependenciesFromExpression(final String expr,
                                                                         final String regex,
                                                                         final List<T> wrappersList,
                                                                         final Wrapper referrer) throws ExpressionProcessingException {
        Set<T> dependencies = new HashSet<T>();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(expr);
        int index;
	    String prefixGroup, indexGroup;
        while(matcher.find()) {
	        indexGroup = matcher.group(2);
            index = Integer.parseInt(indexGroup);
	        if(index < wrappersList.size()) {
                dependencies.add(wrappersList.get(index));
	        } else {
		        prefixGroup = matcher.group(1);
		        throw new ExpressionProcessingException(String.format("%s%s referred by %s is undefined",
																        prefixGroup,
																        indexGroup,
																        referrer.getIndex()),
				                                        ExpressionProcessingException.UNDEFINED_ID_EXCEPTION);
	        }
        }
        return dependencies;
    }
    //______________________________End Building Dependencies__________________________________________


	private void verifyNoCallToViewPortOptimasFromFExpressions() throws ExpressionProcessingException {
		String expr;
		for(FunctionWrapper fw : f) {
			expr = fw.getExpression();
			if(maxMinMethodParamsProcessor.containsViewPortOptimaCall(expr)) {
				String msg = String.format("Un-authorized expression detected in %s: %s\n\n" +
						"Optima methods should not be called from function expressions for following reasons:\n" +
						"- They are heavy in terms of performance\n" +
						"- They are independent of x\n\n" +
						"Please consider referring to a variable that makes the call instead.",
						fw.getIdentifier(), expr);
				throw new ExpressionProcessingException(msg,
						ExpressionProcessingException.UN_AUTHORIZED_CALL);
			}
		}
	}

    private void clearAll() {
        ExpressionBuilder.a = new ArrayList<VariableWrapper>();
        ExpressionBuilder.f = new ArrayList<FunctionWrapper>();
        ExpressionBuilder.min = new ArrayList<VariableWrapper>();
        ExpressionBuilder.max = new ArrayList<VariableWrapper>();
	    ExpressionBuilder.axisUnReferencableVariables = new ArrayList<AxisUnReferencableVariables>();
    }

	private void reInitAll(final List<String> functionExpressions,
	                       final List<String> variableExpressions,
	                       final List<String> minAxisExpressions,
	                       final List<String> maxAxisExpressions,
	                       final List<AxisUnReferencableInputExpressions> axisUnReferencableInput) {
		clearAll();
		initiateFunctions(functionExpressions);
		initiateVariables(VariableComponent.VARIABLE_PREFIX, null, variableExpressions, ExpressionBuilder.a);
		initiateVariables(AxisComponent.AXIS_PREFIX, "min", minAxisExpressions, ExpressionBuilder.min);
		initiateVariables(AxisComponent.AXIS_PREFIX, "max", maxAxisExpressions, ExpressionBuilder.max);
		initAxisUnReferencableVariables(axisUnReferencableInput);
		initiateViewPorts();
	}

	private void initiateVariables(String prefix,
	                               String suffix,
	                               List<String> expressionList,
	                               List<VariableWrapper> variablesList) {
		VariableWrapper vWrapper;
		for(int i = 0; i < expressionList.size(); i++) {
			vWrapper = new VariableWrapper(prefix, suffix, i, expressionList.get(i));
			variablesList.add(vWrapper);
		}
	}

	private void initAxisUnReferencableVariables(List<AxisUnReferencableInputExpressions> axisInput) {
		AxisUnReferencableVariables axisVariables;
		AxisUnReferencableInputExpressions input;
		VariableWrapper offsetVariable, locationVariable, unitVariable;
		final String offsetSuffix = AxisComponent.OFFSET_LABEL.toLowerCase(),
				locationSuffix = AxisComponent.LOCATION_LABEL.toLowerCase(),
				unitSuffix = AxisComponent.UNIT_LABEL.toLowerCase();
		for(int i = 0; i < axisInput.size(); i++) {
			input = axisInput.get(i);
			axisVariables = new AxisUnReferencableVariables();

			offsetVariable = new VariableWrapper(AxisComponent.AXIS_PREFIX, offsetSuffix, i, input.getOffsetExpression());

			locationVariable = new VariableWrapper(AxisComponent.AXIS_PREFIX, locationSuffix, i, input.getLocationExpression());

			unitVariable = new VariableWrapper(AxisComponent.AXIS_PREFIX, unitSuffix, i, input.getUnitExpression());

			axisVariables.setOffsetVariable(offsetVariable);
			axisVariables.setLocationVariable(locationVariable);
			axisVariables.setUnitVariable(unitVariable);
			axisUnReferencableVariables.add(axisVariables);
		}
	}

	private void initiateFunctions(List<String> functionExpressions) {
		FunctionWrapper fWrapper;
		for(int i = 0; i < functionExpressions.size(); i++) {
			fWrapper = new FunctionWrapper(i, functionExpressions.get(i));
			ExpressionBuilder.f.add(fWrapper);
		}
	}

	/**
	 * viewPortsBoundaryDependentsWrappers are used to notify the dependents of all boundary fields in the right
	 * order and each only once when 2 or more boundary fields get updated at the same time.
	 */
	private void initiateViewPorts() {
		VariableWrapper vw;
		Function[] mappedFunctionsArray;
		for(ViewPortComponent viewPortComponent : viewPorts) {
			vw = new VariableWrapper("V", null, viewPortComponent.getIndex(), "-");
			viewPortComponent.setBoundaryDependentsWrapper(vw);
			mappedFunctionsArray = buildMappedFunctionsArray(viewPortComponent);
			viewPortComponent.setMappedFunctionsArray(mappedFunctionsArray);
		}
	}

	private Function[] buildMappedFunctionsArray(ViewPortComponent viewPort) {
		List<FunctionComponent> mappedFunctions = viewPort.getMappedFunctions();
		Function[] mappedFunctionsArray = new Function[mappedFunctions.size()];
		int index;
		FunctionComponent fComp;
		for(int i = 0; i < mappedFunctions.size(); i++) {
			fComp = mappedFunctions.get(i);
			index = fComp.getIndex();
			mappedFunctionsArray[i] = f.get(index);
		}
		return mappedFunctionsArray;
	}
    //_________________________________________________________________________________________________


	private static ExpressionBuilder instance = null;
	public static ExpressionBuilder getInstance() {
		if(instance == null) {
			instance = new ExpressionBuilder();
		}
		return instance;
	}

	public static void setViewPorts(List<ViewPortComponent> viewPorts) {
		ExpressionBuilder.viewPorts = viewPorts;
	}
}
