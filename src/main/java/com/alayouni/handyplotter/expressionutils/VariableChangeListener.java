package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/29/13
 * Time: 6:43 PM
 * To change this template use File | Settings | File Templates.
 */
public interface VariableChangeListener {
	void variableValueChanged(VariableWrapper v) throws Exception;
}
