package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/17/13
 * Time: 5:05 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CompiledExpressionsLoader {

	Function[] getFunctions();

	Variable[] getVariables();

	Variable[] getMin();

	Variable[] getMax();

	Variable[] getOffsets();

	Variable[] getUnits();

	Variable[] getLocations();
}
