package com.alayouni.handyplotter.expressionutils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/18/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class VariableWrapper extends Wrapper implements Variable {

    private Variable variable;

	/**
	 * to avoid null pointer exceptions due to invocations of variables/functions by unexpected repainting on the event dispatch thread
	 * ==> avoiding synchronization for performance
	 */
    private Double value = 0.;

    private List<VariableWrapper> dependentVariables;
	private Set<VariableWrapper> directVariableDependencies;
	private Set<VariableWrapper> indirectDependencies;

	private List<VariableChangeListener> listeners = new ArrayList<VariableChangeListener>();


    public VariableWrapper(final String prefix,
                           final String suffix,
		                   Integer index,
                           final String expression) {
        super(prefix, suffix, index, expression);
        dependentVariables = new ArrayList<VariableWrapper>();
	    directVariableDependencies = new HashSet<VariableWrapper>();
    }



    public void setVariable(Variable variable) {
        this.variable = variable;
    }



    public Double f() throws Exception {
	    try{
		    setValue(variable.f());
		    return value;
	    } catch (Exception e) {
		    throw new ExpressionProcessingException(getIdentifier() + " " + expression,
				    ExpressionProcessingException.EXPRESSION_EVAL_EXCEPTION,
				    e);
	    }
    }

    public Double getValue() {
        return value;
    }

    /**
     * sets the current value without updating dependents, neither firing a variable value changed event. Those
     * should be handled externally as needed
     * @param value
     * @see #updateDependents(boolean)
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * update all the variables that depend on this
     */
    public void updateDependents(boolean fireDependentValueChanged) throws Exception {
        //dependents have been built in the right order based on dependencies
        // if a1 depends on a2 depends on this => dependents will have a2, a1 order
        // also dependent variables have been built to include recursively all the dependents even if their
        // relationship is not direct
        for(VariableWrapper vw : getDependentVariables()) {
            vw.f();
	        if(fireDependentValueChanged) {
		        vw.fireVariableValueChanged();
	        }
        }
    }

    public List<VariableWrapper> getDependentVariables() {
        return dependentVariables;
    }

	/**
	 * adds indirect dependencies.
	 */
    public void reProcessVariableDependencies() {
        indirectDependencies = new HashSet<VariableWrapper>();
        reProcessVariableDependencies(this.getDependencyVariables());
        reProcessVariableDependencies(this.getDependencyFunctions());
	    getDependencyVariables().addAll(indirectDependencies);
    }

    private <T extends Wrapper> void reProcessVariableDependencies(final Set<T> dependenciesToCrawl) {
	    Set<VariableWrapper> variableDependencies;
        for(Wrapper dependency : dependenciesToCrawl) {
	        variableDependencies = dependency.getDependencyVariables();
	        indirectDependencies.addAll(variableDependencies);
            reProcessVariableDependencies(variableDependencies);
            reProcessVariableDependencies(dependency.getDependencyFunctions());
        }
    }

    public void addAsDependentOfDependencies() {
        for(VariableWrapper dependency : getDependencyVariables()) {
            dependency.dependentVariables.add(this);
        }
    }

    @Override
    public String toString() {
        return getIdentifier();
    }

	public void addVariableChangeListener(VariableChangeListener listener) {
		listeners.add(listener);
	}

	public void fireVariableValueChanged() throws Exception {
		for(VariableChangeListener listener : listeners) {
			listener.variableValueChanged(this);
		}
	}

	public Variable getVariable() {
		return variable;
	}

	public void incrementValue(double step) {
		value += step;
	}

	@Override
	public void setDependencyVariables(Set<VariableWrapper> dependencyVariables) {
		super.setDependencyVariables(dependencyVariables);
		directVariableDependencies = new HashSet<VariableWrapper>(dependencyVariables);
	}

	/**
	 * Sets the dependencies of this to a clone of its indirect dependencies.
	 */
	public void resetDependenciesAndDependents() {
		Set<VariableWrapper> directDependencies = new HashSet<VariableWrapper>(directVariableDependencies);
		super.setDependencyVariables(directDependencies);
		dependentVariables.clear();
	}


	public void addDirectVariableDependency(VariableWrapper dependency) {
		directVariableDependencies.add(dependency);
		getDependencyVariables().add(dependency);
	}

	public Set<VariableWrapper> getIndirectDependencies() {
		return indirectDependencies;
	}
}
