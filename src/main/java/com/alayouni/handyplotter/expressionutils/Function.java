package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/14/13
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Function {

    /**
     * @param x
     * @return f(x) if f is defined in x, otherwise returns null
     */
    Double f(double x) throws Exception;
}
