package com.alayouni.handyplotter.expressionutils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 10/20/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExpressionProcessorImpl implements ExpressionProcessor {
	private static class ExpressionReplacement {
		private String regex;
		private String replacement;

		private ExpressionReplacement(String regex, String replacement) {
			this.regex = regex;
			this.replacement = replacement;
		}
	}

	private List<ExpressionReplacement> replacements;
	private final String indexPrefix;

	public ExpressionProcessorImpl(String indexPrefix) {
		this.indexPrefix = indexPrefix;
	}

	public void buildItemAddedExpressionReplacements(int addedItemIndex, int itemsCount) {
		replacements = new ArrayList<ExpressionReplacement>();
		String regex, replacement;
		for(int i = itemsCount - 2; i >= addedItemIndex; i--) {
			regex = "\\b" + indexPrefix + i + "\\b";
			replacement = indexPrefix + (i + 1);
			replacements.add(new ExpressionReplacement(regex, replacement));
		}
	}


	public void buildItemRemovedExpressionReplacements(int removedItemIndex, int itemsCount) {
		replacements = new ArrayList<ExpressionReplacement>();
		String regex, replacement;
		for(int i = removedItemIndex + 1; i <= itemsCount; i++) {
			regex = "\\b" + indexPrefix + i + "\\b";
			replacement = indexPrefix + (i - 1);
			replacements.add(new ExpressionReplacement(regex, replacement));
		}
	}


	@Override
	public String processExpression(String expression) {
		for(ExpressionReplacement replacement : replacements) {
			expression = expression.replaceAll(replacement.regex, replacement.replacement);
		}
		return expression;
	}
}
