package com.alayouni.handyplotter.expressionutils;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/22/13
 * Time: 12:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScriptEvalException extends Exception {

    public ScriptEvalException(final String expressionIdentifier, String expression, Exception e) {
        super("Double " + expressionIdentifier + ": {" + expression + "} " + e.getMessage(), e);
    }
}
