package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisTicksCountProcessor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 11/11/13
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class MaxMinMethodParamsProcessorTest {
	@Test
	public void testProcessOptimaMethodsCalls() throws Exception {
		MaxMinMethodParamsProcessor processor = new MaxMinMethodParamsProcessor();
		String expression = "min(5., 3, 4, 2.5, min(V0, f1, f2) , j, max(V0))";
		String processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("min(new double[] {((double)5.),((double)3),((double)4),((double)2.5),min(V0, new Function[]{ f1, f2}),j,max(V0)})", processed);

		expression = "min(double {5., 3, 4, 2.5, max(V0, \nf1, \nf2)\n , j, min(V0)})";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("min(new double[] {((double)5.),((double)3),((double)4),((double)2.5),max(V0, new Function[]{ \nf1, \nf2}),j,min(V0)})", processed);

		expression = "min(int {5., 3, 4, 2.5, max(V0, f1, f2) , j, max(V0)})";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("min(new int[] {((int)5.),((int)3),((int)4),((int)2.5),max(V0, new Function[]{ f1, f2}),j,max(V0)})", processed);

		expression = "min(long {5., 3, 4, 2.5, \nmin(\nV0, f1, f2) , j, min(V0)})";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("min(new long[] {((long)5.),((long)3),((long)4),((long)2.5),min(V0, new Function[]{ f1, f2}),j,min(V0)})", processed);


		expression = "min(double {\n\tmin(V0, f1, f2)\t\n,max(4, 2)})";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("min(new double[] {min(V0, new Function[]{ f1, f2}),max(new double[] {((double)4),((double)2)})})", processed);

		expression = "some code; some other code;min(min(V0, f1, f2),max(4, 2)), some more code";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("some code; some other code;" +
				"min(new double[] {min(V0, new Function[]{ f1, f2}),max(new double[] {((double)4),((double)2)})})" +
				", some more code", processed);


		expression = "some code; some other code;min(max(4, 2)), some more code";
		processed = processor.processOptimaMethodsCalls(expression);
		assertEquals("some code; some other code;" +
				"min(new double[] {max(new double[] {((double)4),((double)2)})})" +
				", some more code", processed);
	}


	@Test
	public void testDummy() throws Exception {
		AxisTicksCountProcessor axisTicksCountProcessor = new AxisTicksCountProcessor();
		System.out.println(axisTicksCountProcessor.evaluateSpecialExpression("-20t"));
	}


}
