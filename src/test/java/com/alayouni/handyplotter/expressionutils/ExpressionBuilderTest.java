package com.alayouni.handyplotter.expressionutils;

import com.alayouni.handyplotter.ui.components.splitpane.items.axis.AxisUnReferencableInputExpressions;
import org.junit.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 * Date: 9/21/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExpressionBuilderTest {

    @Test
    public void testBuildExpressionsCycleError() {
        //prepare the test scenario
        String[] fExpressions = {
                "return a1 * f2(x) + cos(x);",//f0
                //______________________________
                "return a2 + 6.3;",//f1
                //_____________________________
                "if(x < 0) {" +//f2
                        "return sin(x);" +
                        "} else {" +
                        "return a2 * x;" +
                        "}",
                //______________________________
                "return 2 * x + 11;"//f3
        };

        String[] aExpressions = {
                "return f1(0.25);",//a0
                //___________________________
                "return 3.;",//a1
                //___________________________
                "return f0(a0);"//a2
        };
        List<String> fList = Arrays.asList(fExpressions);
        List<String> aList = Arrays.asList(aExpressions);

        //make sure a cycle is detected a2 -> f0 -> f2 -> a2
        ExpressionBuilder expressionBuilder = ExpressionBuilder.getInstance();
        boolean thrown =false;
        try {
            expressionBuilder.buildExpressions(fList, aList, new ArrayList<String>(), new ArrayList<String>(), new ArrayList<AxisUnReferencableInputExpressions>());
        } catch (Exception e) {
            thrown = true;
            assert e instanceof ExpressionProcessingException;

            ExpressionProcessingException ee = (ExpressionProcessingException)e;
            assertEquals(ee.getExceptionType(), ExpressionProcessingException.CYCLE_EXCEPTION);
        }
        assertTrue(thrown);

        //test the generated functions/variables against some sample expected results
        //for variables make sure dependent variables get updated when changing their dependency
    }


    @Test
    public void testBuildExpressionsRecursiveError() {
        //prepare the test scenario
        String[] fExpressions = {
                "return f2(x) + cos(x);",//f0
                //______________________________
                "return 6.3;",//f1
                //_____________________________
                "return 2 * x + 11;"//f2
        };

        String[] aExpressions = {
                "return f0(0.25);",//a0
                //___________________________
                "return 3. + a1;",//a1
        };
        List<String> fList = Arrays.asList(fExpressions);
        List<String> aList = Arrays.asList(aExpressions);

        //make sure a1 expression is detected as a recursive call
        ExpressionBuilder expressionBuilder = ExpressionBuilder.getInstance();
        boolean thrown =false;
        try {
            expressionBuilder.buildExpressions(fList, aList, new ArrayList<String>(), new ArrayList<String>(), new ArrayList<AxisUnReferencableInputExpressions>());
        } catch (Exception e) {
            thrown = true;
            assert e instanceof ExpressionProcessingException;

            ExpressionProcessingException ee = (ExpressionProcessingException)e;
            assertEquals(ee.getExceptionType(), ExpressionProcessingException.RECURSIVE_EXCEPTION);
        }
        assertTrue(thrown);
    }


    @Test
    public void testBuildExpressionsUndefinedIdentifierError() {
        //prepare the test scenario
        String[] fExpressions = {
                "return f2(x) + cos(x);",//f0
                //______________________________
                "return 6.3;",//f1
                //_____________________________
                "return 2 * x + 11;"//f2
        };

        String[] aExpressions = {
                "return f0(0.25);",//a0
                //___________________________
                "return 3. + a5;",//a1
        };
        List<String> fList = Arrays.asList(fExpressions);
        List<String> aList = Arrays.asList(aExpressions);

        //make sure a5 is detected as undefined identifier
        ExpressionBuilder expressionBuilder = ExpressionBuilder.getInstance();
        boolean thrown =false;
        try {
            expressionBuilder.buildExpressions(fList, aList, new ArrayList<String>(), new ArrayList<String>(), new ArrayList<AxisUnReferencableInputExpressions>());
        } catch (Exception e) {
            thrown = true;
            assert e instanceof ExpressionProcessingException;

            ExpressionProcessingException ee = (ExpressionProcessingException)e;
            assertEquals(ee.getExceptionType(), ExpressionProcessingException.UNDEFINED_ID_EXCEPTION);
        }
        assertTrue(thrown);
    }


    /**
     * set a1 as instance variable so that it can be used in anonymous implementation.
     * Purpose is to assign multiple values to it and test whether dependent variables
     * get updated accordingly or not
     */
    private double a1 = 3.;
    @Test
    public void testBuildExpressionsSuccessScenario() throws Exception {
        final Map<Integer, Function> fImpls = new HashMap<Integer, Function>();
        final Map<Integer, Variable> aImpls = new HashMap<Integer, Variable>();

        //init f expressions and map them to expected implementations________________________________________
        String[] fArray = {
                "return a1 * f2(x) + cos(x);",//f0
                //______________________________
                "return a2 + 6.3;",//f1
                //_____________________________
                "if(x < 0) {" +//f2
                        "return sin(x);" +
                        "} else {" +
                        "return a2 * x;" +
                        "}",
                //______________________________
                "return 2 * x + 11;"//f3
        };

        fImpls.put(0, new Function() {
            //"return a1 * f2(x) + cos(x);"

            public Double f(double x) throws Exception {
                return aImpls.get(1).f() * fImpls.get(2).f(x) + Math.cos(x);
            }
        });

        fImpls.put(1, new Function() {
//            "return a2 + 6.3;"

            public Double f(double x) throws Exception {
                return aImpls.get(2).f() + 6.3;
            }
        });

        fImpls.put(2, new Function() {
//            "if(x < 0) {" +
//                    "return sin(x);" +
//                    "} else {" +
//                    "return a2 * x;" +
//                    "}"

            public Double f(double x) throws Exception {
                if(x < 0) {
                    return Math.sin(x);
                }
                return aImpls.get(2).f() * x;
            }
        });

        fImpls.put(3, new Function() {
//            "return 2 * x + 11;"

            public Double f(double x) throws Exception {
                return 2 * x + 11;
            }
        });
        // end f mapping____________________________________________________________________________________



        //init a expressions and map them to expected implementations_____________________________________
        String[] aArray = {
                "return f1(0.25);",//a0
                //___________________________
                "return 3.;",//a1
                //___________________________
                "return f3(a1);"//a2
        };
        aImpls.put(0, new Variable() {
//            "return f1(0.25);"

            public Double f() throws Exception {
                return fImpls.get(1).f(0.25);
            }
        });

        aImpls.put(1, new Variable() {
//            "return 3.;"

            public Double f() throws Exception {
                return a1;
            }
        });

        aImpls.put(2, new Variable() {
//            "return f3(a1);"

            public Double f() throws Exception {
                return fImpls.get(3).f(aImpls.get(1).f());
            }
        });
        // end a mapping____________________________________________________________________________________

        //build expressions
        List<String> fList = Arrays.asList(fArray);
        List<String> aList = Arrays.asList(aArray);
        ExpressionBuilder expressionBuilder = ExpressionBuilder.getInstance();
        expressionBuilder.buildExpressions(fList, aList, new ArrayList<String>(), new ArrayList<String>(), new ArrayList<AxisUnReferencableInputExpressions>());

        //test functions in some random points
        double[] xArr = {0.33, 90.5, 18754.3, 2, -1, -0.5, -10};
        for(double x : xArr) {
            for(Integer fIndex : fImpls.keySet()) {
                assertTrue(approximatelyEquals(fImpls.get(fIndex).f(x), ExpressionBuilder.f.get(fIndex).f(x)));
            }
        }

        //test whether  a0 and a2 which depend on a1, will follow a1 changes
        double[] a1Array = {0.23, 70., -120, 3};//some random values

        for(double a : a1Array) {
            //changing a1 value
            a1 = a;
            VariableWrapper a_1 = ExpressionBuilder.a.get(1);

            //for performance and more flexibility when spinning variables from the ui
            // VariableWrapper stores the value of a_i in an instance variable to avoid
            // re-calculating it multiple times
            //utilities have been implemented to make sure the dependent stored values get updated whenever necessary
            //==> that's what we're testing here
            a_1.setValue(a1);
            a_1.updateDependents(false);
            for(Integer aIndex : aImpls.keySet()) {
                assertTrue(approximatelyEquals(aImpls.get(aIndex).f(), ExpressionBuilder.a.get(aIndex).getValue()));
            }
        }




    }



    private static final double ERROR_MARGIN = 0.0001;
    private boolean approximatelyEquals(double y1, double y2) {
        return Math.abs(y1 - y2) < ERROR_MARGIN;
    }


	@Test
	public void testDummy() {
		String regex = "(?<!\\.)(\\bmin\\s*\\()";
		Pattern pattern = Pattern.compile(regex);
		String str = "min(1), Math.min(1) min (3  .min(1  ";
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()) {
			System.out.println(matcher.group(1));
		}
		System.out.println(str.replaceAll(regex, "MathUtils.min("));

	}
}
